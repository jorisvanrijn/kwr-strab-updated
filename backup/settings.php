<?php 

return  [

    'distribution' => 'Serpentes',

    'version' => '3.0',
    'dbversion' => '2.0',

    'essential_tables' => ['migrations', 'online_operations', 'users'],
 
    'operations_table' => [

        ////////////////
        // personalia //
        ////////////////
    	'id' => [
    		'is_actual_field' => false,
    		'laravel_db_function' => 'increments',
            'upload' => true,
            'filterable' => false,
    	],

        'set_id' => [
            'is_actual_field' => true,
            'laravel_db_function' => 'string',
            // 'hidden' => true,
            'filterable' => true,
            'comparator' => ['='],
            'trivial' => 'Set',
            'category' => 'metadata',
            'trivial' => 'Set ID',
            'editable' => false,
        ],

        'operation_type' => [
            'is_actual_field' => true,
            'laravel_db_function' => 'enum',
            'choices' => ['pool', 'eso', 'exo'],
            // 'hidden' => true,
            'filterable' => true,
            'comparator' => ['=', '<>'],
            'category' => 'metadata',
            'trivial' => 'Operatie type',
            'editable' => false,
            'upload' => true,
        ],

        'uploaded_at' => [
            'is_actual_field' => true,
            'laravel_db_function' => 'timestamp',
            // 'hidden' => true,
            'filterable' => false,
            'category' => 'metadata',
            'trivial' => 'Geupload op',
            'editable' => false,
        ],

        'completed_at' => [
            'is_actual_field' => true,
            'laravel_db_function' => 'timestamp',
            // 'hidden' => true,
            'filterable' => true,
            'comparator' => ['=', '>', '<', '>=', '<=', '<>'],
            'category' => 'metadata',
            'trivial' => 'Compleet gemarkeerd op',
            'editable' => false,
        ],

    	'timestamps' => [
    		'is_actual_field' => false,
    		'laravel_db_function' => 'timestamps',
            'upload' => true,
            'filterable' => false,
    	],

    	'patientnummer' => [
    		'is_actual_field' => true,
    		'laravel_db_function' => 'string',
    		'trivial' => 'Pati&euml;ntnummer',
    		'category' => 'personalia',
    		'required_for' => ['save'],
            'faker' => ['randomNumber', 8],
            'comparator' => ['=', 'LIKE', '<>'],
    	],

    	'patientnaam' => [
    		'is_actual_field' => true,
    		'laravel_db_function' => 'string',
    		'trivial' => 'Pati&euml;nt naam',
    		'category' => 'personalia',
    		'required_for' => ['save'],
            'faker' => 'name',
            'comparator' => ['=', 'LIKE', '<>'],
    	],

    	'geboortedatum' => [
    		'is_actual_field' => true,
    		'laravel_db_function' => 'date',
    		'trivial' => 'Geboorte datum',
    		'category' => 'personalia',
    		'required_for' => ['save', 'upload'],
            'faker' => ['date', 'Y-m-d'],
            'upload' => true,
            'comparator' => ['=', '>', '<', '>=', '<=', '<>'],
    	],

    	'geslacht' => [
    		'is_actual_field' => true,
    		'laravel_db_function' => 'enum',
    		'choices' => ['M', 'V'],
    		'trivial' => 'Geslacht',
    		'category' => 'personalia',
    		'required_for' => ['save'],
            'faker' => ['randomElement', ['M','V']],
            'upload' => true,
            'comparator' => ['=', '<>'],
    	],

    	'datum_operatie' => [
    		'is_actual_field' => true,
    		'laravel_db_function' => 'date',
    		'trivial' => 'Datum OK',
    		'category' => 'personalia',
    		'required_for' => ['save', 'upload'],
            'faker' => ['date', 'Y-m-d'],
            'upload' => true,
            'comparator' => ['=', '>', '<', '>=', '<=', '<>'],
    	],

    	'opmerkingen_personalia' => [
    		'is_actual_field' => true,
    		'laravel_db_function' => 'text',
    		'trivial' => 'Opmerkingen',
    		'category' => 'personalia',
            'faker' => ['paragraph', 3],
            'filterable' => false
    	],

    	'verwijzer' => [
    		'is_actual_field' => true,
    		'laravel_db_function' => 'string',
    		'trivial' => 'Verwijzer',
    		'category' => 'personalia',
            'faker' => 'name',
            'filterable' => false
    	],

        ////////////////////////
        // prognostisch_basis //
        ////////////////////////
        'type_scheelzien' => [
            'is_actual_field' => true,
            'laravel_db_function' => 'enum',
            'choices' => ['esotropie 1', 'esotropie 2', 'exotropie 1', 'exotropie 1', 'overig 1', 'overig 2'],
            'trivial' => 'Type scheelzien',
            'category' => 'prognostisch_basis',
            'required_for' => ['save', 'upload'],
            'faker' => ['randomElement', ['esotropie 1', 'esotropie 2', 'exotropie 1', 'exotropie 1', 'overig 1', 'overig 2']],
            'upload' => true,
            'comparator' => ['=', '<>'],
        ],

        /////////////////////////////
        // prognostisch_uitgebreid //
        /////////////////////////////

        //////////////////////////
        // preoperatieve_hoeken //
        //////////////////////////

        ///////////////////////////////
        // preoperatieve_bevindingen //
        ///////////////////////////////

        ///////////////
        // chirurgie //
        ///////////////

        //////////////////////////
        // chirurgie_uitgebreid //
        //////////////////////////

        ////////////////////////////
        // chirurgie_complicaties //
        ////////////////////////////

        //////////////////////
        // follow_up_hoeken //
        //////////////////////

        ////////////////////////////
        // follow_up_complicaties //
        ////////////////////////////

        //////////////////////////
        // follow_up_uitgebreid //
        //////////////////////////

    ],

    'operations_table_categories' => [

    	'personalia' => [
    		'trivial' => 'Personalia'
    	],

        'prognostisch_basis' => [
            'trivial' => 'Prognostisch basis'
        ],

        'prognostisch_uitgebreid' => [
            'trivial' => 'Prognostisch uitgebreid'
        ],

        'preoperatieve_hoeken' => [
            'trivial' => 'Preoperatieve hoeken'
        ],

        'preoperatieve_bevindingen' => [
            'trivial' => 'Preoperatieve bevindingen'
        ],

        'chirurgie' => [
            'trivial' => 'Chirurgie'
        ],

        'chirurgie_uitgebreid' => [
            'trivial' => 'Chirurgie uitgebreid'
        ],

        'chirurgie_complicaties' => [
            'trivial' => 'Chirurgie complicaties'
        ],

        'follow_up_hoeken' => [
            'trivial' => 'Follow up hoeken'
        ],

        'follow_up_complicaties' => [
            'trivial' => 'Follow up complicaties'
        ],

        'follow_up_uitgebreid' => [
            'trivial' => 'Follow up uitgebreid'
        ],

        'metadata' => [
            'trivial' => 'Record gegevens',
        ],

    ],

    'extra_tables' => [
        'id' => [
            'is_actual_field' => false,
            'laravel_db_function' => 'increments',
            'upload' => true,
        ],
        'timestamps' => [
            'is_actual_field' => false,
            'laravel_db_function' => 'timestamps',
            'upload' => true,
        ],
        'key' => [
            'is_actual_field' => true,
            'laravel_db_function' => 'string',
            'trivial' => 'Naam',
            'required_for' => ['save'],
        ],
        'value' => [
            'is_actual_field' => true,
            'laravel_db_function' => 'string',
            'trivial' => 'Data',
            'required_for' => ['save'],
        ],
        'value2' => [
            'is_actual_field' => true,
            'laravel_db_function' => 'string',
            'trivial' => 'Data',
        ],

    ]

];