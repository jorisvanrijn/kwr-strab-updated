<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Slack;
use App\AnalyseTable;

use Illuminate\Support\Facades\Config;
use App\LocalOperation;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

use App\Helpers\DatabaseCreator as DatabaseCreator;

class Analyse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'analyse:prepare {type} {tempanalysetable} {serpentes}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Prepare analysis';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    
    private function logMsg($msg){
        Slack::send($msg);
        $this->info($msg);
    }

    public function handle()
    {
        
        $this->logMsg('Doing analysis prep for type: ' . $this->argument('type'));

        // Maak een tabel voor de analyse date
        Schema::dropIfExists($this->argument('tempanalysetable'));
        
        Schema::create($this->argument('tempanalysetable'), function(Blueprint $table){
            $fields_of_table = Config::get('settings.extra_tables');
            DatabaseCreator::prepareTable($fields_of_table, $table);
        });

        $this->logMsg('Table made: '.$this->argument('tempanalysetable'));

        switch( $this->argument('type') ){
            case 'local':

                return $this->localAnalyse($this->argument('tempanalysetable'), $this->argument('serpentes'));

                break;
            default:
                return 1;
        }

        return 2;
    }

    private function localAnalyse($table, $serpentes){

        $fields = Config::get('settings.operations_table');
        $categories = Config::get('settings.operations_table_categories');
        $to_analyse = Config::get('settings.analyse_fields');        

        $operations = LocalOperation::sessiondb($serpentes)->get();

        foreach($fields as $f => $field){

            $data = [];
            $dataToPass = [];

            $total = count($operations);

            if($field['is_actual_field'] AND in_array($field['laravel_db_function'], $to_analyse) AND $total > 5){
                
                switch($field['laravel_db_function']){
                    case 'boolean':

                        $data[0] = 0;
                        $data[1] = 0;

                        foreach($data as $k => $v){
                            $dataToPass[$k] = round($v/$total*100, 2);
                        }

                        break;
                    case 'double';
                        break;
                    case 'degdpt';
                        break;
                    case 'int';
                        break;
                    case 'enum';

                        foreach($field['choices'] as $c){
                            $data[$c] = 0;
                        }     

                        foreach($data as $k => $v){
                            $dataToPass[$k] = round($v/$total*100, 2);
                        }

                        break;
                    case 'visus';
                        break;
                    default:
                        break;
                }

            }

            $analyseTable = new AnalyseTable;
            $analyseTable->setTable($table);
            $analyseTable->setData(['key' => $f, 'value' => $field['laravel_db_function'], 'value2' => json_encode($dataToPass)]);
            $analyseTable->save();

        }

        return 0;
    }
}
