$resp = Artisan::call('analyse:prepare', [
            'type' => 'local',
            'tempanalysetable' => $table,
            'serpentes' => $request->session()->get('serpentes'),
        ]);

        $responseCodesAnalyse = [
            0 => 'Analyse voorbereid',
            1 => 'Type not found',
            2 => 'Undefined error',
        ];

        $analyseTable = new AnalyseTable;
        $analyseTable->setTable($table);
        $analysis = $analyseTable->get();

        $data = [];

        $data['analysis'] = [];
        foreach($analysis as $row){
            $data['analysis'][$row->key] = $row;
        }

        $data['fields'] = Config::get('settings.operations_table');
        $data['categories'] = Config::get('settings.operations_table_categories');
        
        $data['request'] = $request;   

        return view('analysis.local', $data);