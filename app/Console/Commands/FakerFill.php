<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\LocalOperation;
use Faker;
use Illuminate\Support\Facades\Config;

use Slack;

class FakerFill extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'faker:fill {aantal} {serpentes} {--queue}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fill database with fake data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $faker = Faker\Factory::create();
        $fields = Config::get('settings.operations_table');
        $aantal = $this->argument('aantal');
        $serpentes = $this->argument('serpentes');

        $bar = $this->output->createProgressBar($aantal);

        for ($i=0; $i < $aantal; $i++) { 

            $d = [];

            foreach($fields as $kf => $f){
                if(isset($f['faker'])){

                    if($f['faker'] == 'randomDouble'){
                        $min = isset($f['minimum_faker']) ? $f['minimum_faker'] : 0;
                        $max = isset($f['maximum_faker']) ? $f['maximum_faker'] : 0;

                        $d[$kf] = $faker->randomFloat(4, $min, $max);
                    }elseif($f['faker'] == 'yesOrNo'){
                        $d[$kf] = $faker->numberBetween(0,1);
                    }else{

                        if(is_array($f['faker'])){
                            if(count($f['faker']) > 1){
                                $d[$kf] = $faker->{$f['faker'][0]}($f['faker'][1]);
                            }else{
                                $d[$kf] = $faker->{$f['faker'][0]};
                            }
                        }else{
                            $d[$kf] = $faker->{$f['faker']};
                        }

                    }

                }
            }

            $o = new LocalOperation($d);
            $o->setSessionDB($serpentes);    
            // dd($o->verifyForUpload(true));       
            $o->save();

            // dd(array_keys(get_defined_vars()));
            
            unset($d);
            unset($kf);
            unset($f);
            unset($min);
            unset($max);
            unset($o);

            $bar->advance();

        }

        $bar->finish();

        Slack::send('database is filled');
        $this->comment("database is filled");

    }    
}
