<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Slack;
use DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Config;

class CleanDB extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'serpentes:cleandb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean non essential data to protect database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tables = DB::select('SHOW TABLES');
        $essential_tables = Config::get('settings.essential_tables');
        
        foreach($tables as $table){
            if(!in_array($table->Tables_in_serpentes, $essential_tables)){
                Schema::dropIfExists($table->Tables_in_serpentes);
            }
        }

        Slack::send('database is cleaned');
        $this->comment("database is cleaned");
    }
}
