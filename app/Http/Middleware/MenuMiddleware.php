<?php

namespace App\Http\Middleware;

use Closure;
use Menu;
use Caffeinated\Menus\Builder;

use App\Session;
use Auth;
use Illuminate\Support\Facades\Schema;

use App\Setting;

class MenuMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        // Make the top menu
        Menu::make('firstBar', function(Builder $menu){

            if (Auth::check()){
                if(Auth::user()->userid == 'admin'){
                    $menu->add('Admin Dashboard',       'admin')->active('/admin/*');
                }else{
                    $menu->add('Dashboard',   'dashboard')->active('/dashboard/*');
                    $menu->add('Operaties',   'operations')->active('/operations/*');
                    $menu->add('Analyse',     'analysis')->active('/analysis/*');
                    $menu->add('Tools',       'tools')->active('/tools/*');
                    $menu->add('Help',        'help')->active('/help/*');
                }
            }

        });

        Menu::make('secondBar', function(Builder $menu){

        });

        Menu::make('userBar', function(Builder $menu){

            if (Auth::check()){
                if(Auth::user()->userid != 'admin'){
                    $menu->add('Tussentijds opslaan',   'dashboard/download');
                }

                $menu->add('Uitloggen', url('logout'));
            }

        });

        if (Auth::check() AND Auth::user()->userid != 'admin'){

            if(Schema::hasTable('settings_'.$request->session()->get('serpentes'))){
                $enable_job_lock = Setting::sessiondb($request->session()->get('serpentes'))->firstOrNew(['key' => 'job_lock']);
                $enable_job_lock->setSessionDB($request->session()->get('serpentes'));

                if($enable_job_lock->value == 'on'){
                    $job_data = json_decode($enable_job_lock->value2, true);
                    view()->share('job_data', '1');
                }
            }

        }

        return $next($request);

    }
}
