<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Session;
use Menu;
use Auth;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

use Illuminate\Support\Facades\Config;

use App\Helpers\DatabaseCreator as DatabaseCreator;

use App\Helpers\Layout as Layout;
use App\Helpers\Calculator as Calculator;
use App\Helpers\FilterHelper as FilterHelper;

use App\LocalOperation;
use App\OnlineOperation;
use App\Set;
use App\Filter;
use App\View;
use App\AnalyseTable;

use Slack;

use JobHelper;

use Illuminate\Support\Facades\DB;

class AnalysisController extends Controller
{
    /////////////////
    // Constructor //
    /////////////////
	public function __construct(Request $request){
        Menu::get('secondBar')->add('Analyse', 'analysis');
        Menu::get('secondBar')->add('Set Analyse', 'analysis/set')->active('show/set')->active('do/set/{id}');
        Menu::get('secondBar')->add('Lokale vs. landelijke analyse', 'analysis/localvsnational')->active('show/localvsnational')->active('do/localvsnational');
	    Menu::get('secondBar')->add('Lokale analyse', 'analysis/local')->active('show/local')->active('do/local');
        Menu::get('secondBar')->add('Landelijke analyse', 'analysis/national')->active('show/national')->active('do/national');
	}

	//////////////////////
    // List of analyses //
    //////////////////////
    public function index(Request $request){

    	$data = [];

    	return view('analysis.all', $data);

    }

    /**
     * Er zijn 4 anlayses: 
     *     set                  (parent_type: set)
     *     lokaal vs landelijk  (parent_type: list)
     *     lokaal               (parent_type: list)
     *     landelijk            (parent_type: list)
     */
    
    ///////////
    // set //
    ///////////
    public function set(Request $request){

        $sets = Set::sessiondb($request->session()->get('serpentes'));
        $sets->paginate(10);

        $data = [];
        $data['sets'] = $sets->paginate(10);
        $data['request'] = $request;

        return view('analysis.sets', $data);

    }

    public function do_set(Request $request, $id){
        return $this->do_set_analyse($request, $id);
    }
    
    ///////////
    // local //
    ///////////
    public function local(Request $request){

        $data = [];
        $data['fields'] = Config::get('settings.operations_table');
        $data['request'] = $request;
        $data['url'] = url('analysis/do/local').'/';

        return view('analysis.start', $data);

    }

    public function do_local(Request $request, $id){
        return $this->do_list_analyse($request, 'lokaal', $id);
    }

    //////////////
    // national //
    //////////////
    public function national(Request $request){

        $data = [];
        $data['fields'] = Config::get('settings.operations_table');
        $data['request'] = $request;
        $data['url'] = url('analysis/do/national').'/';

        return view('analysis.start', $data);

    }

    public function do_national(Request $request, $id){
        return $this->do_list_analyse($request, 'landelijk', $id);
    }

    ///////////////////////
    // local vs national //
    ///////////////////////
    public function localvsnational(Request $request){

        $data = [];
        $data['fields'] = Config::get('settings.operations_table');
        $data['request'] = $request;
        $data['url'] = url('analysis/do/localvsnational').'/';

        return view('analysis.start', $data);

    }

    public function do_localvsnational(Request $request, $id){
        return $this->do_list_analyse($request, 'lokaalvslandelijk', $id);
    }

    /////////////////
    // Set Analyse //
    /////////////////
    public function do_set_analyse(Request $request, $id){
        $field = 'postop_hor_manifest_afstand';
        $original_set = Set::sessionDB($request->session()->get('serpentes'))->find($id);
        // $xVals = [-20, -15, -10, -5, 0, 5, 10, 15, 20];
        $xVals = [-20, -15, -10, -5, -3, 0, 3, 5, 10, 15, 20];
        $sets = OnlineOperation::select('set_id')->where('operation_type', $original_set['value2'])->groupBy('set_id')->get();
        $set_size = Config::get('settings.set_size');

        if(count($sets) < 5){
            return view('general.justanerror', 
                ['title' => 'Niet genoeg sets', 'msg' => 'De landelijke database bevat te weinig '.$original_set['value2'].'-sets om te analyseren']
            );
        }

        $table = 'analyse_'.$request->session()->get('serpentes').'_'.str_random(10);
        $request->session()->put('last_analysis_table', $table);
        $request->session()->put('last_analysis_set', $original_set['key']);

        Schema::create($table, function(Blueprint $table){
            $operations_table = Config::get('settings.extra_tables');
            DatabaseCreator::prepareTable($operations_table, $table);
        });

        $job = 'SetAnalyseJob';
        $totaal = count($sets);
        $in_batches_of = 1;
        $serpentes = $request->session()->get('serpentes');
        $success = 'Sets worden geanalyseerd';
        $error = 'Analyse kon niet uitgevoerd worden';
        $button = 'Ga verder';

        $data = [];
        $data['field'] = $field;
        $data['original_set'] = $original_set;
        $data['xVals'] = $xVals;
        $data['sets'] = $sets;
        $data['set_size'] = $set_size;
        $data['table'] = $table;

        Slack::send('nieuwe set analyse. data: ```'.json_encode([$job, $totaal, $in_batches_of, $serpentes]).'```');

        return JobHelper::processJobs($job, $totaal, $in_batches_of, $serpentes, $success, $error, $button, $data);
    }

    /////////////////
    // SET ANALYSE //
    /////////////////
    public function show_set_analyse(Request $request){
        $field = 'postop_hor_manifest_afstand';
        $last_analysis_table = $request->session()->get('last_analysis_table');
        $last_analysis_set = $request->session()->get('last_analysis_set');
        $set_size = Config::get('settings.set_size');

        $results = new AnalyseTable;
        $results->setTable($last_analysis_table);
        $setData = $results->get();

        $graphBoxPlots = [];
        $graphData = [];
        // $xVals = [-20, -15, -10, -5, 0, 5, 10, 15, 20];
        $xVals = [-20, -15, -10, -5, -3, 0, 3, 5, 10, 15, 20];

        // Eerst de landelijke data maar eens pakken
        foreach($xVals as $xVal){
            $graphData[$xVal] = [];

            foreach($setData as $set){
                $d = json_decode($set['value'], true);
                // dd($d);
                $graphData[$xVal][] = $d[$xVal];
            }

            $graphBoxPlots[$xVal] = [
                'min' => Calculator::get_min($graphData[$xVal]),
                'q1' => Calculator::get_percentile(25, $graphData[$xVal]),
                'median' => Calculator::get_median($graphData[$xVal]),
                'q3' => Calculator::get_percentile(75, $graphData[$xVal]),
                'max' => Calculator::get_max($graphData[$xVal]),
            ];
        }

        // Aangepast voor 3.1.0.8
        // $used_operations = OnlineOperation::where('set_id', $last_analysis_set)->where($field, '<>', '', 'and')->count();
        $used_operations = OnlineOperation::where('set_id', $last_analysis_set)->whereNotNull($field)->count();

        // Haal ook eigen data op
        foreach($xVals as $xVal){
            // $graphBoxPlots[$xVal]['own'] = rand(10, 80);
            
            // Aangepast voor 3.1.0.5
            // $graphBoxPlots[$xVal]['own'] = (OnlineOperation::where($field, '<=', $xVal)->where('set_id', $last_analysis_set)->count()/$set_size)*100;
            
            /*$graphBoxPlots[$xVal]['own'] = (

                OnlineOperation::where($field, '<=', $xVal)->where('set_id', $last_analysis_set)->where($field, '<>', '', 'and')->count()

                /

                $used_operations

            )*100;*/

            // Aangepast voor 3.1.0.8
            $graphBoxPlots[$xVal]['own'] = (

                OnlineOperation::where($field, '<=', $xVal)->where('set_id', $last_analysis_set)->whereNotNull($field)->count()

                /

                $used_operations

            )*100;
        }

        // Maak hier maar een mooie graph
        
        $data_for_GoogleChart = [];
        $data_for_PHPlot = [];
        foreach($graphBoxPlots as $x => $y){
            $data_for_GoogleChart[] = [$x, $y['min'], $y['q1'], $y['q3'], $y['max']];
            $data_for_PHPlot[] = [(String) ($x), $y['min'], $y['q1'], $y['median'], $y['q3'], $y['max'], $y['own']];
        }

        require_once(base_path('third/phplot.php'));
        $x = mt_rand();

        $plot = new \PHPlot(800, 500);

        //$plot->SetTitle('Box Plot (without outliers)');
        $plot->SetDataType('text-data');
        $plot->SetDataValues($data_for_PHPlot);
        $plot->SetPlotType('boxes');

        $plot->SetXTickIncrement(1);
        $plot->SetXTickAnchor(0.5);
        $plot->SetImageBorderType('plain');
        $plot->SetLineWidths(array(2, 2, 2));

        $plot->SetDataBorderColors('#3ea95f');
        $plot->SetDataColors(array('#3ea95f', '#27324e', '#d43633'));

        $plot->SetIsInline(true);
        $plot->SetOutputFile(base_path('public/assets/graphs/'.$x.'.png'));
        $plot->DrawGraph();

        $graph_url = (asset('assets/graphs/'.$x.'.png'));

        // Laten we nu maar een tabel gaan maken dan
        
        $d = ($graphBoxPlots);

        $tabel = '<table class="table table-striped table-hover">';
        $tabel .= '
            <tr>
                <th>hoek</th>
                <th>Min</th>
                <th>Q1</th>
                <th>Q2</th>
                <th>Q3</th>
                <th>Max</th>
                <th>Eigen</th>
            </tr>
        ';

        foreach($d as $row_key => $row){
            $tabel .= '<tr><th>'.$row_key.'</th>';
            foreach($row as $column_key => $column_value){
                $tabel .= '<td>'.round($column_value, 2).'</td>';
            }
            $tabel .= '</tr>';
        }

        return view('analysis.set', [
            'data_for_graph' => $data_for_GoogleChart, 
            'set' => $last_analysis_set, 
            'graph_url' => $graph_url, 
            'tabel' => $tabel,
            'aantal_sets' => count($setData),
            'set_size' => $set_size,
            'used_operations' => $used_operations,
        ]);
    }

    //////////////////
    // LIST ANALYSE //
    //////////////////
    public function do_list_analyse(Request $request, $type = 'lokaal', $filter_id = -1){

        // Kijk of de filters wel geldig zijn

        if(($f = FilterHelper::filter_general($request)) !== false) return $f;

        // Eerst: minimaal 5 operaties nodig voor een lijst analyse
        
        $serpentes = $request->session()->get('serpentes');

        // $aantal_operaties = LocalOperation::sessionDB($serpentes)
        //     ->orderBy('created_at', 'asc')
        //     ->count();
            
        $do_count = [];

        if($type == 'lokaalvslandelijk'){
            $do_count[] = 'landelijk';
            $do_count[] = 'lokaal';
        }

        if($type == 'landelijk'){
            $do_count[] = 'landelijk';
        }
        
        if($type == 'lokaal'){
            $do_count[] = 'lokaal';
        }

        $request->session()->forget('last_analysis_count_landelijk');
        $request->session()->forget('last_analysis_count_lokaal');

        foreach($do_count as $tabel){
            $operations_filtered_query = FilterHelper::filter_query($request, $tabel, $filter_id);
            
            $sql = $operations_filtered_query->toSql();
            $bindings = $operations_filtered_query->getBindings();
            
            // $re = "/\\bwhere(.*)?\\b/"; 
            $re = "/\\bwhere(.*)?\\border by\\b/"; 
            // $str = "select * from `operations_M5dDHatwJ8BiahmuQNdTPERnatGJ0xj0e` where `type_scheelzien` LIKE ? and `operation_type` = ? order by `id` desc"; 
            preg_match($re, $sql, $matches);

            if(count($matches) > 0){
                $sql_where = ($matches[1]);
            }else{
                $sql_where = '';
                $bindings = [];
            }

            try{
                $aantal_operaties = $operations_filtered_query->count();
            } catch (\Exception $e) {
                return view('general.justanerror', 
                    ['title' => 'Invalide filter', 'msg' => 'Het geselecteerde filter kon niet gebruikt worden om operaties te selecteren. Er zijn condities toegevoegd die enkel lokale databases kunnen filteren en niet te gebruiken zijn op landelijke databases.']
                );
            }

            $request->session()->put('last_analysis_count_'.$tabel, $aantal_operaties);

            if($aantal_operaties < 5){
                return view('general.justanerror', 
                    ['title' => 'Niet genoeg operaties', 'msg' => 'Uw selectie bevat te weinig operaties om te analyseren']
                );
            }
        }

        // Analyse logica

        $table = 'analyse_'.$serpentes.'_'.str_random(10);
        $request->session()->put('last_analysis_table', $table);
        $request->session()->put('last_analysis_parent_type', 'list');
        $request->session()->put('last_analysis_type', $type);

        Schema::create($table, function(Blueprint $table){
            $operations_table = Config::get('settings.extra_tables');
            DatabaseCreator::prepareTable($operations_table, $table);
        });

        $fields = config('settings.operations_table');
        $fields_to_analyse = [];
        foreach($fields as $f => $field){
            if(isset($field['category']) AND trim($field['category']) != 'metadata' AND isset($field['is_actual_field']) AND $field['is_actual_field']){

                // Dit is voor als het om online operaties gaat:
                if($type == 'landelijk' OR $type == 'lokaalvslandelijk'){
                    if(isset($field['upload']) AND $field['upload']){
                        $fields_to_analyse[] = $f;
                    }
                }
                
                if($type == 'lokaal'){
                    $fields_to_analyse[] = $f;
                }

            }
        }

        $job = 'ListAnalyseJob';
        $totaal = count($fields_to_analyse);
        $in_batches_of = 1;
        $serpentes = $serpentes;
        $success = 'Velden van geselecteerde operaties worden geanalyseerd';
        $error = 'Analyse kon niet uitgevoerd worden';
        $button = 'Ga verder';

        $data = [];
        $data['table'] = $table;
        $data['type_analysis'] = $type;
        $data['filter_query'] = $sql_where;
        $data['filter_bindings'] = $bindings;
        $data['fields_to_analyse'] = $fields_to_analyse;

        Slack::send('nieuwe list analyse. data: ```'.json_encode([$job, $totaal, $in_batches_of, $serpentes]).'```');

        return JobHelper::processJobs($job, $totaal, $in_batches_of, $serpentes, $success, $error, $button, $data);

    }

    public function show_list_analyse(Request $request){
        $last_analysis_table = $request->session()->get('last_analysis_table');
        $last_analysis_parent_type = $request->session()->get('last_analysis_parent_type');
        $last_analysis_type = $request->session()->get('last_analysis_type');
        $last_analysis_count_lokaal = $request->session()->get('last_analysis_count_lokaal');
        $last_analysis_count_landelijk = $request->session()->get('last_analysis_count_landelijk');

        $results = new AnalyseTable;
        $results->setTable($last_analysis_table);
        $analyse = $results->get();

        $analysis = [];
        foreach($analyse as $a){
            $analysis[$a->key] = $a;
        }

        // dd($analyse);
        
        $data = [
            'analyse' => $analysis,
            'last_analysis_table' => $last_analysis_table,
            'last_analysis_parent_type' => $last_analysis_parent_type,
            'last_analysis_type' => $last_analysis_type,
            'last_analysis_count_lokaal' => $last_analysis_count_lokaal,
            'last_analysis_count_lokaal' => $last_analysis_count_lokaal,
            'last_analysis_count_landelijk' => $last_analysis_count_landelijk,
            'categories' => config('settings.operations_table_categories'),
            'fields' => config('settings.operations_table'),
            'request' => $request,
        ];
        
        return view('analysis.list', $data);
    }

}

// $analayseResult = new AnalyseTable;
// $analayseResult->setTable($table);

// $analayseResult->setData([
//     'key' => 'test',
//     'value' => 'a',
//     'value2' => 'b',
// ]);

// $analayseResult->save();