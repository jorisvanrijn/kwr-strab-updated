<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Setting;
use Slack;

class HelpController extends Controller
{
    public function index(Request $request){
    	$data['user_id'] = $request->session()->get('userid');
    	$data['session_key'] = $request->session()->get('serpentes');
    	$data['demodb'] = $request->session()->get('demodb');

    	// dd($request->session()->get('demodb'));

    	$db_version = Setting::sessiondb($request->session()->get('serpentes'))->firstOrNew(['key' => 'db_version']);
    	$data['db_version'] = (trim($db_version->value) == '') ? '[nieuwe database]' : $db_version->value;

    	return view('help.main', $data);
    }
}

/**
 * Filter help:
 * > Zoeken in strings kan met LIKE
 * > Selecteren op niet geupload: WHERE uploaded_at == null
 * > Selecteren op niet compleet: WHERE completed_at == null
 * > Data moeten speciaal ingevuld worden: Y-m-d
 * Data faker: max 1500
 * Je kunt geen database opslaan zonder operaties (ook al zitten er filters en views in)
 * Database upload < 50 MB
 */
