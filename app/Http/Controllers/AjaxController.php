<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Setting;
use App\View;
use App\Filter;
use App\Set;

class AjaxController extends Controller
{
    public function setlastview(Request $request, $view_id){
    	$last_view = Setting::sessiondb($request->session()->get('serpentes'))->firstOrNew(['key' => 'last_used_view']);
        $last_view->setSessionDB($request->session()->get('serpentes'));
        $last_view->value = $view_id;
        $last_view->save();
    }
    public function setorder(Request $request){
    	$order = $request->input('orderedids');
    	$sorttype = $request->input('sorttype');

    	foreach($order as $o => $key){
    		if($sorttype == 'views'){
    			$x = View::sessiondb($request->session()->get('serpentes'))->find($key);
    		}elseif($sorttype == 'sets'){
    			$x = Set::sessiondb($request->session()->get('serpentes'))->find($key);
    		}elseif($sorttype == 'filters'){
    			$x = Filter::sessiondb($request->session()->get('serpentes'))->find($key);
    		}

    		if(isset($x)){
				$x->setSessionDB($request->session()->get('serpentes'));
				$x->value3 = $o;
				$x->save();
				unset($x);
			}
    		
    	}
    }
}
