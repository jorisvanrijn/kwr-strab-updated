<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Session;
use Menu;
use Auth;

use Illuminate\Support\Facades\Config;

use App\Helpers\Layout as Layout;
use App\Helpers\FilterHelper as FilterHelper;

use App\LocalOperation;
use App\OnlineOperation;
use App\Set;
use App\Filter;
use App\View;
use Excel;
use Slack;

use DateTime;

use Carbon\Carbon;

use JobHelper;

class OperationsController extends Controller
{
    /////////////////
    // Constructor //
    /////////////////
	public function __construct(Request $request){
	
        $enoughOperationsForUpload = false;

        Menu::get('secondBar')->add('Operaties', 'operations')->active('operations/new')->active('operations/edit/*')->active('operations/del/*')->active('operations/view/*')->active('operations/filter/*');
        Menu::get('secondBar')->add('Sets', 'operations/sets')->active('edit_set/*');
        Menu::get('secondBar')->add('Filters', 'operations/filters')->active('new_filter')->active('edit_filter/*');
        Menu::get('secondBar')->add('Views', 'operations/views')->active('new_view')->active('edit_view/*');
        Menu::get('secondBar')->add('Uploaden'.(($enoughOperationsForUpload) ? ' <i class="icon-warning-sign icon-white icon"></i>' : ''), 'operations/upload');
	}

    ////////////////////////
    // List of operations //
    ////////////////////////
    public function index(Request $request, $filter = -1){

        if(($f = FilterHelper::filter_general($request)) !== false) return $f;
    	
    	$data = [];
        
        $operations_filtered_query = FilterHelper::filter_query($request, 'lokaal', $filter);

        $data['fields'] = Config::get('settings.operations_table');
        $data['request'] = $request;
        $data['current_filter'] = $filter;

    	if(isset($_GET['export'])){
            $operations = $operations_filtered_query->get();
            Excel::create('operations', function($excel) use($operations) {
                $excel->sheet('Sheet 1', function($sheet) use($operations) {
                    $sheet->fromArray($operations);
                });
            })->export('xls');
        }else{
            $data['operations'] = $operations_filtered_query->paginate(80);
            return view('operations.all', $data);
        }

    }

    /////////////////////
    // Operations CRUD //
    /////////////////////
    public function viewoperation(Request $request, $id){

        $data = [];
    
        $data['fields'] = Config::get('settings.operations_table');
        $data['categories'] = Config::get('settings.operations_table_categories');
        $data['id'] = $id;
        $data['operation'] = LocalOperation::sessionDB($request->session()->get('serpentes'))->find($id);
        $data['request'] = $request;

        return view('operations.single', $data);

    }

    public function newoperation(Request $request){

    	$data = [];
        $data['operation'] = [];
    
    	$data['fields'] = Config::get('settings.operations_table');
    	$data['categories'] = Config::get('settings.operations_table_categories');
        $data['request'] = $request;

    	return view('operations.new', $data);

    }

    public function editoperation(Request $request, $id){

        $data = [];
    
        $data['fields'] = Config::get('settings.operations_table');
        $data['categories'] = Config::get('settings.operations_table_categories');
        $data['id'] = $id;
        $data['operation'] = LocalOperation::sessionDB($request->session()->get('serpentes'))->find($id);
        $data['request'] = $request;

        return view('operations.edit', $data);

    }

    public function saveoperation(Request $request, $id = null){

        if(is_null($id)){
            $operation = new LocalOperation;
        	$operation->setSessionDB($request->session()->get('serpentes'));
        }else{
            $operation = LocalOperation::sessionDB($request->session()->get('serpentes'))->find($id);
            $operation->setSessionDB($request->session()->get('serpentes'));
        }

        $data = [];

        $fields = Config::get('settings.operations_table');
        Carbon::setLocale('nl');

        $_errors = [];
        foreach($fields as $k => $v){            
            if($v['is_actual_field']){
                if($v['laravel_db_function'] == 'date'){
                    try{
                        if($request->input($k)){
                            $data[$k] = Carbon::parse($request->input($k), 'Europe/Amsterdam')->format('Y-m-d');
                        }else{
                            $data[$k] = $request->input($k);
                        }
                    }catch(\Exception $e){
                        $_errors[$k] = 'Ongeldig data formaat';
                    }
                }else{
                    $data[$k] = $request->input($k);
                }
            }
        }

        $operation->setData($data);

        $errormsg = array_merge($operation->verifyForSave(), $_errors);

        if( count($errormsg) > 0 ){
            // dd($errormsg);
            return redirect()->back()
            ->withInput($request->all())
            ->withErrors($errormsg);
        }

    	$operation->save();

        if($request->submitandthen == 'Opslaan en nieuw'){
    	   return redirect('operations/new')->with([
                'status' => 'Operatie is opgeslagen',
                'type' => 'success'
            ]);
        }else{
            return redirect('operations/view/'.$operation->id)->with([
                'status' => 'Operatie is opgeslagen',
                'type' => 'success'
            ]);;
        }

    }

    public function deloperation(Request $request, $id){
        $operation = LocalOperation::sessionDB($request->session()->get('serpentes'))->find($id);
        $operation->setSessionDB($request->session()->get('serpentes'));
        $operation->delete();

        return redirect('operations')->with([
            'status' => 'Operatie is verwijderd',
            'type' => 'info'
        ]);
    }

    ///////////////
    // Sets CRUD //
    ///////////////
    public function sets(Request $request){
        $data = [];
        $sets = Set::sessiondb($request->session()->get('serpentes'));

        if(isset($_GET['export'])){
            $sets = $sets->get();

            Excel::create('sets', function($excel) use($sets) {
                $excel->sheet('Sheet 1', function($sheet) use($sets) {
                    $sheet->fromArray($sets);
                });
            })->export('xls');
        }else{
            $sets->orderBy('value3', 'asc')->orderBy('created_at', 'desc');
            $sets->paginate(10);
            $data['sets'] = $sets->paginate(10);
            $data['request'] = $request;
            $data['sorttype'] = 'sets';

            return view('operations.sets.all', $data);
        }
    }

    public function editset(Request $request, $id){
        $data['set'] = Set::sessionDB($request->session()->get('serpentes'))->find($id);
        $data['id'] = $id;
        return view('operations.sets.edit', $data);
    }

    public function saveset(Request $request, $id){
        $set = Set::sessionDB($request->session()->get('serpentes'))->find($id);
        $set->setSessionDB($request->session()->get('serpentes'));
        $set->value = $request->input('value');
        $set->save();

        return redirect('operations/sets')->with([
            'status' => 'Set is opgeslagen',
            'type' => 'success'
        ]);;
    }

    // DEPRECATED
    /* public function delset(Request $request, $id){
        $set = Set::sessionDB($request->session()->get('serpentes'))->find($id);
        $set->setSessionDB($request->session()->get('serpentes'));
        $set->delete();

        return redirect('operations/sets');
    } */

    //////////////////
    // Filters CRUD //
    //////////////////
    public function filters(Request $request){
        $data = [];
        $filters = Filter::sessiondb($request->session()->get('serpentes'));
        $filters->where('key', '<>', '(naamloze filter)');
         
        if(isset($_GET['export'])){
            $filters = $filters->get();
            Excel::create('filters', function($excel) use($filters) {
                $excel->sheet('Sheet 1', function($sheet) use($filters) {
                    $sheet->fromArray($filters);
                });
            })->export('xls');
        }else{
            $filters->orderBy('value3', 'asc')->orderBy('created_at', 'desc');
            $filters->paginate(10);
            // dd($filters->toSql());
            $data['filters'] = $filters->paginate(10);
            $data['request'] = $request;     
            $data['sorttype'] = 'filters';  

            return view('operations.filters.all', $data);
        }
    }

    public function newfilter(Request $request){
        $data = [];
        $data['data'] = '';
        // if($request->session()->has('filterdata')){
        //     $args = $request->session()->get('filterdata');
        //     unset($args['savefilter']);
        //     $data['data'] = http_build_query($args);
        // }
        
        // dd(url()->previous());
        $data['prevurl'] = url()->previous();

        $data['fields'] = Config::get('settings.operations_table');
        return view('operations.filters.new', $data);
    }

    public function editfilter(Request $request, $id){
        $current_filter_obj = Filter::sessiondb($request->session()->get('serpentes'))->findOrFail($id);

        $data = [];
        $data['data'] = '';
        // if($request->session()->has('filterdata')){
        //     $args = $request->session()->get('filterdata');
        //     unset($args['savefilter']);
        //     $data['data'] = http_build_query($args);
        // }
        $data['prevurl'] = url()->previous();
        $data['fields'] = Config::get('settings.operations_table');

        $data['filterid'] = (int) $id;

        $data['filterkey'] = $current_filter_obj->key;
        $data['filtervalue'] = addslashes(html_entity_decode(base64_decode($current_filter_obj->value), ENT_QUOTES));
        // dd($data['filtervalue']);
        return view('operations.filters.edit', $data);
    }

    public function savefilter(Request $request, $id = null){

        ///////////////
        // validatie //
        ///////////////
        $err = [];

        if($request->input('actionbutton') == 'save'){
            if(trim($request->key) == '') $err['key'] = 'Geef de filter een naam';
            if(trim($request->key) == '(naamloze filter)') $err['key'] = 'Geef de filter een naam';
        }

        if(trim($request->value) == '') $err['value'] = 'Geef filter data op';
        if(count($err) > 0) return redirect()->back()->withErrors($err);

        ///////////////////
        // sla filter op //
        ///////////////////
        if(is_null($id) OR $request->input('actionbutton') == 'use'){
            $set = new Filter;
        }else{
            $set = Filter::sessionDB($request->session()->get('serpentes'))->find($id);
        }

        $set->setSessionDB($request->session()->get('serpentes'));
        
        if($request->input('actionbutton') == 'save'){
            $set->key = $request->input('key');
        }else{
            $set->key = '(naamloze filter)';
            $set->value2 = 'temp';
        }
        
        $set->value = base64_encode($request->input('value'));
        $set->save();

        if($request->input('actionbutton') == 'save'){
            return redirect('operations/filters')->with([
                'status' => 'Filter is opgeslagen',
                'type' => 'success'
            ]);
        }else{

            $prevurl = $request->input('prevurl');

            if(str_contains($prevurl, 'operations')){
                $redurl = url('operations/filter/'.$set->id);
            }

            // TODO: andere filter steps

            return redirect($redurl);
        }

    }

    public function delfilter(Request $request, $id){
        $filter = Filter::sessionDB($request->session()->get('serpentes'))->find($id);
        $filter->setSessionDB($request->session()->get('serpentes'));
        $filter->delete();

        return redirect('operations/filters')->with([
            'status' => 'Filter is verwijderd',
            'type' => 'info'
        ]);
    }

    ////////////////
    // Views CRUD //
    ////////////////
    public function views(Request $request){
        $data = [];
        $views = View::sessiondb($request->session()->get('serpentes'));
         
        if(isset($_GET['export'])){
            $views = $views->get();
            Excel::create('views', function($excel) use($views) {
                $excel->sheet('Sheet 1', function($sheet) use($views) {
                    $sheet->fromArray($views);
                });
            })->export('xls');
        }else{
            $views->orderBy('value3', 'asc')->orderBy('created_at', 'desc');
            $views->paginate(10);
            $data['views'] = $views->paginate(10);
            $data['request'] = $request;       
            $data['sorttype'] = 'views';

            return view('operations.views.all', $data);
        }
    }

    public function newview(Request $request){
        $data = [];
        $data['fields'] = Config::get('settings.operations_table');
        $data['categories'] = Config::get('settings.operations_table_categories');
        return view('operations.views.new', $data);
    }

    public function editview(Request $request, $id){
        $data = [];
        $view = View::sessionDB($request->session()->get('serpentes'))->find($id);
        $data['view'] = $view;
        $data['fields'] = Config::get('settings.operations_table');
        $data['categories'] = Config::get('settings.operations_table_categories');
        $data['inview'] = explode('|', $view->value);
        $data['id'] = $id;

        return view('operations.views.edit', $data);
    }

    public function saveview(Request $request, $id = null){

        // validation
        $err = [];
        if(trim($request->key) == '') $err['key'] = 'Geef de view een naam';
        if(count($request->tostoreinview) < 1) $err['value'] = 'Selecteer tenminste 1 veld voor deze view';
        if(count($err) > 0) return redirect()->back()->withErrors($err);

        if(is_null($id)){
            $view = new View;
        }else{
            $view = View::sessionDB($request->session()->get('serpentes'))->find($id);
        }

        $val = implode('|', $request->tostoreinview);

        $view->setSessionDB($request->session()->get('serpentes'));
        $view->key = $request->input('key');
        $view->value = $val;
        $view->save();

        return redirect('operations/views')->with([
            'status' => 'View is opgeslagen',
            'type' => 'success'
        ]);

    }

    public function delview(Request $request, $id){
        $view = View::sessionDB($request->session()->get('serpentes'))->find($id);
        $view->setSessionDB($request->session()->get('serpentes'));
        $view->delete();

        return redirect('operations/views')->with([
            'status' => 'View is verwijderd',
            'type' => 'info'
        ]);
    }

    ///////////////////////
    // Upload operations //
    ///////////////////////
    
    // specifiek voor kwr strabismus
    public function upload(Request $request){

        $data = [];

        $data['set_size'] = config('settings.set_size');
        $data['sets'] = ['eso', 'exo'];        

        $data['pool_operations_count'] = LocalOperation::sessionDB($request->session()->get('serpentes'))
            ->whereRaw('uploaded_at IS NULL')
            ->whereRaw('completed_at IS NOT NULL')
            ->where(['operation_type' => 'pool'])
            ->orderBy('created_at', 'asc')
            ->count();

        $data['eso_operations_count'] = LocalOperation::sessionDB($request->session()->get('serpentes'))
            ->whereRaw('uploaded_at IS NULL')
            ->whereRaw('completed_at IS NOT NULL')
            ->where(['operation_type' => 'eso'])
            ->orderBy('created_at', 'asc')
            ->count();

        $data['exo_operations_count'] = LocalOperation::sessionDB($request->session()->get('serpentes'))
            ->whereRaw('uploaded_at IS NULL')
            ->whereRaw('completed_at IS NOT NULL')
            ->where(['operation_type' => 'exo'])
            ->orderBy('created_at', 'asc')
            ->count();

        $data['demodb'] = $request->session()->get('demodb');
        
        if(!isset($_GET['action'])){

            if($data['exo_operations_count'] < $data['set_size'] AND $data['eso_operations_count'] < $data['set_size'] AND $data['pool_operations_count'] == 0){
                $data['btn'] = '<a href="#" class="btn btn-default disabled">Uploaden</a><br/><br/>
                <small class="alert alert-error">Er zijn te weinig operaties gevonden om een upload te kunnen starten</small>';
            }else{
                $data['btn'] = '<a href="'.url('operations/upload/step1').'" class="btn btn-primary">Uploaden</a>';
            }

            return view('operations.upload', $data);   
        }

        // Voor zowel eso als exo:
            // Kijk hoeveel operaties er geupload kunnen worden
            // Maak chunks van 50 operaties
            // Voor alle chuncks die 50 operaties bevatten:
                // Upload deze naar de database; geef ze een set id
                // Zet de velden in de database naar uploaded
        // Voor pool uploads:
            // Verplaats alles naar een online tabel
            // Zet de velden in de database naar uploaded
        
    }

    public function upload_pool(Request $request){

        $totaal = LocalOperation::sessionDB($request->session()->get('serpentes'))
                    ->whereRaw('uploaded_at IS NULL')
                    ->whereRaw('completed_at IS NOT NULL')
                    ->where(['operation_type' => 'pool'])
                    ->orderBy('created_at', 'asc')
                    ->count();

        $job = 'SubmitPoolJob';
        $in_batches_of = config('settings.set_size');
        $serpentes = $request->session()->get('serpentes');
        $msg = 'Stap 1/3: Er worden operaties geupload naar de pool';
        $error = false;
        $button = 'Verder';

        $data = [];
        $data['type'] = 'pool';
        $data['fields'] = config('settings.operations_table');

        return JobHelper::processJobs($job, $totaal, $in_batches_of, $serpentes, $msg, $error, $button, $data);

    }

    public function upload_eso(Request $request){

        $totaal = LocalOperation::sessionDB($request->session()->get('serpentes'))
                    ->whereRaw('uploaded_at IS NULL')
                    ->whereRaw('completed_at IS NOT NULL')
                    ->where(['operation_type' => 'eso'])
                    ->orderBy('created_at', 'asc')
                    ->count();

        $job = 'SubmitSetJob';
        $in_batches_of = config('settings.set_size');
        $serpentes = $request->session()->get('serpentes');
        $msg = 'Stap 2/3: Er worden eso operaties geupload';
        $error = false;
        $button = 'Verder';

        $data = [];
        $data['type'] = 'eso';
        $data['fields'] = config('settings.operations_table');

        return JobHelper::processJobs($job, $totaal, $in_batches_of, $serpentes, $msg, $error, $button, $data);

    }

    public function upload_exo(Request $request){

        $totaal = LocalOperation::sessionDB($request->session()->get('serpentes'))
                    ->whereRaw('uploaded_at IS NULL')
                    ->whereRaw('completed_at IS NOT NULL')
                    ->where(['operation_type' => 'exo'])
                    ->orderBy('created_at', 'asc')
                    ->count();

        $job = 'SubmitSetJob';
        $in_batches_of = config('settings.set_size');
        $serpentes = $request->session()->get('serpentes');
        $msg = 'Stap 3/3: Er worden exo operaties geupload';
        $error = false;
        $button = 'Verder';

        $data = [];
        $data['type'] = 'exo';
        $data['fields'] = config('settings.operations_table');

        return JobHelper::processJobs($job, $totaal, $in_batches_of, $serpentes, $msg, $error, $button, $data);
        
    }

}
