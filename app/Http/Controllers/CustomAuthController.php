<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

use App\Helpers\DatabaseCreator as DatabaseCreator;
use Chumper\Zipper\Zipper as Zipper;

use App\LocalOperation;
use App\OnlineOperation;
use App\Set;
use App\View;
use App\Filter;
use Slack;

use JobHelper;

use Illuminate\Support\Facades\Config;

class CustomAuthController extends Controller
{
	public function loginForm(){
        if(Auth::check()){
            return redirect('');
        }

		return view('customauth.login', [
            'versie' => Config::get('settings.version'),
            'maintenancemessage' => Config::get('settings.maintenancemessage'),
        ]);
	}

    public function login(Request $request)
    {
    	$creds = $request->only('userid', 'password');

        if (Auth::attempt($creds)) {
            
            // als het GEEN admin is moet een database verplicht zijn => anders gelijk weer uitloggen
            
            if($request->userid == 'admin'){
                return redirect('admin');
            }
            
            $databaseFailure = false;

            // wel een db? maak een schema en zet alle data er in 
            
            if(!is_null($request->file)){

                $serpentes = str_random(33);
                $request->session()->set('serpentes', $serpentes);
                $request->session()->set('userid', $request->userid);

                // Verplaatst het zipje naar een goede plaatst op de server
                $actual_zip = $zip_path = base_path() . '/storage/cache/' . $serpentes . '.zip';
                $request->file->move( base_path() . '/storage/cache/', $serpentes . '.zip' );

                $zipper = new Zipper;
                $zipper->make($actual_zip);

                // UITPROBEREN
                
                $dir = base_path() . '/storage/cache/'.$serpentes.'_extracted';
                mkdir($dir);

                $zip = new \ZipArchive;
                if ($zip->open($actual_zip) === TRUE) {
                    $zip->extractTo($dir);
                    $zip->close();
                } else {
                    $databaseFailure = true;
                }

                // get headers

                $content = decrypt($zipper->getFileContent('operation_headers.csv'));
                $reader = \League\Csv\Reader::createFromString($content);
                $headers = $reader->fetchOne(0);

                // get aantal
                $nbRows = (int) file_get_contents($dir.'/total_operations.txt');

                if(file_exists($dir.'/demodb')){
                    $request->session()->set('demodb', 'ja');
                }else{
                    $request->session()->set('demodb', 'nee');
                }

                // dd($request->file->move( base_path() . '/storage/cache/', str_random(33) . '.zip' ));

                Schema::create('operations_'.$serpentes, function(Blueprint $table){
                    $operations_table = Config::get('settings.operations_table');
                    DatabaseCreator::prepareTable($operations_table, $table);
                });

                // $databaseFailure = !($this->importTable('LocalOperation', 'operations', $request, $zipper));

                // Maak hier de views, filters en sets tabellen
                Schema::create('views_'.$serpentes, function(Blueprint $table){
                    $fields_of_table = Config::get('settings.extra_tables');
                    DatabaseCreator::prepareTable($fields_of_table, $table);
                });

                $databaseFailure = !($this->importTable('View', 'views', $request, $zipper));

                Schema::create('filters_'.$serpentes, function(Blueprint $table){
                    $fields_of_table = Config::get('settings.extra_tables');
                    DatabaseCreator::prepareTable($fields_of_table, $table);
                });

                $databaseFailure = !($this->importTable('Filter', 'filters', $request, $zipper));

                Schema::create('sets_'.$serpentes, function(Blueprint $table){
                    $fields_of_table = Config::get('settings.extra_tables');
                    DatabaseCreator::prepareTable($fields_of_table, $table);
                });

                $databaseFailure = !($this->importTable('Set', 'sets', $request, $zipper));

                Schema::create('settings_'.$serpentes, function(Blueprint $table){
                    $fields_of_table = Config::get('settings.extra_tables');
                    DatabaseCreator::prepareTable($fields_of_table, $table);
                });

                $databaseFailure = !($this->importTable('Setting', 'settings', $request, $zipper));

                // Voeg hier standaard filters en views toe als die nog niet in de database zitten
                
                $default_views_filters = Config::get('settings.default_views_filters');

                foreach($default_views_filters['filters'] as $name => $data){
                    if(Filter::sessiondb($serpentes)->where('key', $name)->count() == 0){
                        $filter = new Filter;
                        $filter->setSessionDB($serpentes);
                        $filter->setData([
                            'key' => $name,
                            'value' => $data,
                            'value2' => 'predefined',
                        ]);
                        $filter->save();
                    }
                }

                foreach($default_views_filters['views'] as $name => $data){
                    if(View::sessiondb($serpentes)->where('key', $name)->count() == 0){
                        $view = new View;
                        $view->setSessionDB($serpentes);
                        $view->setData([
                            'key' => $name,
                            'value' => $data,
                            'value2' => 'predefined',
                        ]);
                        $view->save();
                    }
                }

            }else{
                $databaseFailure = true;
            }
            
            if($databaseFailure){
                $this->logoutUser($request);

                return redirect()->back()
                ->withInput($request->only('userid'))
                ->withErrors([
                    'database' => 'Geen geldige database',
                ]);
            }

            Slack::send('Nieuw login door user: '.$request->userid.'. sessie: '.$serpentes);

            // Ga maar uploaden

            $job = 'UploadJob';
            // dd($job);
            $totaal = (($nbRows) > 0) ? ($nbRows) : 0;
            $in_batches_of = 100;
            $serpentes = $serpentes;
            $success = 'Uw operaties worden klaargezet';
            $error = false;
            $button = 'Ga verder';

            $data = [];
            $data['zipper'] = $zipper;
            $data['headers'] = $headers;
            $data['dir'] = $dir;

            return JobHelper::processJobs($job, $totaal, $in_batches_of, $serpentes, $success, $error, $button, $data);
            // return JobHelper::processJobs($job, 1, 1, $serpentes, $success, $error, $button, $data);

            // return redirect()->intended('dashboard');

        }else{
        	return redirect()->back()
            ->withInput($request->only('userid'))
            ->withErrors([
                'userid' => 'Invalide gebruikersgegevens',
            ]);
        }
    }

    // Only used by the function above
    private function importTable($class, $file, $request, $zipper){
        
        // Dit kan sneller met Maatsite/Excel

        if(!$zipper->contains($file.'.csv')) return false;
        if(trim($zipper->getFileContent($file.'.csv') == '')) return true;

        $content = decrypt($zipper->getFileContent($file.'.csv'));
        $reader = \League\Csv\Reader::createFromString($content);

        $headers = $reader->fetchOne(0);
        $res = $reader->setOffset(1)->fetch();

        // dd($headers);

        foreach($res as $row){

            $newrow = [];
            foreach($row as $r){
                if(trim($r) == ''){
                    $newrow[] = null;
                }else{
                    $newrow[] = $r;
                }
            }

            $data = array_combine($headers, $newrow);

            $a = 'App\\'.$class;
            $op = new $a();
            $op->setSessionDB($request->session()->get('serpentes'));
            $op->setData($data);
            $op->save();

        }

        return true;
    }

    public function logout(Request $request){
        if(!isset($_GET['continue']) AND Auth::user()->userid != 'admin'){
            return view('customauth.logout');
        }

        $this->logoutUser($request);

        return redirect('dashboard');
    }

    private function logoutUser(Request $request){
        $serpentes = $request->session()->get('serpentes');

        if(Schema::hasTable('operations_'.$serpentes)) Schema::drop('operations_'.$serpentes);
        if(Schema::hasTable('views_'.$serpentes)) Schema::drop('views_'.$serpentes);
        if(Schema::hasTable('filters_'.$serpentes)) Schema::drop('filters_'.$serpentes);
        if(Schema::hasTable('sets_'.$serpentes)) Schema::drop('sets_'.$serpentes);
        if(Schema::hasTable('settings_'.$serpentes)) Schema::drop('settings_'.$serpentes);

        Auth::logout();
        Slack::send('new user logout. sessie: '.$serpentes);
    }
}
