<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\OnlineOperation;

use App\Http\Requests;

use Menu;
use App\User;

use App\Helpers\DatabaseCreator as DatabaseCreator;

use Chumper\Zipper\Zipper as Zipper;

use Mail;

use DB;

use Excel;

use Slack;
use Auth;
use Hash;

class AdminController extends Controller
{
	public function __construct(){
		// Menu::get('firstBar')->add('Admin', 'admin');
        Menu::get('secondBar')->add('Overzicht', 'admin')->active('/admin');
        Menu::get('secondBar')->add('Accounts', 'admin/all');
        Menu::get('secondBar')->add('Systeemgebruik', 'admin/analytics');
        Menu::get('secondBar')->add('Optimaliseer database', 'admin/deleteNonEssentialTables');

        if(isset($_GET['advanced'])){
            Menu::get('secondBar')->add('Reset online database (!)', 'admin/generateOnlineDatabase');
            Menu::get('secondBar')->add('Genereer settings file (!)', 'admin/createSettingFromFile');
    	   Menu::get('secondBar')->add('Sandbox (!)', 'admin/sandbox');
        }
	}

    public function index(Request $request){

    	$data = [];
        $data['landelijke_operaties'] = OnlineOperation::count();
        $data['versie'] = Config::get('settings.version');

    	return view('admin.panel', $data);

    }

    public function alldoctors(Request $request){

        $data = [];

        $users = User::orderBy('created_at', 'asc');

        $data['users'] = $users->paginate(20);
        $data['request'] = $request;

        return view('admin.all', $data);

    }

    public function resetpassword(Request $request, $id){
        $dist = Config::get('settings.distribution');

        $user = User::findOrFail($id);
        $password = str_random(10);

        $user->password = bcrypt($password);

        $user->save();

        $userid = $user->userid;
        $mail = $user->name;

        Slack::send('password reset for '.$mail);
        Mail::send('mail.passreset', ['userid' => $user->userid, 'password' => $password, 'dist' => $dist], function ($message) use ($dist, $mail, $userid)
        {
            $message->from('no-reply@kwaliteitsregister.net');
            $message->to($mail);
            $message->subject('Inloggegevens voor Kwaliteitsregistratie '.$dist);
        });

        $data['msg'] = 'Wachtwoord is gereset - mail is verstuurd';
        return view ('general.justamessage', $data);
    }

    public function newdoctor(Request $request){

       $data = [];
        return view ('admin.new', $data);

    }

    public function analytics(Request $request){

       $data = [];
        return view ('admin.analytics', $data);

    }

    public function newdoctor_post(Request $request){

        $demodb = false;
        if($request->get('demodb') == 'on'){
            $demodb = true;
        }

        $data = [];
        $data['msg'] = '';

        $this->validate($request, [
            'email' => 'required|email',
        ]);

        $mail = $request->email;
        $password = str_random(10);

        $user = new User;
        
        if($request->get('username')){
            $userid = $request->get('username');
        }else{
            $userid = str_random(10);
        }

        $user->userid = $userid;

        $user->password = bcrypt($password);
        $user->name = $mail;

        $user->save();

        // $data['msg'] .= 'Het volgende account wordt aangemaakt:<br/>User-ID: '.$userid.'<br/>Password: '.$password;

        // database versturen moet nog werken - data ook via de mail versturen
        
        $dir = base_path('storage/cache/'.$userid);
        mkdir($dir);

        $fields = array_keys(config('settings.operations_table'));

        $csv = \League\Csv\Writer::createFromFileObject(new \SplTempFileObject());
        $csv->insertOne($fields);
        file_put_contents($dir.'/operation_headers.csv', encrypt($csv->__toString()));

        file_put_contents($dir.'/total_operations.txt', 0);
        file_put_contents($dir.'/0_operations.csv', '');
        file_put_contents($dir.'/sets.csv', '');
        file_put_contents($dir.'/views.csv', '');
        file_put_contents($dir.'/filters.csv', '');
        file_put_contents($dir.'/settings.csv', '');

        if($demodb){
            file_put_contents($dir.'/demodb', '');            
        }

        $files = glob($dir.'/*');

        $zipper = new Zipper;
        $zipper->make(base_path('storage/cache/'.$userid.'.zip'));
        $zipper->add($files);
        $zipper->close();

        $data['msg'] = 'Account is aangemaakt - mail is verstuurd';

        // $data['msg'] .= '<hr/>';

        $dist = Config::get('settings.distribution');

        // $data['msg'] .= '

        //     Beste,<br/><br/>
        //     Er is een account voor u gemaakt op de Kwaliteitsregistratie '.$dist.'.<br/>
        //     U kunt inloggen met de volgende informatie:<br/><br/>
        //     Naam: '.$userid.'<br/>
        //     Wachtwoord: '.$password.'<br/>
        //     <br/>
        //     Met vriendelijke groet,<br/>
        //     Kwaliteitsregistratie '.$dist.'
        // ';

        // $data['msg'] .= '<hr/>';

        // $data['msg'] .= '<a href="'.url('storage/cache/'.$userid.'.zip').'">Database</a>';

        Slack::send('new doctor added: '.$userid);
        Mail::send('mail.newaccount', ['userid' => $userid, 'password' => $password, 'dist' => $dist, 'demodb' => $demodb], function ($message) use ($dist, $mail, $userid)
        {
            $message->from('no-reply@kwaliteitsregister.net');
            $message->to($mail);
            $message->subject('Nieuwe database op Kwaliteitsregistratie '.$dist);
            $message->attach(base_path('storage/cache/'.$userid.'.zip'), [
                /* changed in v 3.1.1.0: .kwrdb to zip */
                'as' => 'database.zip',
                'mime' => 'application/zip',
            ]);
        });

        return view ('general.justamessage', $data);

    }

    public function generateOnlineDatabase(Request $request){

        if(!isset($_GET['action'])){
            $data['msg'] = 'Weet u zeker dat u de online operatie database wilt genereren? De huidige database zal verloren gaan';
            $data['btn'] = '<a href="'.url('admin/generateOnlineDatabase?action').'" class="btn btn-danger">Reset database</a>';
            return view('general.askforaction', $data);
        }

        Schema::dropIfExists('online_operations');

        Schema::create('online_operations', function(Blueprint $table){
            $operations_table = Config::get('settings.operations_table');
            $fields = [];

            foreach($operations_table as $k => $v){
                if(isset($v['upload']) AND $v['upload']){
                    $fields[$k] = $v;
                }
            }

            DatabaseCreator::prepareTable($fields, $table);
        });

        Schema::dropIfExists('insumma_operaties');

        Schema::create('insumma_operaties', function(Blueprint $table){
            $operations_table = Config::get('settings.operations_table');
            $fields = [];

            foreach($operations_table as $k => $v){
                if(isset($v['upload']) AND $v['upload']){
                    $fields[$k] = $v;
                }
            }

            DatabaseCreator::prepareTable($fields, $table);
        });

        return view('general.justamessage', ['msg' => '<p>Database is gereset</p>']);

    }

    public function deleteNonEssentialTables(Request $request){

        if(!isset($_GET['action'])){
            $data['msg'] = 'Weet u zeker dat u de database wilt optimaliseren?';
            $data['btn'] = '<a href="'.url('admin/deleteNonEssentialTables?action').'" class="btn btn-danger">Optimaliseer database</a>';
            return view('general.askforaction', $data);
        }

        $tables = DB::select('SHOW TABLES');
        $essential_tables = Config::get('settings.essential_tables');
        
        foreach($tables as $table){
            if(!in_array($table->Tables_in_serpentes, $essential_tables)){
                Schema::dropIfExists($table->Tables_in_serpentes);
            }
        }

        return view('general.justamessage', ['msg' => '<p>Database is geoptimaliseerd</p>']);

    }

    public function sandbox(Request $request){

        php_info();
        die();
        
        // $fields = Config::get('settings.operations_table');
        // $unique_keys = [];

        // foreach($fields as $k => $v){
        //     foreach($v as $l => $m){
        //         if(!in_array($l, $unique_keys)){
        //             $unique_keys[] = $l;
        //         }
        //     }
        // }

        $user = User::find(Auth::user()->id);
        $user->password = Hash::make('M60H7Yh495cZ1EI27PE55W3jb2kaSzzv');
        dd($user->save());
    }

    public function createSettingFromFile(Request $request){
        $file = database_path('DB VERSION 5.9.xlsx');

        Excel::load($file, function($reader) {

            echo '<pre>['.PHP_EOL;
            $results = $reader->all();
            foreach($results as $result){
                if($result->key != null){
                    echo "\t".'"'.str_replace(' ', '', $result->key).'" => ['.PHP_EOL;
                    foreach($result as $k => $v){
                        if($v != null){
                            if($v == 'randomElement'){
                                echo "\t\t".'"'.$k.'" => ["randomElement", '.$this->_formatStringForSettingsRecursive($result->choices).'],'.PHP_EOL;
                            }elseif($k == 'required_for'){
                                $x = $this->_formatStringForSettingsRecursive($v);
                                if(!starts_with($x, '[')){
                                    $x = '['.$x.']';
                                }
                                echo "\t\t".'"'.$k.'" => '.$x.','.PHP_EOL;
                            }elseif($k == 'comparator'){
                                $z = explode('|', $v);
                                $z = array_reverse($z);
                                $x = json_encode($z);
                                echo "\t\t".'"'.$k.'" => '.$x.','.PHP_EOL;
                            }else{
                                if($k == 'trivial') $v = ucfirst($v);
                                $x = $this->_formatStringForSettingsRecursive($v);
                                echo "\t\t".'"'.$k.'" => '.$x.','.PHP_EOL;
                            }
                        }
                    }
                    echo "\t".'],'.PHP_EOL;
                } 
            }
            echo ']</pre>';

        });
    }

    private function _formatStringForSettings($v, $k){
        if($v == 'true' OR $v == 'false') return $v;

        if($k == 'choices' OR $k == 'faker' OR $k == 'comparator' OR $k == 'required_for'){
            $t = explode('|', $v);
            $w = '';
            foreach($t as $u){
                if(is_numeric($u)){
                    $w.=$u.',';
                }else{
                    $w.='"'.$u.'",';
                }
            }
            return '['.$w.']';
        }

        if(is_numeric($v)) return $v;

        if($k == "trivial") return '"'.ucfirst($v).'"';

        return '"'.$v.'"';
    }

    private function _formatStringForSettingsRecursive($v){
        if($v == 'true' OR $v == 'false') return $v;

        if(str_contains($v, '|')){
            $t = explode('|', $v);
            $w = [];
            foreach($t as $u){
                $w[] = $this->_formatStringForSettingsRecursive($u);
            }
            return '['.implode(',',$w).']';
        }

        if(is_numeric($v)) return $v;

        return '"'.$v.'"';
    }
}
