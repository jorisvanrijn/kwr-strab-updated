<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Menu;
use App\LocalOperation;
use Faker;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use FilterHelper;
use Excel;
use Slack;
use JobHelper;

use Artisan;

use App\Setting;
use App\User;
use App\InsummaUser;
use App\InsummaOperation;

use DB;

use DateTime;

use Carbon\Carbon;

class ToolsController extends Controller
{

    public function __construct(){
    	// Menu::get('secondBar')->add('Tools', 'tools');
        // Menu::get('secondBar')->add('Faker', 'tools/faker');
        // Menu::get('secondBar')->add('Importeer', 'tools/import');
        // Menu::get('secondBar')->add('Exporteer', 'tools/export');
        // Menu::get('secondBar')->add('Upload operaties', 'operations/upload');
        // Menu::get('secondBar')->add('Wachtrij', 'tools/currentjob');
    }

    public function index(Request $request){
    	return view('tools.overzicht');
    }

    public function faker(Request $request){
        // Event::fire(new JobFinished("Job is klaar"));
        // dd(LaravelPusher::getSettings());
        // dd(LaravelPusher::trigger( 'test-channel',
        //               'test-event', 
        //               array('text' => 'Preparing the Pusher Laracon.eu workshop!'), null, true));
        $data = [];
        $data['demodb'] = $request->session()->get('demodb');
    	return view('tools.faker', $data);
    }

    public function import(Request $request){
        $data = [];
        $data['username'] = $request->session()->get('userid');

        if($request->isMethod('post')){

            if(!$request->get('username') OR
                !$request->get('authcode')){

                return redirect()->back()->with([
                    'status' => 'Vul alle velden in',
                    'type' => 'error'
                ]);
            }

            if(InsummaUser::where([
                'name' => $request->get('username'),
                'authcode' => $request->get('authcode')
            ])->count() == 0){
                return redirect()->back()->with([
                    'status' => 'Foutieve gebruikersnaam of authenticatiecode',
                    'type' => 'error'
                ]);
            }

            $data['account'] = $request->get('username');

            $data['found_operations'] = (InsummaOperation::where([
                'summa_pract_username' => $request->get('username')
            ])->count());

            return view('tools.import.step2', $data);
        }else{
            return view('tools.import.step1', $data);
        }
        
    }

    public function process_import(Request $request){

        if(!$request->get('account')){
            return redirect('operations')->with([
                'status' => '0 operaties zijn geimporteerd',
                'type' => 'error'
            ]);
        }

        $account = $request->get('account');

        $aantal = InsummaOperation::where([
            'summa_pract_username' => $account
        ])->count();

        $operaties = InsummaOperation::where([
            'summa_pract_username' => $account
        ])->get();

        $fields = Config::get('settings.operations_table');
        Carbon::setLocale('nl');

        foreach($operaties as $operatie){

            $operation = new LocalOperation;
            $operation->setSessionDB($request->session()->get('serpentes'));

            $data = [];
            $_errors = [];

            $data['type_overige_deviaties'] = 'nvt';

            foreach($fields as $k => $v){            
                if($v['is_actual_field']){
                    if($v['laravel_db_function'] == 'date'){
                        try{
                            if(isset($operatie->{$k})){
                                $data[$k] = Carbon::parse($operatie->{$k}, 'Europe/Amsterdam')->format('Y-m-d');
                            }else{
                                $data[$k] = $operatie->{$k};
                            }
                        }catch(\Exception $e){
                            $_errors[$k] = 'Ongeldig data formaat';
                        }
                    }elseif($v['laravel_db_function'] == 'boolean'){
                        $data[$k] = ($operatie->{$k} == 0) ? 0 : 1;

                        // 3.1.0.5; als een veld verplicht is voor uploaden en deze is niet ingevuld; zet de waarde dan naar 0
                        
                        if(isset($v['required_for']) AND in_array('upload', $v['required_for'])){
                            if(is_null($operatie->{$k}) OR trim($operatie->{$k}) == ''){
                                $data[$k] = 0;
                            }
                        }

                    }else{
                        $data[$k] = $operatie->{$k};
                    }
                }
            }

            $operation->setData($data);

            // In 3.1.0.4 toegevoegd, weer verwijderd in 3.1.0.5
            // $operation->forceValid = true;

            $operation->save();

        }

        return redirect('operations')->with([
            'status' => $aantal.' operaties zijn geimporteerd',
            'type' => 'success'
        ]);

    }

    public function credentials(Request $request){
        $data = [];
        $data['username'] = $request->session()->get('userid');

        if($request->isMethod('post')){
            
            if(!$request->get('old_pass') OR
                !$request->get('new_pass') OR
                !$request->get('new_pass_2') OR
                !$request->get('old_pass')){

                return redirect()->back()->with([
                    'status' => 'Vul alle velden in',
                    'type' => 'error'
                ]);

            }

            if($request->get('new_pass') != $request->get('new_pass_2')){
                return redirect()->back()->with([
                    'status' => 'De nieuwe wachtwoorden waren niet gelijk',
                    'type' => 'error'
                ]);
            }

            if(!Hash::check($request->get('old_pass'), Auth::user()->password)){
                return redirect()->back()->with([
                    'status' => 'Het oude wachtwoord is niet correct',
                    'type' => 'error'
                ]);
            }

            if(User::where('userid', $request->get('username'))->count() > 0 AND $request->get('username') != $request->session()->get('userid')){
                return redirect()->back()->with([
                    'status' => 'De gekozen gebruikersnaam is niet beschikbaar',
                    'type' => 'error'
                ]);
            }
            
            $user = User::findOrFail(Auth::user()->id);
            $user->userid = $request->get('username');
            $user->password = Hash::make($request->get('new_pass'));
            $user->save();

            $request->session()->set('userid', $request->get('username'));

            return redirect('tools')->with([
                'status' => 'Gebruikersnaam en/of wachtwoord zijn opgeslagen',
                'type' => 'success'
            ]);
        }else{
            
            return view('tools.username', $data);
        }
    }

    public function followupletter(Request $request, $id = -1){
        $data = [];

        if($id < 0){

            if($request->isMethod('post')){
                $data['operation'] = LocalOperation::sessionDB($request->session()->get('serpentes'))->where([
                    'patientnummer' => $request->get('patientnummer')
                ])->first();

                // dd($data['operation']);
            }else{
                return view('tools.followup.select', $data);
            }

        }else{
            
            $data['operation'] = LocalOperation::sessionDB($request->session()->get('serpentes'))->find($id);

        }
        
        if($data['operation'] == null){
            return redirect()->back()->with([
                'status' => 'Pati&euml;nt niet gevonden',
                'type' => 'error'
            ]);
        }

        return view('tools.followup.letter', $data);
    }

    public function visitationreport(Request $request){

        $data = [];
        
        if($request->isMethod('post')){

            $fields = Config::get('settings.operations_table');

            $data['datum_start'] = $request->get('datum_start');
            $data['datum_eind'] = $request->get('datum_eind');

            $query = LocalOperation::sessionDB($request->session()->get('serpentes'))->where([
                ['operatiedatum', '>=', $data['datum_start']],
                ['operatiedatum', '<=', $data['datum_eind']],
            ]);

            $data['user_id'] = $request->session()->get('userid');
            $data['operations_count'] = $query->count();

            $data['operation_type'] = LocalOperation::sessionDB($request->session()->get('serpentes'))->groupBy('operation_type')->select('operation_type', DB::raw('count(*) as total'))->get();

            $data['type_esodeviaties'] = LocalOperation::sessionDB($request->session()->get('serpentes'))->groupBy('type_esodeviaties')->select('type_esodeviaties', DB::raw('count(*) as total'))->get();

            $data['type_exodeviaties'] = LocalOperation::sessionDB($request->session()->get('serpentes'))->groupBy('type_exodeviaties')->select('type_exodeviaties', DB::raw('count(*) as total'))->get();

            $data['type_overige_deviaties'] = LocalOperation::sessionDB($request->session()->get('serpentes'))->groupBy('type_overige_deviaties')->select('type_overige_deviaties', DB::raw('count(*) as total'))->get();

            return view('tools.visitation.report', $data);
        }else{
            return view('tools.visitation.select', $data);
        }

    }

    public function export(Request $request){
        $data = [];
        $data['fields'] = Config::get('settings.operations_table');
        $data['request'] = $request;
        $data['filtered'] = false;

        if($request->has('filter')){
            $data['filtered'] = true;

            // $params = ($request->query->all());
            $params['filter'] = $request->input('filter');
            
            $data['xls_url'] = url($request->path().'?'.http_build_query(array_merge($params, ['export_format' => 'xls'])));
            $data['xlsx_url'] = url($request->path().'?'.http_build_query(array_merge($params, ['export_format' => 'xlsx'])));
            $data['csv_url'] = url($request->path().'?'.http_build_query(array_merge($params, ['export_format' => 'csv'])));

            $operations = FilterHelper::filter_query($request, 'lokaal', $request->input('filter'))->count();
            $data['operations'] = $operations;

        }

        if($request->has('export_format')){

            $operations = FilterHelper::filter_query($request, 'lokaal', $request->input('filter'))->get();
            
            Excel::create('operations', function($excel) use($operations) {
                $excel->sheet('Sheet 1', function($sheet) use($operations) {
                    $sheet->fromArray($operations);
                });
            })->export($request->export_format);

        }

        return view('tools.export', $data);
    }

    public function fakerfill(Request $request){
        // $resp = Artisan::queue('faker:fill', [
        //        'aantal' => $request->get('aantal'),
        //        'serpentes' => $request->session()->get('serpentes'),
        //        '--queue' => 'default'
        //    ]);

        $job = 'FakerFillJob';
        $totaal = $request->get('aantal');
        $in_batches_of = 50;
        $serpentes = $request->session()->get('serpentes');
        $msg = 'Er worden '.$totaal.' operaties aan uw database toegevoegd.';
        $error = 'Vul een geldig getal > 0 in';
        $button = 'Operaties bekijken';

        return JobHelper::processJobs($job, $totaal, $in_batches_of, $serpentes, $msg, $error, $button);

    }

    public function currentjob(Request $request){
        $serpentes = $request->session()->get('serpentes');

        $job_lock = Setting::sessiondb($serpentes)->firstOrNew(['key' => 'job_lock']);
        $job_lock->setSessionDB($serpentes);
        
        if($job_lock->value == 'on'){
            $data = json_decode($job_lock->value2, true);
            return view('general.waitforpush', $data);
        }else{
            $data = [];
            return view('general.prettymessage', ['msg' => '<h3>Wachtrij leeg</h3>Er worden geen opdrachten voor u uitgevoerd']);
        }
    }
}
