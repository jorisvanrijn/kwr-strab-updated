<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

use App\Http\Requests;

use Session;
use Menu;
use Auth;

use App\LocalOperation;
use App\OnlineOperation;
use App\Set;
use App\View;
use App\Filter;
use App\Setting;
use Slack;

use Chumper\Zipper\Zipper as Zipper;

use JobHelper;

class DashboardController extends Controller
{
    public function __construct(){
        // Make the secondary menu
        Menu::get('secondBar')->add('Dashboard', 'dashboard');
        // Menu::get('secondBar')->add('Operaties uploaden', 'operations/upload');
        Menu::get('secondBar')->add('Help', 'help');
        
    	// Menu::get('secondBar')->add('Instellingen', 'dashboard/settings');
    }

    public function index(Request $request){
        if(Auth::user()->userid == 'admin'){
            return redirect('admin');
        }else{
            return redirect('dashboard');
        }
    }

    // public function waitforpushdemo(){
    //     return view('general.waitforpush', [
    //         'status' => 'Demo van het laad scherm',
    //         'type' => 'info',
    //         'total' => 100,
    //         'serpentes' => 'test_channel',
    //         'button' => 'Ga door'
    //     ]);
    // }

    public function stats(Request $request){
                
    	$data = [];

        $data['eigen_operaties'] = LocalOperation::sessiondb($request->session()->get('serpentes'))->count();
        $data['eigen_sets'] = Set::sessiondb($request->session()->get('serpentes'))->count();
        $data['landelijke_operaties'] = OnlineOperation::count();
        $data['online_eigen_operaties'] = LocalOperation::sessionDB($request->session()->get('serpentes'))->whereRaw('uploaded_at IS NOT NULL')->count();

        $data['pool_operations_count'] = LocalOperation::sessionDB($request->session()->get('serpentes'))->where(['operation_type' => 'pool'])->count();
        $data['eso_operations_count'] = LocalOperation::sessionDB($request->session()->get('serpentes'))->where(['operation_type' => 'eso'])->count();
        $data['exo_operations_count'] = LocalOperation::sessionDB($request->session()->get('serpentes'))->where(['operation_type' => 'exo'])->count();

        $data['set_size'] = 50;
        $data['sets'] = ['eso', 'exo'];        

        $data['pool_operations_count_upl'] = LocalOperation::sessionDB($request->session()->get('serpentes'))
            ->whereRaw('uploaded_at IS NULL')
            ->whereRaw('completed_at IS NOT NULL')
            ->where(['operation_type' => 'pool'])
            ->orderBy('created_at', 'asc')
            ->count();

        $data['eso_operations_count_upl'] = LocalOperation::sessionDB($request->session()->get('serpentes'))
            ->whereRaw('uploaded_at IS NULL')
            ->whereRaw('completed_at IS NOT NULL')
            ->where(['operation_type' => 'eso'])
            ->orderBy('created_at', 'asc')
            ->count();

        $data['exo_operations_count_upl'] = LocalOperation::sessionDB($request->session()->get('serpentes'))
            ->whereRaw('uploaded_at IS NULL')
            ->whereRaw('completed_at IS NOT NULL')
            ->where(['operation_type' => 'exo'])
            ->orderBy('created_at', 'asc')
            ->count();

        $data['versie'] = Config::get('settings.version');

        $data['demodb'] = $request->session()->get('demodb');

    	return view('dashboard.stats', $data);

    }

    public function download(Request $request){

        $serpentes = $request->session()->get('serpentes');

        $db_version = Setting::sessiondb($serpentes)->firstOrNew(['key' => 'db_version']);
        $db_version->setSessionDB($serpentes);
        $db_version->value = Config::get('settings.dbversion');
        $db_version->save();

        $dir = base_path('storage/cache/'.$serpentes);

        //$this->exportTable('operations', LocalOperation::sessiondb($serpentes)->get(), $request, $dir);        
        $this->exportTable('sets', Set::sessiondb($serpentes)->get(), $request, $dir);        
        $this->exportTable('views', View::sessiondb($serpentes)->get(), $request, $dir);        
        $this->exportTable('filters', Filter::sessiondb($serpentes)->get(), $request, $dir);        
        $this->exportTable('settings', Setting::sessiondb($serpentes)->get(), $request, $dir);   

        // Eerst de file aanmaken met header
        $csv = \League\Csv\Writer::createFromFileObject(new \SplTempFileObject());
        $csv->insertOne(\Schema::getColumnListing('operations_'.$serpentes));
        file_put_contents($dir.'/operation_headers.csv', encrypt($csv->__toString()));

        // Dan alle jobs in een queue zetten
        $aantal = LocalOperation::sessiondb($serpentes)->count();

        file_put_contents($dir.'/total_operations.txt', $aantal);

        $job = 'DownloadJob';
        $totaal = $aantal;
        $in_batches_of = 100;
        $serpentes = $serpentes;
        $success = 'Uw download wordt klaar gemaakt';
        $error = false;
        $button = 'Database downloaden';

        $data = [];
        $data['dir'] = $dir;
        $data['zip'] = base_path('storage/cache/'.$serpentes.'.zip');

        return JobHelper::processJobs($job, $totaal, $in_batches_of, $serpentes, $success, $error, $button, $data);

    }

    /* changed in v 3.1.1.0 */
    public function download_zip(Request $request){
        return response()->download(base_path('storage/cache/'.$request->session()->get('serpentes').'.zip'), date('Y-m-d_H_i_s').'.zip');
    }

    // Only used by the function above
    private function exportTable($name, $data, $request, $dir){
        $csv = \League\Csv\Writer::createFromFileObject(new \SplTempFileObject());

        $csv->insertOne(\Schema::getColumnListing($name.'_'.$request->session()->get('serpentes')));

        foreach ($data as $operation) {
            $csv->insertOne($operation->toArray());
        }

        if(!file_exists($dir)){
            mkdir($dir);
        }

        file_put_contents($dir.'/'.$name.'.csv', encrypt($csv->__toString()));
    }

    // Deprecated
    public function download_old(Request $request){

        $db_version = Setting::sessiondb($request->session()->get('serpentes'))->firstOrNew(['key' => 'db_version']);
        $db_version->setSessionDB($request->session()->get('serpentes'));
        $db_version->value = Config::get('settings.dbversion');
        $db_version->save();

        $dir = base_path('storage/cache/'.$request->session()->get('serpentes'));

        $this->exportTable('operations', LocalOperation::sessiondb($request->session()->get('serpentes'))->get(), $request, $dir);        
        $this->exportTable('sets', Set::sessiondb($request->session()->get('serpentes'))->get(), $request, $dir);        
        $this->exportTable('views', View::sessiondb($request->session()->get('serpentes'))->get(), $request, $dir);        
        $this->exportTable('filters', Filter::sessiondb($request->session()->get('serpentes'))->get(), $request, $dir);        
        $this->exportTable('settings', Setting::sessiondb($request->session()->get('serpentes'))->get(), $request, $dir);        
        
        $files = glob($dir.'/*');

        $zipper = new Zipper;
        $zipper->make(base_path('storage/cache/'.$request->session()->get('serpentes').'.zip'));
        $zipper->add($files);
        $zipper->close();

        Slack::send('new database download. sessie: '.$request->session->get('serpentes'));

        /* changed in v 3.1.1.0 */
        return response()->download(base_path('storage/cache/'.$request->session()->get('serpentes').'.zip'), date('Y-m-d_H_i_s').'.zip');

    }
}
