<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 					['middleware' => ['auth', 'menu'], 	'uses' => 'DashboardController@index']);
Route::get('dashboard', 			['middleware' => ['auth', 'menu'], 	'uses' => 'DashboardController@stats']);
Route::get('dashboard/download', 	['middleware' => ['auth', 'menu'], 	'uses' => 'DashboardController@download']);
Route::get('dashboard/download/zip', 	['middleware' => ['auth', 'menu'], 	'uses' => 'DashboardController@download_zip']);
// Route::get('dashboard/waitforpushdemo', 	['middleware' => ['auth', 'menu'], 	'uses' => 'DashboardController@waitforpushdemo']);

////////////////
// Operations //
////////////////
Route::get('operations', 			['middleware' => ['auth', 'menu'], 	'uses' => 'OperationsController@index']);
Route::get('operations/filter/{id}', 			['middleware' => ['auth', 'menu'], 	'uses' => 'OperationsController@index']);

Route::get('operations/new',		['middleware' => ['auth', 'menu'], 	'uses' => 'OperationsController@newoperation']);
Route::post('operations/new',		['middleware' => ['auth', 'menu'], 	'uses' => 'OperationsController@saveoperation']);
Route::get('operations/view/{id}',	['middleware' => ['auth', 'menu'], 	'uses' => 'OperationsController@viewoperation']);
Route::get('operations/edit/{id}',	['middleware' => ['auth', 'menu'], 	'uses' => 'OperationsController@editoperation']);
Route::post('operations/edit/{id}',	['middleware' => ['auth', 'menu'], 	'uses' => 'OperationsController@saveoperation']);
Route::get('operations/del/{id}',	['middleware' => ['auth', 'menu'], 	'uses' => 'OperationsController@deloperation']);

// Sets
Route::get('operations/sets', 		['middleware' => ['auth', 'menu'], 	'uses' => 'OperationsController@sets']);
Route::get('operations/edit_set/{id}',	['middleware' => ['auth', 'menu'], 	'uses' => 'OperationsController@editset']);
Route::post('operations/edit_set/{id}',	['middleware' => ['auth', 'menu'], 	'uses' => 'OperationsController@saveset']);
// Route::get('operations/del_set/{id}',	['middleware' => ['auth', 'menu'], 	'uses' => 'OperationsController@delset']);

// Filters
Route::get('operations/filters', 	['middleware' => ['auth', 'menu'], 	'uses' => 'OperationsController@filters']);
Route::get('operations/new_filter', ['middleware' => ['auth', 'menu'], 	'uses' => 'OperationsController@newfilter']);
Route::post('operations/new_filter', ['middleware' => ['auth', 'menu'], 	'uses' => 'OperationsController@savefilter']);
Route::get('operations/edit_filter/{id}',	['middleware' => ['auth', 'menu'], 	'uses' => 'OperationsController@editfilter']);
Route::post('operations/edit_filter/{id}', ['middleware' => ['auth', 'menu'], 	'uses' => 'OperationsController@savefilter']);
Route::get('operations/del_filter/{id}',	['middleware' => ['auth', 'menu'], 	'uses' => 'OperationsController@delfilter']);

// Views
Route::get('operations/views', 	['middleware' => ['auth', 'menu'], 	'uses' => 'OperationsController@views']);
Route::get('operations/new_view', ['middleware' => ['auth', 'menu'], 	'uses' => 'OperationsController@newview']);
Route::post('operations/new_view', ['middleware' => ['auth', 'menu'], 	'uses' => 'OperationsController@saveview']);
Route::get('operations/edit_view/{id}', ['middleware' => ['auth', 'menu'], 	'uses' => 'OperationsController@editview']);
Route::post('operations/edit_view/{id}', ['middleware' => ['auth', 'menu'], 	'uses' => 'OperationsController@saveview']);
Route::get('operations/del_view/{id}',	['middleware' => ['auth', 'menu'], 	'uses' => 'OperationsController@delview']);

// Upload
Route::get('operations/upload',		['middleware' => ['auth', 'menu'], 	'uses' => 'OperationsController@upload']);
Route::get('operations/upload/step1',		['middleware' => ['auth', 'menu'], 	'uses' => 'OperationsController@upload_pool']);
Route::get('operations/upload/step2',		['middleware' => ['auth', 'menu'], 	'uses' => 'OperationsController@upload_eso']);
Route::get('operations/upload/step3',		['middleware' => ['auth', 'menu'], 	'uses' => 'OperationsController@upload_exo']);

//////////////
// Analysis //
//////////////
Route::get('analysis',				['middleware' => ['auth', 'menu'], 	'uses' => 'AnalysisController@index']);
Route::get('analysis/set',		['middleware' => ['auth', 'menu'], 	'uses' => 'AnalysisController@set']);
Route::get('analysis/do/set/{id}',		['middleware' => ['auth', 'menu'], 	'uses' => 'AnalysisController@do_set']);
Route::get('analysis/show/set',	['middleware' => ['auth', 'menu'], 	'uses' => 'AnalysisController@show_set_analyse']);
Route::get('analysis/local',		['middleware' => ['auth', 'menu'], 	'uses' => 'AnalysisController@local']);
Route::get('analysis/do/local/{id}',		['middleware' => ['auth', 'menu'], 	'uses' => 'AnalysisController@do_local']);
Route::get('analysis/show/local',	['middleware' => ['auth', 'menu'], 	'uses' => 'AnalysisController@show_list_analyse']);
Route::get('analysis/national',		['middleware' => ['auth', 'menu'], 	'uses' => 'AnalysisController@national']);
Route::get('analysis/do/national/{id}',		['middleware' => ['auth', 'menu'], 	'uses' => 'AnalysisController@do_national']);
Route::get('analysis/show/national',	['middleware' => ['auth', 'menu'], 	'uses' => 'AnalysisController@show_list_analyse']);
Route::get('analysis/localvsnational',		['middleware' => ['auth', 'menu'], 	'uses' => 'AnalysisController@localvsnational']);
Route::get('analysis/do/localvsnational/{id}',		['middleware' => ['auth', 'menu'], 	'uses' => 'AnalysisController@do_localvsnational']);
Route::get('analysis/show/localvsnational',	['middleware' => ['auth', 'menu'], 	'uses' => 'AnalysisController@show_list_analyse']);

///////////
// Tools //
///////////
Route::get('tools',					['middleware' => ['auth', 'menu'], 	'uses' => 'ToolsController@index']);
Route::get('tools/faker',			['middleware' => ['auth', 'menu'], 	'uses' => 'ToolsController@faker']);
Route::post('tools/faker',			['middleware' => ['auth', 'menu'], 	'uses' => 'ToolsController@fakerfill']);
Route::get('tools/export',			['middleware' => ['auth', 'menu'], 	'uses' => 'ToolsController@export']);
Route::get('tools/currentjob',			['middleware' => ['auth', 'menu'], 	'uses' => 'ToolsController@currentjob']);
Route::get('tools/followupletter',	['middleware' => ['auth', 'menu'], 	'uses' => 'ToolsController@followupletter']);
Route::post('tools/followupletter',	['middleware' => ['auth', 'menu'], 	'uses' => 'ToolsController@followupletter']);
Route::get('tools/followupletter/{id}',	['middleware' => ['auth', 'menu'], 	'uses' => 'ToolsController@followupletter']);
Route::get('tools/visitationreport',	['middleware' => ['auth', 'menu'], 	'uses' => 'ToolsController@visitationreport']);
Route::post('tools/visitationreport',	['middleware' => ['auth', 'menu'], 	'uses' => 'ToolsController@visitationreport']);
Route::get('tools/credentials',	['middleware' => ['auth', 'menu'], 	'uses' => 'ToolsController@credentials']);
Route::post('tools/credentials',	['middleware' => ['auth', 'menu'], 	'uses' => 'ToolsController@credentials']);
Route::get('tools/import',	['middleware' => ['auth', 'menu'], 	'uses' => 'ToolsController@import']);
Route::post('tools/import',	['middleware' => ['auth', 'menu'], 	'uses' => 'ToolsController@import']);
Route::post('tools/process_import',	['middleware' => ['auth', 'menu'], 	'uses' => 'ToolsController@process_import']);

//////////
// Help //
//////////
Route::get('help',					['middleware' => ['auth', 'menu'], 	'uses' => 'HelpController@index']);

/////////////
// General //
/////////////
Route::get('login', 	'CustomAuthController@loginForm');
Route::post('login', 	'CustomAuthController@login');
Route::get('logout', 				['middleware' => ['auth', 'menu'], 	'uses' => 'CustomAuthController@logout']);

Route::get('ajax/setlastview/{id}',	['middleware' => ['auth', 'menu'], 	'uses' => 'AjaxController@setlastview']);
Route::post('ajax/setorder', ['uses' => 'AjaxController@setorder']);

///////////
// Admin //
///////////
Route::get('admin',					['middleware' => ['auth', 'menu'], 	'uses' => 'AdminController@index']);
Route::get('admin/all',				['middleware' => ['auth', 'menu'], 	'uses' => 'AdminController@alldoctors']);
Route::get('admin/new',				['middleware' => ['auth', 'menu'], 	'uses' => 'AdminController@newdoctor']);
Route::get('admin/analytics',				['middleware' => ['auth', 'menu'], 	'uses' => 'AdminController@analytics']);
Route::post('admin/new',			['middleware' => ['auth', 'menu'], 	'uses' => 'AdminController@newdoctor_post']);
Route::get('admin/generateOnlineDatabase', ['middleware' => ['auth', 'menu'], 	'uses' => 'AdminController@generateOnlineDatabase']);
Route::get('admin/deleteNonEssentialTables', ['middleware' => ['auth', 'menu'], 	'uses' => 'AdminController@deleteNonEssentialTables']);
Route::get('admin/sandbox', ['middleware' => ['auth', 'menu'], 	'uses' => 'AdminController@sandbox']);
Route::get('admin/createSettingFromFile', ['middleware' => ['auth', 'menu'], 	'uses' => 'AdminController@createSettingFromFile']);
Route::get('admin/resetpassword/{id}', ['middleware' => ['auth', 'menu'], 	'uses' => 'AdminController@resetpassword']);