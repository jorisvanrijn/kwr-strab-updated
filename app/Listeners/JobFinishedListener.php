<?php

namespace App\Listeners;

use App\Events\JobFinished;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class JobFinishedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  JobFinished  $event
     * @return void
     */
    public function handle(JobFinished $event)
    {
        //
    }
}
