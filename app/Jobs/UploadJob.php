<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Carbon\Carbon;

use App\LocalOperation;
use Faker;
use Illuminate\Support\Facades\Config;

use Chumper\Zipper\Zipper as Zipper;

use Slack;
use Excel;

use Illuminate\Support\Facades\DB;

class UploadJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($args, $last = false)
    {
        parent::__construct($args, $last);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        // Slack::send('Ik doe echt een UploadJob');

        $offset = round($this->iteration * $this->per_iteration);
        $limit = $this->this_batch_count;

        // Slack::send('In totaal zijn er '.$this->grand_total.'. Nu process ik vanaf '.$offset.' tot '.($offset+$limit).'. Dat is iteratie '.($this->iteration+1).' van de '.$this->max_iteration.'.');

        // Action of job below
        
        $this->jobLogic($offset, $limit);

        // ACtion of job above

        $this->finishedMyJob([
            'msg' => 'Het systeem is gereed', 
            'type' => 'success',
            'link' => url('dashboard')
        ]);

    }

    public function jobLogic($offset, $limit){

        if($this->this_batch_count > 0){

            // Slack::send($this->data['dir']);
            
            $csv = $this->data['dir'].'/'.$this->iteration.'_operations.csv';

            $content = decrypt(file_get_contents($csv));
            $reader = \League\Csv\Reader::createFromString($content);

            $headers = $this->data['headers'];
            $res = $reader->fetch();

            // dd($headers);

            foreach($res as $row){

                $newrow = [];
                foreach($row as $r){
                    if(trim($r) == ''){
                        $newrow[] = null;
                    }else{
                        $newrow[] = $r;
                    }
                }

                $row = array_combine($headers, $newrow);

                $op = new LocalOperation();
                $op->setSessionDB($this->serpentes);
                $op->setData($row);
                $op->save();

            }

        }

        if($this->last){
            
        }
        
    }
}


// Excel::load($path , function($reader) {

//     $results = $reader->all();

//     foreach($results as $result){
//         $op = new LocalOperation();
//         $op->setSessionDB($this->serpentes);
//         $op->setData($result);
//         $op->save();  
//     }           

// })->take($limit)->skip($offset);