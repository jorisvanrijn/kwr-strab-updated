<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Carbon\Carbon;

use App\LocalOperation;
use App\OnlineOperation;
use App\AnalyseTable;
use Slack;

use Illuminate\Support\Facades\DB;

class ListAnalyseJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($args, $last = false)
    {
        parent::__construct($args, $last);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        // Slack::send('Ik doe echt een UploadJob');

        $offset = round($this->iteration * $this->per_iteration);
        $limit = $this->this_batch_count;

        // Slack::send('In totaal zijn er '.$this->grand_total.'. Nu process ik vanaf '.$offset.' tot '.($offset+$limit).'. Dat is iteratie '.($this->iteration+1).' van de '.$this->max_iteration.'.');

        // Action of job below
        
        $this->jobLogic($offset, $limit);

        // ACtion of job above

        if($this->data['type_analysis'] == 'lokaal'){
            $link = url('analysis/show/local');
        }elseif($this->data['type_analysis'] == 'landelijk'){
            $link = url('analysis/show/national');
        }elseif($this->data['type_analysis'] == 'lokaalvslandelijk'){
            $link = url('analysis/show/localvsnational');
        }

        $this->finishedMyJob([
            'msg' => 'De analyse is voltooid', 
            'type' => 'success',
            'link' => $link
        ]);

    }

    public function jobLogic($offset, $limit){

        $fields = config('settings.operations_table');

        if($this->this_batch_count > 0){

            // Om welk veld gaat het?
            $current_field = $this->data['fields_to_analyse'][$this->iteration];
            
            // Om welke tabel(len) gaat het?
            $current_table_list = [];
            if($this->data['type_analysis'] == 'lokaal'){
                $current_table_list['value'] = 'operations_'.$this->serpentes;
            }
            if($this->data['type_analysis'] == 'landelijk'){
                $current_table_list['value'] = 'online_operations';
            }
            if($this->data['type_analysis'] == 'lokaalvslandelijk'){
                $current_table_list['value'] = 'operations_'.$this->serpentes;
                $current_table_list['value2'] = 'online_operations';
            }

            // maak de WHERE clause klaar:
            $sql_where = $this->data['filter_query'];
            $sql_bindings = $this->data['filter_bindings'];

            if($sql_where != '') $sql_where = ' where '.$sql_where;
            if($sql_bindings == '') $sql_bindings = [];

            $analyseResults = [];

            foreach($current_table_list as $afield => $current_table){

                $numeric_analysis = ['double', 'int', 'degdpt', 'visus'];
                if(in_array($fields[$current_field]['laravel_db_function'], $numeric_analysis)){

                    // calc median and quartiles
                    
                    DB::statement('set @ct := (select count(1) from '.$current_table.''.$sql_where.');', $sql_bindings);
                    DB::statement('set @row_id := 0;');

                    DB::enableQueryLog();

                    $results = DB::select('
                                    select avg('.$current_field.') as median
                                    from ((select * from '.$current_table.' '.$sql_where.' order by '.$current_field.') as subset_ops)
                                    where (select @row_id := @row_id + 1)
                                    between @ct*0.5 and @ct*0.5 + 1
                                ', $sql_bindings);

                    $analyseResults[$afield]['median'] = round($results[0]->median, 4);

                    DB::statement('set @ct := (select count(1) from '.$current_table.''.$sql_where.');', $sql_bindings);
                    DB::statement('set @row_id := 0;');

                    $results = DB::select('
                                    select avg('.$current_field.') as q1
                                    from ((select * from '.$current_table.' '.$sql_where.' order by '.$current_field.') as subset_ops)
                                    where (select @row_id := @row_id + 1)
                                    between @ct*0.25 and @ct*0.25 + 1
                                ');

                    DB::statement('set @ct := (select count(1) from '.$current_table.''.$sql_where.');', $sql_bindings);
                    DB::statement('set @row_id := 0;');

                    $results2 = DB::select('
                                    select avg('.$current_field.') as q3
                                    from ((select * from '.$current_table.' '.$sql_where.' order by '.$current_field.') as subset_ops)
                                    where (select @row_id := @row_id + 1)
                                    between @ct*0.75 and @ct*0.75 + 1
                                ');

                    $analyseResults[$afield]['q1'] = round($results[0]->q1, 4);
                    $analyseResults[$afield]['q2'] = $analyseResults[$afield]['median'];
                    $analyseResults[$afield]['q3'] = round($results2[0]->q3, 4);

                    // calc avg
                    $results = DB::SELECT('select avg('.$current_field.') as avgop from '.$current_table.$sql_where, $sql_bindings);
                    // Slack::send(json_encode(DB::getQueryLog()));
                    $analyseResults[$afield]['avg'] = round($results[0]->avgop, 4);

                    // calc mode
                    $results = DB::SELECT('select '.$current_field.' as mode from '.$current_table.$sql_where.' group by 1 order by count(1) desc limit 1', $sql_bindings);
                    // Slack::send(json_encode(DB::getQueryLog()));
                    $analyseResults[$afield]['mode'] = round($results[0]->mode, 4);

                    // calc max and min
                    $results = DB::SELECT('select max('.$current_field.') as maxop, min('.$current_field.') as minop from '.$current_table.$sql_where, $sql_bindings);
                    // Slack::send(json_encode(DB::getQueryLog()));
                    $analyseResults[$afield]['max'] = round($results[0]->maxop, 4);
                    $analyseResults[$afield]['min'] = round($results[0]->minop, 4);
                    
                }elseif($fields[$current_field]['laravel_db_function'] == 'enum' OR $fields[$current_field]['laravel_db_function'] == 'boolean'){

                    // FILTER
                    $results = DB::SELECT('SELECT a.'.$current_field.' as foption, COUNT(1) AS cnt FROM '.$current_table.' a'.$sql_where.' GROUP BY a.'.$current_field.';', $sql_bindings);
                    // Slack::send(json_encode(DB::getQueryLog()));

                    $tempRes = [];
                    foreach($results as $k => $v){
                        $tempRes[$v->foption] = $v->cnt;
                    }

                    $total = array_sum($tempRes);
                    foreach($tempRes as $k => $v){

                        $key_to_show = $k;
                        if($fields[$current_field]['laravel_db_function'] == 'boolean'){
                            $key_to_show = ($k == '0') ? 'Nee' : 'Ja';
                        }

                        $analyseResults[$afield][$key_to_show] = round( ($v/$total*100), 2).'% ('.$v.')';
                    }

                }

            }

            // Zet de uitkomst in een tabel
            if(count($analyseResults) > 0 AND (count($analyseResults['value']) > 0 OR count($analyseResults['value2']) > 0)){
                $atableres = new AnalyseTable;
                $atableres->setTable($this->data['table']);

                $atableres->setData([
                    'key' => $current_field
                ]);

                if(isset($analyseResults['value'])){
                    $atableres->setData([
                        'value' => json_encode($analyseResults['value'])
                    ]);
                }

                if(isset($analyseResults['value2'])){
                    $atableres->setData([
                        'value2' => json_encode($analyseResults['value2'])
                    ]);
                }

                $atableres->save();
            }

        }

        if($this->last){
            
        }
        
    }
}