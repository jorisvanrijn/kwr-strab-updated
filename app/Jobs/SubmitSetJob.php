<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Carbon\Carbon;

use App\LocalOperation;
use App\OnlineOperation;
use App\Set;

class SubmitSetJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($args, $last = false)
    {
        parent::__construct($args, $last);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        // Slack::send('Ik doe echt een UploadJob');

        $offset = round($this->iteration * $this->per_iteration);
        $limit = $this->this_batch_count;

        // Slack::send('In totaal zijn er '.$this->grand_total.'. Nu process ik vanaf '.$offset.' tot '.($offset+$limit).'. Dat is iteratie '.($this->iteration+1).' van de '.$this->max_iteration.'.');

        // Action of job below
        
        $this->jobLogic($offset, $limit);

        // ACtion of job above

        if($this->data['type'] == 'eso'){
            $this->finishedMyJob([
                'msg' => 'Uw eso uploads zijn gereed', 
                'type' => 'success',
                'link' => url('operations/upload/step3')
            ]);
        }else{
            $this->finishedMyJob([
                'msg' => 'De complete upload is voltooid', 
                'type' => 'success',
                'link' => url('operations')
            ]);
        }

    }

    public function jobLogic($offset, $limit){

        if($this->this_batch_count > 0){

            $operations = LocalOperation::sessionDB($this->serpentes)
                ->whereRaw('uploaded_at IS NULL')
                ->whereRaw('completed_at IS NOT NULL')
                ->where(['operation_type' => $this->data['type']])
                ->orderBy('created_at', 'asc')
                ->take($limit)
                // ->skip($offset)
                ->get();

            if(count($operations) >= config('settings.set_size')){
                $set_id = str_random(7);

                $set = new Set();
                $set->setSessionDB($this->serpentes);
                $set->key = $set_id;
                $set->value2 = $this->data['type'];
                $set->save();

                foreach ($operations as $operation) {
                        
                    $passData = [];
                    $tempData = $operation->toArray();
                    $tempData['set_id'] = $set_id;
                    
                    foreach($this->data['fields'] as $k => $v){
                        if(isset($v['upload']) AND $v['upload'] AND $v['is_actual_field']){
                            $passData[$k] = $tempData[$k];
                        }
                    }
                    
                    OnlineOperation::create($passData);

                    $operation->setSessionDB($this->serpentes);
                    $operation->setData(['uploaded_at' => date('Y-m-d H:i:s'), 'set_id' => $set_id]);
                    $operation->save();
                    
                }
            }

        }

        if($this->last){
            
        }
        
    }
}