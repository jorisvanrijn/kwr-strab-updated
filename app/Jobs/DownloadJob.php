<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Carbon\Carbon;

use App\LocalOperation;
use Faker;
use Illuminate\Support\Facades\Config;

use Chumper\Zipper\Zipper as Zipper;

use Slack;

class DownloadJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($args, $last = false)
    {
        parent::__construct($args, $last);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        // Slack::send('Ik doe echt een DownloadJob');

        $offset = round($this->iteration * $this->per_iteration);
        $limit = $this->this_batch_count;

        // Slack::send('In totaal zijn er '.$this->grand_total.'. Nu process ik vanaf '.$offset.' tot '.($offset+$limit).'. Dat is iteratie '.($this->iteration+1).' van de '.$this->max_iteration.'.');

        // Action of job below
        
        $this->jobLogic($offset, $limit);

        // Action of job above

        $this->finishedMyJob([
            'msg' => 'Nadat u het bestand lokaal heeft opgeslagen kunt u veilig <a href="'.url('logout?continue').'">uitloggen</a>', 
            'type' => 'success',
            'link' => url('dashboard/download/zip')
        ]);

    }

    public function jobLogic($offset, $limit){

        if($this->this_batch_count > 0){
            // Haal hier de operaties op die opgeslagen moeten worden
            $operations = LocalOperation::sessiondb($this->serpentes)->skip($offset)->take($limit)->get();

            // Open de CSV
            // $writer = \League\Csv\Writer::createFromPath(new \SplFileObject($this->data['dir'].'/operations.csv', 'a+'), 'a+');

            // foreach ($operations as $k=>$operation) {
            //     $writer->insertOne($operation->toArray());
            //     unset($operations[$k]);
            // }

            // unset($operations);
            // unset($writer);
            
            $csv = \League\Csv\Writer::createFromFileObject(new \SplTempFileObject());

            foreach ($operations as $k => $operation) {
                $csv->insertOne($operation->toArray());
                unset($operations[$k]);
            }

            file_put_contents($this->data['dir'].'/'.$this->iteration.'_operations.csv', encrypt($csv->__toString()));
        }

        if($this->last){
            $files = glob($this->data['dir'].'/*');

            $zipper = new Zipper;
            $zipper->make($this->data['zip']);
            $zipper->add($files);
            $zipper->close();   
        }
        
    }
}
