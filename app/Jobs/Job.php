<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Slack;

use App\Events\JobFinished;

use Queue;
use Illuminate\Queue\Events\JobProcessed;

use App\Setting;

abstract class Job
{
    /*
    |--------------------------------------------------------------------------
    | Queueable Jobs
    |--------------------------------------------------------------------------
    |
    | This job base class provides a central location to place any logic that
    | is shared across all of your jobs. The trait included with the class
    | provides access to the "onQueue" and "delay" queue helper methods.
    |
    */

    use Queueable;

    protected $last;
    protected $this_batch_count;
    protected $serpentes;
    protected $iteration;
    protected $max_iteration;
    protected $per_iteration;
    protected $grand_total;
    protected $data;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($args, $last = false)
    {
        foreach($args as $k => $v){
            $this->{$k} = $v;
        }

        $this->last = $last;
    }

    public function finishedMyJob($data){
        if($this->max_iteration == 1){
            sleep(1);
        }

        event(new JobFinished([
            'msg' => $data['msg'], 
            'type_alert' => $data['type'], 
            'link' => $data['link'], 
            'last' => $this->last,
            'iteration' => ($this->iteration+1), 
            'max_iterations' => $this->max_iteration,
            'per_keer' => $this->per_iteration,
            'serpentes' => $this->serpentes
        ]));

        if($this->last){
            $disable_job_lock = Setting::sessiondb($this->serpentes)->firstOrNew(['key' => 'job_lock']);
            $disable_job_lock->setSessionDB($this->serpentes);
            $disable_job_lock->value = 'off';
            $disable_job_lock->save();
        }
    }
}
