<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Carbon\Carbon;

use App\LocalOperation;
use App\OnlineOperation;
use App\AnalyseTable;

use Illuminate\Support\Facades\DB;

class SetAnalyseJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($args, $last = false)
    {
        parent::__construct($args, $last);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        // Slack::send('Ik doe echt een UploadJob');

        $offset = round($this->iteration * $this->per_iteration);
        $limit = $this->this_batch_count;

        // Slack::send('In totaal zijn er '.$this->grand_total.'. Nu process ik vanaf '.$offset.' tot '.($offset+$limit).'. Dat is iteratie '.($this->iteration+1).' van de '.$this->max_iteration.'.');

        // Action of job below
        
        $this->jobLogic($offset, $limit);

        // Action of job above

        $this->finishedMyJob([
            'msg' => 'De analyse is voltooid', 
            'type' => 'success',
            'link' => url('analysis/show/set')
        ]);

    }

    public function jobLogic($offset, $limit){

        $fields = config('settings.operations_table');

        if($this->this_batch_count > 0){
            // Q
            
            $current_set_id = $this->data['sets'][$this->iteration]['set_id'];
            
            $setData = [];
            
            foreach($this->data['xVals'] as $xVal){
                
                // Aangepast voor 3.1.0.5
                // $setData[$xVal] = (OnlineOperation::where($this->data['field'], '<=', $xVal)->where('set_id', $current_set_id)->count()/$this->data['set_size'])*100;
                
               /* $setData[$xVal] = (

                    OnlineOperation::where($this->data['field'], '<=', $xVal)->where('set_id', $current_set_id)->where($this->data['field'], '<>', '', 'and')->count()

                    /

                    OnlineOperation::where('set_id', $current_set_id)->where($this->data['field'], '<>', '', 'and')->count()

                )*100;*/

                // Aangepast voor 3.1.0.8
                $setData[$xVal] = (

                    OnlineOperation::where($this->data['field'], '<=', $xVal)->where('set_id', $current_set_id)->whereNotNull($this->data['field'])->count()

                    /

                    OnlineOperation::where('set_id', $current_set_id)->whereNotNull($this->data['field'])->count()

                )*100;
            
            }

            $atableres = new AnalyseTable;
            $atableres->setTable($this->data['table']);

            $atableres->setData([
                'key' => $current_set_id,
                'value' => json_encode($setData)
            ]);

            $atableres->save();
        }

        if($this->last){
            
        }
        
    }
}