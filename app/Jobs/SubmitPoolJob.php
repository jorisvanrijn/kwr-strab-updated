<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Carbon\Carbon;

use App\LocalOperation;
use App\OnlineOperation;

class SubmitPoolJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($args, $last = false)
    {
        parent::__construct($args, $last);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        // Slack::send('Ik doe echt een UploadJob');

        $offset = round($this->iteration * $this->per_iteration);
        $limit = $this->this_batch_count;

        // Slack::send('In totaal zijn er '.$this->grand_total.'. Nu process ik vanaf '.$offset.' tot '.($offset+$limit).'. Dat is iteratie '.($this->iteration+1).' van de '.$this->max_iteration.'.');

        // Action of job below
        
        $this->jobLogic($offset, $limit);

        // ACtion of job above

        $this->finishedMyJob([
            'msg' => 'Er zijn operaties naar de pool geupload', 
            'type' => 'success',
            'link' => url('operations/upload/step2')
        ]);

    }

    public function jobLogic($offset, $limit){

        if($this->this_batch_count > 0){

            $operations = LocalOperation::sessionDB($this->serpentes)
                ->whereRaw('uploaded_at IS NULL')
                ->whereRaw('completed_at IS NOT NULL')
                ->where(['operation_type' => 'pool'])
                ->orderBy('created_at', 'asc')
                ->take($limit)
                // ->skip($offset)
                ->get();

            foreach ($operations as $operation) {
                    
                $passData = [];
                $tempData = $operation->toArray();
                
                foreach($this->data['fields'] as $k => $v){
                    if(isset($v['upload']) AND $v['upload'] AND $v['is_actual_field']){
                        $passData[$k] = $tempData[$k];
                    }
                }
                
                OnlineOperation::create($passData);

                $operation->setSessionDB($this->serpentes);
                $operation->setData(['uploaded_at' => date('Y-m-d H:i:s')]);
                $operation->save();
                
            }

        }

        if($this->last){
            
        }
        
    }
}