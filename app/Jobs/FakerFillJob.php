<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Carbon\Carbon;

use App\LocalOperation;
use Faker;
use Illuminate\Support\Facades\Config;



use Slack;

class FakerFillJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($args, $last = false)
    {
        parent::__construct($args, $last);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $offset = round($this->iteration * $this->per_iteration);
        $limit = $this->this_batch_count;

        // Slack::send('In totaal zijn er '.$this->grand_total.'. Nu process ik vanaf '.$offset.' tot '.($offset+$limit).'. Dat is iteratie '.($this->iteration+1).' van de '.$this->max_iteration.'.');

        // Action of job below
        
        $this->jobLogic($offset, $limit);

        // ACtion of job above

        $this->finishedMyJob([
            'msg' => 'Er zijn '.$this->grand_total.' operaties aan uw database toegevoegd', 
            'type' => 'success',
            'link' => url('operations')
        ]);

    }

    public function jobLogic($offset, $limit){

        $faker = Faker\Factory::create();
        $fields = Config::get('settings.operations_table');

        $aantal = $limit;
        $serpentes = $this->serpentes;

        for ($i=0; $i < $aantal; $i++) { 

            $d = [];

            foreach($fields as $kf => $f){
                if(isset($f['faker'])){

                    if($f['faker'] == 'randomDouble'){
                        $min = isset($f['minimum_faker']) ? $f['minimum_faker'] : 0;
                        $max = isset($f['maximum_faker']) ? $f['maximum_faker'] : 0;

                        $d[$kf] = $faker->randomFloat(4, $min, $max);
                    }elseif($f['faker'] == 'yesOrNo'){
                        $d[$kf] = $faker->numberBetween(0,1);
                    }else{

                        if(is_array($f['faker'])){
                            if(count($f['faker']) > 1){
                                $d[$kf] = $faker->{$f['faker'][0]}($f['faker'][1]);
                            }else{
                                $d[$kf] = $faker->{$f['faker'][0]};
                            }
                        }else{
                            $d[$kf] = $faker->{$f['faker']};
                        }

                    }

                }
            }

            $check = rand(1,3);

            if($check == 1){
                // $d['type_esodeviaties']         = 'nvt';
                $d['type_exodeviaties']         = 'nvt';
                $d['type_overige_deviaties']    = 'nvt';
            }elseif($check == 2){
                $d['type_esodeviaties']         = 'nvt';
                // $d['type_exodeviaties']         = 'nvt';
                $d['type_overige_deviaties']    = 'nvt';
            }else{
                $d['type_esodeviaties']         = 'nvt';
                $d['type_exodeviaties']         = 'nvt';
                // $d['type_overige_deviaties']    = 'nvt';
            }

            $o = new LocalOperation($d);
            $o->setSessionDB($serpentes);         
            $o->save();
            
            unset($d); unset($kf); unset($f); unset($min); unset($max); unset($o);

        }
        
    }
}
