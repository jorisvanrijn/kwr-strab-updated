<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Set extends UserSpecificModel
{
    public $type = 'sets';
}
