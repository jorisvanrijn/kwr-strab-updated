<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Queue;
use Illuminate\Queue\Events\JobProcessed;
use Slack;
use App\Events\JobFinished;

use App\Setting;

use URL;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        app('view')->composer('layouts.app', function ($view) {
            $action = app('request')->route()->getAction();

            $controller = class_basename($action['controller']);
            list($controller, $action) = explode('@', $controller);

            $view->with(compact('controller', 'action'));
        });

        // Queue::after(function (JobProcessed $event) {
        //     // $event->connectionName
        //     // $event->job
        //     // $event->data
        //     // event(new JobFinished("Job is klaar "));
        //     // $this->info($event->job->isDeleted());
        // });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
