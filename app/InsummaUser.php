<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsummaUser extends Model
{
    protected $table = 'insumma_users';
    protected $guarded = ['id'];
}
