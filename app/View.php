<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class View extends UserSpecificModel
{
    public $type = 'views';
}
