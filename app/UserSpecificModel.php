<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class UserSpecificModel extends Model{
	use \Stevebauman\EloquentTable\TableTrait;

	protected $guarded = ['id'];
	
	protected static $_table;
	protected $serpentes = null;

	public $type;

	public function setSessionDB($serpentes){
	    $this->serpentes = $serpentes;
	    if($serpentes != null){
	        $this->table = $this->type.'_'.$serpentes; // you could use the logic from your example as well, but getTable looks nicer
	    }
	}

	public static function sessionDB($serpentes){
	    $instance = new static;
	    $instance->setSessionDB($serpentes);
	    return $instance->newQuery();
	}

	public function newInstance($attributes = array(), $exists = false){
	    $model = parent::newInstance($attributes, $exists);
	    $model->setSessionDB($this->serpentes);
	    return $model;
	}

	public function setData($data){

		$configfields = 'extra_tables';
		if($this->type == 'operations') $configfields = 'operations_table';
    	$fields = Config::get('settings.'.$configfields);

    	foreach($fields as $k => $v){
    		if(isset($data[$k])){
    			$this->{$k} = $data[$k];
    		}
    	}
    	
    }
}