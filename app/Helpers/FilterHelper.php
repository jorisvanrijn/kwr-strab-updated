<?php

namespace App\Helpers;
use App\LocalOperation;
use App\OnlineOperation;
use App\Filter;
use Illuminate\Http\Request;

use Carbon\Carbon;
use Illuminate\Support\Facades\Config;

class FilterHelper{
	///////////////////
	// Display logic //
	///////////////////

	public static function filter_interface($fields, $request, $action = '', $current_filter){

		// dd($request);

		if($request->has('filter') AND $request->has('comparator') AND $request->has('values')){
			$filter_string = '';

			for($i = 0; $i < count($request->filter); $i++){
				if($request->filter[$i] != 'none'){
					$filter_string .= FilterHelper::filter_string($fields, [
						'filter' => $request->filter[$i], 
						'comparator' => $request->comparator[$i],
						'value' => $request->values[$i],
					]);
				}
			}
		}else{
			$filter_string = FilterHelper::filter_string($fields);	
		}

		$form = '

			<form action="'.$action.'" method="GET" class="noloading no-loading">
				<div class="filter_area">
					'.$filter_string.'
				</div>
				<!--'.csrf_field().'-->
				<!--'.method_field('GET').'-->
				<input type="submit" class="btn btn-primary" value="'.(($action == '') ? 'Filter resultaten' : 'Analyseer operaties').'"/>&nbsp;
				<a href="#" class="add_filter btn btn-default btn-small" data-toggle="tooltip" title="Filter rij toevoegen"><i class="icon icon-plus" ></i></a>&nbsp;

				<button name="savefilter" value="true" class="btn btn-default btn-small" data-toggle="tooltip" title="Filter opslaan" type="submit"><i class="icon icon-folder-close"></i></button>&nbsp;

				<a href="'.url('operations/filters?redirect='.urlencode($request->getPathInfo())).'" class="btn btn-default btn-small" data-toggle="tooltip" title="Filter openen"><i class="icon icon-folder-open" ></i></a>
			</form>

		';

		$filters = Filter::sessiondb($request->session()->get('serpentes'))->where('key', '<>', '(naamloze filter)')->orderBy('value3', 'asc')->get();
		$rec_filters = Filter::sessiondb($request->session()->get('serpentes'))->orderBy('updated_at', 'desc')->limit(6)->get();
        
        $editlink = url('operations/new_filter');
		if($current_filter < 0){
			$current_filter_text = 'Alle operaties';
		}else{
			$current_filter_obj = Filter::sessiondb($request->session()->get('serpentes'))->findOrFail($current_filter);
			$current_filter_text = $current_filter_obj->key;
			$editlink = url('operations/edit_filter/'.$current_filter);
		}

		$r = '';

		if($current_filter > -10){
			$r = '<div class="noloading filterlistcontainer">';
	        $r .= '<h4 class="filterheader">Huidige selectie</h4>';
	        $r .= '<ul class="filter-list">';
	        $r .= '<li class="active"><a href="'.$editlink.'" data-toggle="tooltip" title="Bewerk huidige selectie">'.$current_filter_text.' &nbsp;<i class="icon-pencil icon-white"></i></a></li>';
	        $r .= '</ul>';
	        $r .= '<hr/>';
	    }

	    ///////////////////////////////////////
	    // ook een linkje maken voor de rest //
	    ///////////////////////////////////////
	    
	    $url = 'operations/filter/';
	    if($action != ''){
	    	$url = $action;
	    }

        $r .= '<h4 class="filterheader">Opgeslagen filters</h4>';
        $r .= '<ul class="filter-list">';

        // if($current_filter > -10){
        	$r .= '<li'.(($current_filter < 0 AND $current_filter > -10) ? ' class="active"' : '').'><a href="'.url($url.'-1').'">Alle operaties</a></li>';
        // }

        /*else{
        	if(count($filters) == 0){
        		$r .= '<li><em>geen opgeslagen filters</em></li>';
        	}
        }*/

        foreach($filters as $filter){
        	$r .= '<li'.(($current_filter == $filter['id']) ? ' class="active"' : '').'><a href="'.url($url.$filter['id']).'">'.$filter['key'].'</a></li>';
        }
        $r .= '</ul>';
        $r .= '<hr/>';

        $r .= '<h4 class="filterheader">Recent gebruikt</h4>';
        $r .= '<ul class="filter-list">';
        \Carbon\Carbon::setLocale('nl');
        $aantal = 0;
        foreach($rec_filters as $filter){
        	$t = \Carbon\Carbon::createFromTimeStamp(strtotime($filter->updated_at))->diffForHumans();
        	if($current_filter == $filter['id']){
        		continue;
        	}else{
        		$aantal++;
        	}
        	$r .= '<li'.(($current_filter == $filter['id']) ? ' class="active"' : '').'><a href="'.url($url.$filter['id']).'">'.$filter['key'].'<br/><small>('.$t.')</small></a></li>';
        }
        if($aantal == 0){
        	$r .= '<li><em>geen recente filters</em></li>';
        }
        $r .= '</ul>';
        $r .= '</div>';
        // $r .= '<hr/>';

		return $r;
	}

	public static function filter_logic($fields){

		$filter_string = FilterHelper::filter_string($fields, [], "'+ rand +'");

		$r = "
		<script type=\"text/javascript\">
		
			$(document).ready(function(){
				$('.add_filter').click(function(){
					var rand = Math.random().toString(12).substring(7);
					var filter_string = '".$filter_string."';
					$('.filter_area').append(filter_string);
					return false;
				});

				$('body').on('change', '.type_selector', function(){
					var raw_value = $(this).find('option:selected').val();

					if(raw_value == 'none'){
						$('.comparator_selector_' + ($(this).data('keyfield'))).prop('disabled', 'disabled');
						$('.value_' + ($(this).data('keyfield'))).prop('disabled', 'disabled');
						return;
					}

					var raw_comparators = ($(this).find('option:selected').data('comparator'));
					var comparators = raw_comparators.split('|');
					console.log(comparators);

					$('.comparator_selector_' + ($(this).data('keyfield'))).prop('disabled', false);
					$('.value_' + ($(this).data('keyfield'))).prop('disabled', false);
					
					$('.comparator_selector_' + ($(this).data('keyfield'))).html('');
					for(var i = 0; i < comparators.length; i++){
						$('.comparator_selector_' + ($(this).data('keyfield'))).append('<option>' + comparators[i] + '</option>')
					}
				});

				$('body').on('click', '.close_filter_row', function(){
					$('#filter_' + $(this).data('keyfield')).remove();
				});
			});

		</script>
		";

		return $r;
	}

	public static function comp_format($c){
		// dd($c);
		// if($c == 'LIKE') return 'zoeken op';
		return $c;
	}

	public static function filter_string($fields, $data = [], $keyfield = ''){

		if($keyfield == ''){
			$keyfield = str_random(6);
		}

		$filter_string = '<div id="filter_'.$keyfield.'"><div class="tcontainer-fluid"><div class="row-fluid"><div class="span12">';
		$filter_string .= '<select name="filter[]" class="input-block-level type_selector type_selector_'.$keyfield.'" data-keyfield="'.$keyfield.'"><option value="none">Veld:</option>';
		foreach($fields as $k => $v){
			if( (isset($v['filterable']) AND $v['filterable'] != false) OR !isset($v['filterable']) ){
				$d = '';
				if(isset($data['filter']) AND $data['filter'] == $k) $d = ' selected="selected"';
				if(isset($v['comparator']) AND is_array($v['comparator'])) $filter_string .= '<option value="'.$k.'"'.$d.' data-comparator="'.implode('|', $v['comparator']).'">'.$v['trivial'].'</option>';
			}
		}
		$filter_string .= '</select>';
		$filter_string .= '</div></div></div>';

		$filter_string .= '<div class="tcontainer-fluid"><div class="row-fluid"><div class="span4">';

		$e = '';
		if(isset($data['comparator'])){
			foreach($fields[$data['filter']]['comparator'] as $c){
				$e .= '<option'.(($c == $data['comparator']) ? ' selected="selected"' : '').' value="'.$c.'">'.FilterHelper::comp_format($c).'</option>';
			}
		}

		// TODO
		$filter_string .= '<select name="comparator[]" class="comparator_selector comparator_selector_'.$keyfield.' input-block-level" data-keyfield="'.$keyfield.'" '.( (isset($data['comparator'])) ? '' : 'disabled="disabled"' ).'>'.$e.'</select>';
		$filter_string .= '</div><div class="span7">';

		// TODO
		$filter_string .= '<input type="text" name="values[]" data-keyfield="'.$keyfield.'" '.( (isset($data['value'])) ? '' : 'disabled="disabled"' ).' value="'.( (isset($data['value'])) ? $data['value'] : '' ).'" class="input-block-level value_'.$keyfield.'"/>';
		$filter_string .= '</div><div class="span1"><a class="close close_filter_row" data-keyfield="'.$keyfield.'" href="#">&times;</a></div></div></div>';

		$filter_string .= '<hr style="margin: 0; padding: 0; margin-bottom: 10px;"/></div>';

		return $filter_string;

	}

	//////////////////
	// Filter logic //
	//////////////////
	
	public static function filter_query(Request $request, $tabel = 'lokaal', $current_filter){
		$fields = Config::get('settings.operations_table');

		if($tabel == 'lokaal'){
			$operations_query = LocalOperation::sessiondb($request->session()->get('serpentes'))->newQuery();
		}elseif($tabel == 'landelijk'){
			$operations_query = (new OnlineOperation)->newQuery();
		}

		if($current_filter > 0){
			$current_filter_obj = Filter::sessiondb($request->session()->get('serpentes'))->findOrFail($current_filter);
			// dd(base64_decode($current_filter_obj->value));
			$operations_query->whereRaw(base64_decode($current_filter_obj->value));
			$current_filter_obj->setSessionDb($request->session()->get('serpentes'));
			$current_filter_obj->touch();
		}

		$operations_query->orderby('id', 'desc');

		return $operations_query;
	}

	public static function filter_general(Request $request){
		if($request->has('savefilter')){
			// dd($request->query());
			return redirect('operations/new_filter')->with(['filterdata' => $request->query()]);
		}

		$fields = Config::get('settings.operations_table');
		if($request->has('filter') AND $request->has('comparator') AND $request->has('values')){

			for($i = 0; $i < count($request->filter); $i++){
				if($request->filter[$i] != 'none'){
					$v = $request->values[$i];

					Carbon::setLocale('nl');
					if($fields[$request->filter[$i]]['laravel_db_function'] == 'date'){
						try{
							$v = Carbon::parse($v, 'Europe/Amsterdam')->format('Y-m-d');
						}catch(\Exception $e){
							return view('general.justanerror', ['title' => 'Error: Onbekend datum formaat', 'msg' => 'Datum formaat niet herkend. De volgende formaten worden ondersteund:<br/>
									'.date('d-m-Y').'<br/>
									'.date('d-m-y').'<br/>
									'.date('j M y')]);
						}
					}
				}
			}

		}

		return false;
	}
}