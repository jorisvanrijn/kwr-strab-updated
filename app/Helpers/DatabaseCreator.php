<?php

//https://laravel.com/docs/4.2/schema#adding-columns

namespace App\Helpers;

class DatabaseCreator{
	public static function prepareTable($config, &$table){
		// $table->uuid('_uuid');

		foreach($config as $key => $row){
            switch($row['laravel_db_function']){
                case 'increments':					//id field
                    $table->bigIncrements('id');
                    break;
                case 'timestamps':					//created_at, updated_at
                    $table->timestamps();
                    break;
                case 'string':						//string
                    $table->string($key)->nullable()->default(null);
                    break;
                case 'binary':						//blob                                                 NOT CONFIGURED
                	$table->binary($key)->nullable()->default(null);
                	break;
                case 'boolean':						//boolean
                	$table->boolean($key)->nullable()->default(null);
                	break;
                case 'date':						//datum
                	$table->date($key)->nullable()->default(null);
                	break;
                case 'dateTime':					//datum en tijd                                        NOT CONFIGURED
                	$table->dateTime($key)->nullable()->default(null);			
                	break;
                case 'double':						//double, PREC, SCAL (16,4)
                    // $table->double($key, $row['precison'], $row['scale'])->nullable()->default(null);
                	$table->double($key, 16, 4)->nullable()->default(null);
                	break;
                case 'degdpt':                      //double, PREC, SCAL (16,4)
                    // $table->double($key, $row['precison'], $row['scale'])->nullable()->default(null);
                    $table->double($key, 16, 4)->nullable()->default(null);
                    break;
                case 'integer':						//integer
                	$table->integer($key)->nullable()->default(null);
                	break;
                case 'enum':						//enum, CHOIC
                	$table->enum($key, $row['choices'])->nullable()->default(null);
                	break;
                case 'text':						//longtext
                	$table->longText($key)->nullable()->default(null);
                	break;
                case 'timestamp':
                    $table->timestamp($key)->nullable()->default(null);
                    break;
            }
        }
	}

}