<?php

//https://laravel.com/docs/4.2/schema#adding-columns

namespace App\Helpers;

class Calculator{

	public static function get_percentile($percentile, $array) {
	    sort($array);
	    $index = ($percentile/100) * count($array);
	    if (floor($index) == $index) {
	         $result = ($array[$index-1] + $array[$index])/2;
	    }
	    else {
	        $result = $array[floor($index)];
	    }
	    return $result;
	}

	public static function get_mean($array){
		return array_sum($array) / count($array);
	}
	public static function get_median($array){
		return Calculator::get_percentile(50, $array);
	}
	public static function get_max($array){
		return max($array);
	}
	public static function get_min($array){
		return min($array);
	}

}