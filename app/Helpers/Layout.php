<?php

namespace App\Helpers;
use App\View;
use App\Setting;

use Illuminate\Support\Facades\Config;

class Layout{
	public static function panel_start($title, $width = 4, $table = false, $masonry = true, $edit = false){
		return '<div style="width: '.(round(95 * $width )-0).'px;" class="'.($masonry ? 'grid-item' : '').($edit ? ' edit-grid-item' : '').($title == 'Filter operaties' ? ' filter-box' : '').'">
			<div class="grid-sizer" style="width: '.(round(95 * $width )-0).'px;"></div>
			<div class="noloading panel'.($table ? ' tablepanel' : '').'">'.(($title != '') ? '<div class="panel-heading">'.$title.' <!--<small class="pull-right close-button"><a class="close closinglink" href="#">[-]</a></small>--></div>' : '').'<div class="panel-body">';
	}

	public static function panel_end(){
		return '</div></div></div>';
	}

	public static function row_start(){
		return Layout::row_masonry_start();
		return '<div class="container-fluid"><div class="row-fluid">';
	}

	public static function row_end(){
		return Layout::row_masonry_end();
		return '</div></div>';
	}

	public static function row_masonry_start(){
		return '<div class="container-fluid"><div class="grid">';
	}

	public static function row_masonry_end(){
		return '</div></div>';
	}

	public static function spacing(){
		return '<div style="height: 30px; display: block;"></div>';
	}

	public static function spacing_small(){
		return '<div style="height: 15px; display: block;"></div>';
	}

	public static function error_and_success_msg($errors, $k){
		if($errors->has($k)){
			return '<p class="text-error">'.$errors->first($k).'</p>';
		}
	}

	/* changed 3.1.1.0: $fields was 2 times in function parameters?! */
	public static function operation_column($col, $fields, $categories, $fields_depr, $operation, $errors, $request, $edit = true){
		$x = '';
		foreach($categories as $c => $cat){

			if($cat['parentcat'] == $col){

				$x .= Layout::panel_start($cat['trivial'], 3, false, false, true);

					if($edit){
						$x .= Layout::htmlFields($fields, $c, $operation, $errors, $request);
					}else{
						$x .= Layout::htmlViewFields($fields, $c, $operation, $request);
					}

				$x .= Layout::panel_end();

			}

		}
		return $x;
	}

	// deprecated
	public static function error_and_success($errors){
		// dd($errors);

		if($errors->has('general')){
            return '<br/><span class="alert alert-error">
                <strong>'.$errors->first('general').'</strong>
            </span><br/><br/>';
        }
	}

	public static function export_button($request){
		$params = ($request->query->all());
		$params['export'] = true;

		return ' <a href="'.url($request->path().'?'.http_build_query($params)).'" class="btn btn-small btn-default pull-right" data-toggle="tooltip" title="exporteer huidige selectie"><i class="icon icon-file"> </i> Exporteer selectie</a>';
	}

	////////////////
	// View logic //
	////////////////
	public static function view_helper($request){
		$views = View::sessiondb($request->session()->get('serpentes'))->orderBy('value3', 'asc')->get();
        
		$last_view = Setting::sessiondb($request->session()->get('serpentes'))->where(['key' => 'last_used_view'])->first();  
		if(!$last_view){
			// dd($last_view->value);
			$current_view = 'all';
		}else{
			$current_view = $last_view->value;
		}

        $r = '<p><strong>Toon data (kies view):</strong></p>';
        $r .= '<select name="viewselector" id="viewselector" class="input-block-level">';
        $r .= '<option value="serpentesshowall" data-id="all"'.(($current_view == 'all') ? ' selected="selected"' : '').'>Alles</option>';
        foreach($views as $view){
        	$r .= '<option value="'.$view['value'].'" data-id="'.$view['id'].'"'.(($current_view == $view['id']) ? ' selected="selected"' : '').'>'.$view['key'].'</option>';
        }
        $r .= '</select>';
        $r .= '<small><a href="'.url('operations/views').'">Views bewerken</a></small><hr/>';

        $r .= '<p><strong>Zoek naar veld:</strong></p>';
        $r .= '<select name="fieldselector" id="fieldselector" class="input-block-level">';
        $r .= '<option>-Kies-</option>';

        $fields = Config::get('settings.operations_table');
        foreach($fields as $k => $field){
        	if(isset($field['trivial'])){
        		$r .= '<option value="'.$k.'">'.$field['trivial'].'</option>';
        	}
        }

        $r .= '</select>';

        return $r;
	}

	public static function view_logic($request){
		$last_view = Setting::sessiondb($request->session()->get('serpentes'))->where(['key' => 'last_used_view'])->first();  
		
		if(!$last_view OR $last_view->value == 'all'){
			// dd($last_view->value);
			$current_view = 'all';
			$q = "$('.allfields').show();";
		}else{
			$current_view = $last_view->value;
			$current_view_info = View::sessiondb($request->session()->get('serpentes'))->find($last_view->value);
			// dd($current_view_info);
			$q = "$('.allfields').hide();
				var fieldsRaw = '".$current_view_info->value."';
				var fields = ( fieldsRaw.split('|') );
				for(var i = 0; i < fields.length; i ++){
					if(fields[i] != ''){
						$('.' + fields[i]).show();
					}
				}";
		}

		return "

			<script type=\"text/javascript\">
				$(document).ready(function(){

					".$q."

					cleanEmptyBoxes();
					$('.grid').masonry();

					$('#fieldselector').change(function(){
						$('.allfields').removeClass('highlight');
						$('html, body').animate({
					        scrollTop: $( '.' + $(this).find('option:selected').val() ).offset().top
					    }, 1000);
					    $( '.' + $(this).find('option:selected').val() ).addClass('highlight');
					});

					$('#viewselector').change(function(){
						if($(this).find('option:selected').val() == 'serpentesshowall'){
							$('.allfields').show();
						}else{
							$('.allfields').hide();
							var fields = ( $(this).find('option:selected').val().split('|') );
							for(var i = 0; i < fields.length; i ++){
								if(fields[i] != ''){
									$('.' + fields[i]).show();
								}
							}	
						}

						var id = ($(this).find('option:selected').data('id'));

						$.get('".url('ajax/setlastview')."/' + id);

						cleanEmptyBoxes();
						$('.grid').masonry();
					});

				});

				function cleanEmptyBoxes(){
					$('.edit-grid-item').show();
					$('.edit-grid-item').each(function(){
						var all_fields = $(this).find('.allfields');
						var total_visible = all_fields.length;
						for(var i = 0; i < all_fields.length; i++){
							if($(all_fields[i]).css('display') == 'none'){
								total_visible-=1;
							}
						}
						if(total_visible == 0){
							$(this).hide();
						}
					});
				}
			</script>

		";
	}

	////////////////////////////
	// HTML fields edit logic //
	////////////////////////////
	public static function sup_manual(){
		return '<p style="margin-bottom: 0;"><small><sup>1</sup> verplicht voor opslaan</small></p>
			<p style="margin-bottom: 0;"><small><sup>2</sup> verplicht voor uploaden</small></p>
			<p><small><sup>3</sup> zichtbaar in de landelijke DB (data: alleen mnd en jr)</small></p>';
	}

	public static function _formatViewField($data, $f, $field){

	    if(is_null($data[$f])){
			return '<small><em>(geen waarde)</em></small>';
		}

    	$r = '<p>'.((isset($data[$f]) AND trim($data[$f]) != '') ? $data[$f] : '<small><em>(geen waarde)</em></small>').'</p>';

    	if($field['laravel_db_function'] == 'date'){
    		$t = \DateTime::createFromFormat('Y-m-d', $data[$f]);
	    	if (!$t) return ('[invalid date]');
	        return '<p>'.$t->format('d-m-Y').'</p>';
    	}

    	if($field['laravel_db_function'] == 'degdpt'){
    		return '<p>'.$data[$f].' deg</p>';
    	}

    	if($field['laravel_db_function'] == 'boolean'){
    		if($data[$f]){
    			return '<p>Ja</p>';
    		}else{
    			return '<p>Nee</p>';
    		}
    	}

    	if(is_numeric($data[$f]) AND !is_integer($data[$f])){
    		return round($data[$f], 2);
    	}
    	
    	return $r;
	}

	public static function _analyseViewField($a, $field, $t, $last_analysis_type){

		if($last_analysis_type == 'lokaal'){
			$header1 = 'Lokaal';
		}elseif($last_analysis_type == 'landelijk'){
			$header1 = 'Landelijk';
		}elseif($last_analysis_type == 'lokaalvslandelijk'){
			$header1 = 'Lokaal';
			$header2 = 'Landelijk';
		}

    	// $r = $a->value;
    	$r = '<table class="table table-bordered">';

    	$col1 = (trim($a->value) == '') ? [] : json_decode($a->value, true);
    	$col2 = (trim($a->value2) == '') ? [] : json_decode($a->value2, true);

    	if(count($col1) > 0 AND count($col2) == 0){
    		$r .= '<tr><th>'.$t.'</th><th>'.$header1.'</th></tr>';
    		foreach($col1 as $k => $v){
	    		$r .= '<tr><td>'.((is_null($k) OR trim($k) == '') ? '<small><em>(geen waarde)</em></small>' : $k ).'</td><td>'.((is_numeric($v) AND !is_integer($v)) ? round($v, 2) : $v).'</td></tr>';
	    	}
    	}

    	if(count($col1) > 0 AND count($col2) > 0){
    		$r .= '<tr><th>'.$t.'</th><th>'.$header1.'</th><th>'.$header2.'</th></tr>';

    		$merged_cols = [];

    		foreach($col1 as $k => $v){
	    		$merged_cols[$k] = ['col1' => $v, 'col2' => (isset($col2[$k]) ? $col2[$k] : '-')];
	    	}

	    	foreach($merged_cols as $k => $v){
	    		$r .= '<tr><td>'.((is_null($k) OR trim($k) == '') ? '<small><em>(geen waarde)</em></small>' : $k ).'</td><td>'.((is_numeric($v['col1']) AND !is_integer($v['col1'])) ? round($v['col1'], 2) : $v['col1']).'</td><td>'.((is_numeric($v['col2']) AND !is_integer($v['col2'])) ? round($v['col2'], 2) : $v['col2']).'</td></tr>';
	    	}
    	}

    	$r .= '</table>';
    	return $r;

	}

	public static function htmlAnalyseFields($fields, $category, $analysis, $request, $last_analysis_type){
		$r = '';

		$to_analyse = Config::get('settings.analyse_fields');   

        foreach($fields as $f => $field){

        	if( (!isset($field['hidden']) OR $field['hidden'] == false) ){
	            if( array_key_exists($f, $analysis) AND $field['category'] == $category ){
	            	//<strong>'.$field['trivial'].'</strong><br/>
	            	$r .= '<div class="allfields '.$f.'">';
	            	$r .= '<div class="'.$f.'_graph" style="height: 300px; width:100%;"></div>';
	            	$r .= '<a href="#" data-graph="'.$f.'" class="noloadinglink btn btn-default btn-mini enlarge-graph"><i class="icon icon-zoom-in"></i> Vergroot grafiek</a><br/><br/>';
	            	$r .= Layout::_analyseViewField($analysis[$f], $field, $field['trivial'], $last_analysis_type);
	            	$r .= '</div>';
	            }
	        }
        }

        return $r;
	}

	public static function graph_logic_list($fields, $analysis, $request, $last_analysis_type){
		$r = '';
		$to_analyse = Config::get('settings.analyse_fields');

		foreach($fields as $f => $field){

        	if( (!isset($field['hidden']) OR $field['hidden'] == false) ){
	            if( array_key_exists($f, $analysis) ){
	            	// $r .= $f.'_graph';
	            	
	            	if(isset($data)) unset($data);
	            	if(isset($data2)) unset($data2);

	            	$data_for_graph = [];
	            	$data = json_decode($analysis[$f]->value, true);

	            	if($last_analysis_type == 'lokaalvslandelijk'){
	            		$data2 = json_decode($analysis[$f]->value2, true);
	            	}
	            	
	            	if(isset($data['median'])){
	            		$data_for_graph[] = [0, $data['min'], $data['q1'], $data['q3'], $data['max']];
	            		if(isset($data2)) $data_for_graph[] = [1, $data['min'], $data['q1'], $data['q3'], $data['max']];

	            		$r .= ' <script type="text/javascript">
						      
						      google.charts.setOnLoadCallback(draw_'.$f.');
								  function draw_'.$f.'() {
								    var data = google.visualization.arrayToDataTable('.json_encode($data_for_graph).', true);

								    var options = {
								      title: \''.$field['trivial'].'\',
								      legend:\'none\',
								      colors: [\'#3ea95f\', \'#27324e\'],
								      vAxis: {  
							              viewWindowMode:\'explicit\',
							              viewWindow: {
							                max:-70,
							                min:70
							              }
							          }
								    };

								    // var chart = new google.visualization.CandlestickChart(document.getElementById(\''.$f.'_graph\'));
								    var chart = new google.visualization.CandlestickChart(document.getElementsByClassName(\''.$f.'_graph\')[0]);

								    chart.draw(data, options);
								  }
						    </script>';	
	            	}else{
	            		if(!isset($data2)){
		            		$data_for_graph = [];
		            		$data_for_graph[] = ['x', 'y'];

		            		foreach($data as $x => $y){
		            			$data_for_graph[] = [$x, (int) str_replace('%', '', $y)];
		            		}
		            	}else{
		            		$data_for_graph = [];
		            		$data_for_graph[] = ['x', 'Lokaal', 'Landelijk'];

		            		foreach($data as $x => $y){
		            			$data_for_graph[] = [
		            				$x, 
		            				(int) str_replace('%', '', $y),
		            				(int) str_replace('%', '', (isset($data2[$x]) ? $data2[$x] : '-'))
		            			];
		            		}
		            	}

	            		$r .= ' <script type="text/javascript">
						      
						      google.charts.setOnLoadCallback(draw_'.$f.');
								  function draw_'.$f.'() {
								    var data = google.visualization.arrayToDataTable('.json_encode($data_for_graph).');

								    var options = {
								      title: \''.$field['trivial'].'\',
								      legend:\'none\',
								      colors: [\'#3ea95f\', \'#27324e\'],
								    };

								    //var chart = new google.visualization.ColumnChart(document.getElementById(\''.$f.'_graph\'));
								    var chart = new google.visualization.ColumnChart(document.getElementsByClassName(\''.$f.'_graph\')[0]);

								    chart.draw(data, options);
								  }
						    </script>';
	            	}
	            	
	            }
	        }
	    }

	    return $r;
	}

	public static function htmlViewFields($fields, $category, $data = [], $request){
		$r = '';

        foreach($fields as $f => $field){

        	if( (!isset($field['hidden']) OR $field['hidden'] == false) ){
	            if( (!isset($field['is_actual_field']) OR $field['is_actual_field']) AND isset($field['category']) AND $field['category'] == $category AND isset($field['trivial']) AND trim($field['trivial']) != ''){

	            	$a = [];
	            	if(isset($field['required_for'])){
	            		if(in_array('save', $field['required_for'])){
	            			$a[] = '1';
	            		}
	            		if(in_array('upload', $field['required_for'])){
	            			$a[] = '2';
	            		}
	            	}

	            	if(isset($field['upload']) AND $field['upload']){
	            		$a[] = '3';
	            	}

	            	$b = '';
	            	if(count($a) > 0){
	            		$b = ' <sup>'.implode(' ', $a).'</sup>';
	            	}

	            	$r .= '<div class="allfields '.$f.((isset($data[$f]) AND trim($data[$f]) != '') ? '' : ' muted').'"><strong>'.$field['trivial'].$b.'</strong><br/>';
	            	$r .= Layout::_formatViewField($data, $f, $field);
	            	$r .= '</div>';

	            }
	        }
        }

        return $r;
	}

	public static function htmlFields($fields, $category, $data = [], $errors, $request){
        $r = '';

        $given_data = $data;

        $uploaded = (!isset($data->uploaded_at) OR is_null($data->uploaded_at)) ? false : true;

        foreach($fields as $f => $field){

        	if( (!isset($field['hidden']) OR $field['hidden'] == false) AND (!isset($field['editable']) OR $field['editable'] == true) ){
	            if( (!isset($field['is_actual_field']) OR $field['is_actual_field']) AND isset($field['category']) AND $field['category'] == $category AND isset($field['trivial']) AND trim($field['trivial']) != ''){	            	

	            	if(!is_null($request->old($f))){
	            		$data[$f] = $request->old($f);
	            	}else{
	            		// var_dump($request->old($f));
	            	}

	            	$a = [];
	            	if(isset($field['required_for'])){
	            		if(in_array('save', $field['required_for'])){
	            			$a[] = '1';
	            		}
	            		if(in_array('upload', $field['required_for'])){
	            			$a[] = '2';
	            		}
	            	}

	            	if(isset($field['upload']) AND $field['upload']){
	            		$a[] = '3';
	            	}

	            	$b = '';
	            	if(count($a) > 0){
	            		$b = ' <sup>'.implode(' ', $a).'</sup>';
	            	}

	            	$e = '';
	            	if(isset($field['eenheid']) AND $field['eenheid'] != ''){
	            		$e = '<small class="muted"> ['.$field['eenheid'].']</small>';
	            	}

	                $r .= '<fieldset class="allfields '.$f.'">';
	            	$r .= '<label for="'.$f.'"><strong>'.$field['trivial'].$e.$b.'</strong></label>';


	            	if($errors->has($f)){
			            $r .= '<p class="text-error">'.$errors->first($f).'</p>';
			        }

			        if($uploaded AND (isset($field['upload']) AND $field['upload'])){
			        	$r .= Layout::_formatViewField($data, $f, $field);
			        }else{

			        	if(count($given_data) == 0 AND isset($field['default'])){
			        		// dd('het is een nieuwe operatie en ik heb een default waarde gevonden voor je');
			        		if($field['default'] != 'today'){
			        			$data[$f] = $field['default'];
			        		}else{
			        			$data[$f] = date('Y-m-d');
			        		}
			        	}

		            	switch($field['laravel_db_function']){
			                // case 'increments':					//id field
			                //     $r .= '';
			                //     break;
			                // case 'timestamps':					//created_at, updated_at
			                //     $r .= '';
			                //     break;
			                case 'string':						//string
			                    $r .= '<input type="text" name="'.$f.'" id="'.$f.'" class="input-block-level"'.((isset($data[$f]) AND trim($data[$f]) != '') ? ' value="'.$data[$f].'"' : '').'>';
			                    break;
			                // case 'binary':						//blob
			                // 	$r .= '';
			                // 	break;
			                case 'boolean':						//boolean
			                	$c = [];
			                	$c[] = '<option disabled="disabled"'.((!isset($data[$f])) ? ' selected="selected"' : '').'>Kies:</option>';
			                	
			                	$c[] = '<option'.((isset($data[$f]) AND trim($data[$f]) == true) ? ' selected="selected"' : '').' value="Ja">Ja</option>';	
			                	$c[] = '<option'.((isset($data[$f]) AND trim($data[$f]) == false) ? ' selected="selected"' : '').' value="Nee">Nee</option>';	
			                	
			                	// $r .= '<select name="'.$f.'" id="'.$f.'" class="input-block-level">'.implode('', $c).'</select>';
			                	
			                	// New in version 3.0.3b
			                	$r .= '<input type="radio" id="'.$f.'_yes" class="'.$f.'" name="'.$f.'" value="Ja"'.((isset($data[$f]) AND trim($data[$f]) == true) ? ' checked="checked"' : '').'> Ja - ';
			                	$r .= '<input type="radio" id="'.$f.'_no" class="'.$f.'" name="'.$f.'" value="Nee"'.((isset($data[$f]) AND trim($data[$f]) == false) ? ' checked="checked"' : '').'> Nee';
			                	break;
			                case 'date':						//datum
			                	$r .= '<input type="text" name="'.$f.'" id="'.$f.'" class="datepicker input-block-level"'.((isset($data[$f]) AND trim($data[$f]) != '') ? ' value="'.$data[$f].'"' : '').'>';
			                	break;
			                // case 'dateTime':					//datum en tijd
			                // 	$r .= '';
			                // 	break;
			                case 'double':						//double, PREC, SCAL (16,4)
			                	$r .= '<input type="text" name="'.$f.'" id="'.$f.'" class="input-block-level"'.((isset($data[$f]) AND trim($data[$f]) != '') ? ' value="'.$data[$f].'"' : '').'>';
			                	break;
			                case 'degdpt':						//double, PREC, SCAL (16,4)
			                	$r .= '<div class="row-fluid">
			                				<div class="span4">
			                					<input type="text" name="'.$f.'" data-pairedwith="'.$f.'" id="'.$f.'" class="deg_'.$f.' deg-to-dpt input-block-level"'.((isset($data[$f]) 
			                						AND trim($data[$f]) != '') ? ' value="'.$data[$f].'"' : '').'>
			                				</div>
			                				<div class="span2">deg</div>
			                				<div class="span4">
			                					<input type="text" data-pairedwith="'.$f.'" class="dpt-to-deg dpt_'.$f.' input-block-level"'.((isset($data[$f]) AND trim($data[$f]) != '') ? ' value="'.round((tan(deg2rad($data[$f]))*100), 4).'"' : '').'>
			                				</div>
			                				<div class="span2">pdt</div>
			                			</div>
			                		  ';

			                	break;
			                case 'integer':						//integer
			                	$r .= '<input type="text" name="'.$f.'" id="'.$f.'" class="input-block-level"'.((isset($data[$f]) AND trim($data[$f]) != '') ? ' value="'.$data[$f].'"' : '').'>';
			                	break;
			                case 'enum':						//enum, CHOIC
			                	$c = [];
			                	$c[] = '<option value="" '.((!isset($data[$f])) ? ' selected="selected"' : '').'>Geen waarde:</option>';

			                	foreach($field['choices'] as $_c){
			                		$c[] = '<option'.((isset($data[$f]) AND trim($data[$f]) == $_c) ? ' selected="selected"' : '').'>'.$_c.'</option>';	
			                	}

			                	$r .= '<select name="'.$f.'" id="'.$f.'" class="input-block-level">'.implode('', $c).'</select>';
			                	break;
			                case 'text':						//longtext
			                	$r .= '<textarea name="'.$f.'" id="'.$f.'" class="input-block-level">'.((isset($data[$f]) AND trim($data[$f]) != '') ? $data[$f] : '').'</textarea>';
			                	break;
		            	}
	            	}

	            	$r .= '<div style="clear: both"></div>';
	            	$r .= '</fieldset>';

	            }
	        }
        }

        return $r;
    }
}