<?php

//https://laravel.com/docs/4.2/schema#adding-columns

namespace App\Helpers;

use App\Jobs\FakerFillJob;
use App\Jobs\DownloadJob;
use App\Jobs\UploadJob;
use App\Jobs\SubmitPoolJob;
use App\Jobs\SubmitSetJob;

use Carbon\Carbon;

use Illuminate\Queue\Events\JobProcessed;
use App\Events\JobFinished;
use Event;
use Queue;

use App\Setting;

class JobHelper{

    public static function processJobs($job_name, $totaal, $in_batches_of, $serpentes, $msg, $error, $button, $data = []){

        $enable_job_lock = Setting::sessiondb($serpentes)->firstOrNew(['key' => 'job_lock']);
        $enable_job_lock->setSessionDB($serpentes);
        
        if($enable_job_lock->value == 'on'){
            return redirect('operations')->with([
                'status' => 'Kon actie niet uitvoeren. Het is systeem is nog bezig met de verwerking van uw vorige request.',
                'type' => 'error'
            ]);
        }

        $time_between_jobs = 500;

        $aantal = $totaal;
        $serpentes = $serpentes;
           
        $subaantal = $aantal;
        $jobs = ceil($aantal/$in_batches_of);

        $job_name = 'App\\Jobs\\'.$job_name;

        $viewInfo = [
            'status' => $msg,
            'type' => 'info',
            'total' => (($aantal > 0) ? $aantal : 0),
            'serpentes' => $serpentes,
            'button' => $button
        ];

        $enable_job_lock->value = 'on';
        $enable_job_lock->value2 = json_encode($viewInfo);
        $enable_job_lock->save();

        // dd($error === false);
        // dd($aantal);

        if($aantal == 0 OR !is_numeric($aantal)){
            if($error === false){
                $passtojob = [
                    'serpentes' => $serpentes,
                    'iteration' => 0,
                    'max_iteration' => 1,
                    'per_iteration' => 0,
                    'grand_total' => 0,
                    'data' => $data,
                    'view' => $viewInfo
                ];
                // dd($job_name);
                $job = new $job_name($passtojob, true);
                dispatch($job);
            }else{
                $disable_job_lock = Setting::sessiondb($serpentes)->firstOrNew(['key' => 'job_lock']);
                $disable_job_lock->setSessionDB($serpentes);
                $disable_job_lock->value = 'off';
                $disable_job_lock->save();

                return redirect()->back()->with([
                    'status' => $error,
                    'type' => 'error'
                ]);
            }
        }

        for($i = 0; $i < $jobs; $i++){
            
            $passtojob = [
                'serpentes' => $serpentes,
                'iteration' => $i,
                'max_iteration' => $jobs,
                'per_iteration' => $in_batches_of,
                'grand_total' => $aantal,
                'data' => $data,
                'view' => $viewInfo
            ];

            $last = false;
            if($subaantal <= $in_batches_of){
                $passtojob['this_batch_count'] = $subaantal;
                $last = true;
            }else{
                $passtojob['this_batch_count'] = $in_batches_of;
            }

            $job = new $job_name($passtojob, $last);
            dispatch($job);

            $subaantal -= $in_batches_of;
        }

        return view('general.waitforpush', $viewInfo);

    }
}