<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class AnalyseTable extends Model
{
    protected $guarded = ['id'];

    public function setTable($table){
        $this->table = $table;
    }

    public function setData($data){

		$configfields = 'extra_tables';
		if($this->type == 'operations') $configfields = 'operations_table';
    	$fields = Config::get('settings.'.$configfields);

    	foreach($fields as $k => $v){
    		if(isset($data[$k])){
    			$this->{$k} = $data[$k];
    		}
    	}
    	
    }
}
