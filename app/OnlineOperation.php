<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OnlineOperation extends Model
{
    protected $table = 'online_operations';
    protected $guarded = ['id'];
}
