<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class LocalOperation extends UserSpecificModel{
    public $type = 'operations';
    public $forceValid = false;

    public function verifyForSave(){
    	$fields = Config::get('settings.operations_table');

        $a = [];
        foreach($fields as $k => $v){
            if(isset($v['required_for'])){
                if(in_array('save', $v['required_for'])){
                    if(trim($this->{$k}) == '' OR is_null($this->{$k}) OR !isset($this->{$k}) OR $this->{$k} === '0000-00-00'){
                        $a[$k] = $v['trivial'] . ' is een verplicht veld';
                    }
                }
            }
        }

        $count_eso = 0;
        if($this->type_esodeviaties == 'nvt'){
            $count_eso++;
        }
        if($this->type_exodeviaties == 'nvt'){
            $count_eso++;
        }
        if($this->type_overige_deviaties == 'nvt'){
            $count_eso++;
        }

        if($count_eso!=2){
            $a['type_esodeviaties'] = 'E&eacute;n type operatie moet geselecteerd worden (de rest op \'nvt\' zetten)';
        }

        return $a;
    }

    public function verifyForUpload($returnArray = false){
        
        // In 3.1.0.4 toegevoegd, weer verwijderd in 3.1.0.5
        // if($this->forceValid) return true;

    	$fields = Config::get('settings.operations_table');

        $a = [];
    	foreach($fields as $k => $v){
    		if(isset($v['required_for'])){
    			if(in_array('upload', $v['required_for'])){
    				if(trim($this->{$k}) == '' OR is_null($this->{$k}) OR !isset($this->{$k}) OR $this->{$k} === '0000-00-00'){
    					$a[] = $k;
    				}
    			}
    		}
    	}

        // dd('stop');

        if(count($a) > 0){
        if($returnArray) return $a;
           return false;
        }

        ////////////////////////////////
        // Aangepast in versie 3.0.3b //
        ////////////////////////////////
        // specifiek aan strab.kwr
        // Kijk ook of er minstens een type niet gelijk is aan nvt
        if($this->type_esodeviaties == 'nvt' AND $this->type_exodeviaties == 'nvt' AND $this->type_overige_deviaties == 'nvt'){
            return false;
        }

        return true;
    }

    public function save(array $options = []){
        if($this->verifyForUpload()){
            $this->completed_at = date('Y-m-d H:i:s');
        }else{
            $this->completed_at = null;
        }

        $fields = Config::get('settings.operations_table');

        foreach($fields as $k => $v){
            if(trim($this->{$k}) == ''){
                $this->{$k} = NULL;
            }
        }

        $this->operation_type = $this->checkType();

        return parent::save();
    }

    // specifiek voor kwr strabismus
    public function checkType(){
        // if(trim($this->type_scheelzien) == '') return null;
        // if(starts_with(strtolower($this->type_scheelzien), 'eso')) return 'eso';
        // if(starts_with(strtolower($this->type_scheelzien), 'exo')) return 'exo';
        // return 'pool';

        ////////////////////////////////
        // Aangepast in versie 3.0.3b //
        ////////////////////////////////
        
        if($this->type_esodeviaties != 'nvt' AND $this->type_exodeviaties == 'nvt' AND $this->type_overige_deviaties == 'nvt') return 'eso';
        if($this->type_esodeviaties == 'nvt' AND $this->type_exodeviaties != 'nvt' AND $this->type_overige_deviaties == 'nvt') return 'exo';
        return 'pool';
    }
}