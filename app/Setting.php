<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends UserSpecificModel
{
    public $type = 'settings';
}
