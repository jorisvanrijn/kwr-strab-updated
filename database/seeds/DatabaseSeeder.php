<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'userid' => 'admin',
            'password' => bcrypt('M60H7Yh495cZ1EI27PE55W3jb2kaSzzv'),
        ]);
    }
}
