@extends('layouts.app')

@section('content')
{!! Layout::row_start() !!}
	{!! Layout::panel_start('<a href="javascript:history.go(-1);"><i class="icon icon-arrow-left"></i></a> Verzoek follow up generatie', 3) !!}
		
		<p>Geef het nummer in van de pati&euml;nt waarvoor u een brief wilt genereren. U kunt ook in het <a href="{{ url('operations') }}">operatieoverzicht</a> op <i class="icon icon-envelope"></i> naast een operatie klikken.</p>

		<form action="{{ url('tools/followupletter') }}" method="POST">
			{{ method_field('POST') }}
			{{ csrf_field() }}

			<strong>Pati&euml;nt nummer</strong><br/>
			<input type="text" name="patientnummer" class="form-control input-block-level">

			<br/>
			<input type="submit" value="Genereer brief" class="btn btn-primary">
		</form>

	{!! Layout::panel_end() !!}
{!! Layout::row_end() !!}
@endsection