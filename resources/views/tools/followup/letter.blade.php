@extends('layouts.app')

@section('content')
{!! Layout::row_start() !!}
	{!! Layout::panel_start('Verzoek follow up generatie: '.$operation['patientnummer'], 7) !!}
<a href="#" class="btn btn-primary print-button"><i class="icon icon-white icon-print"></i> Print</a> <a href="javascript:history.go(-1);" class="btn btn-default">Terug</a>
<hr/>
<pre class="print-this">Betreft:
Pati&euml;ntnummer: {{ $operation['patientnummer'] }}
Pati&euml;nt: {{ $operation['patientnaam'] }} ({{ $operation['geslacht'] }}) 
Geboortedatum: {{ $operation['geboortedatum'] }}

Geachte collega,
Op {{ $operation['operatiedatum'] }} is heeft bovengenoemde patient in ons ziekenhuis een scheelziensoperatie ondergaan.
Voor onze kwalteitsregistratie zouden we graag beschikken over de resultaten na 5 maanden

Het gaat om de volgende parameters (indien van toepassing):
Horizontale hoek (manifest, afstand)
Verticale hoek (manifest, afstand)
Cyclo-hoek 
Manfestatie scheelzien (constant intermitterend latent)
Sensoriek (niet gespecificeerd, suppressie fusie stereozien)
Eventuele complicaties en/of problemen

Hartelijk dank voor de te nemen moeite

Met collegiale hoogachting,</pre>

	{!! Layout::panel_end() !!}
{!! Layout::row_end() !!}
@endsection