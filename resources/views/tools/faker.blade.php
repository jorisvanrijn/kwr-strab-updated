@extends('layouts.app')

@section('content')
{!! Layout::row_start() !!}
	{!! Layout::panel_start('<a href="javascript:history.go(-1);"><i class="icon icon-arrow-left"></i></a> Faker', 3) !!}
		
		@if($demodb != 'ja')
			
		@else
			<p class="text-warning">Let op: u gebruikt een demo-database. Upload geen demonstratie data naar de landelijke database.</p>
		@endif

		<p>Vul de lokale database met gegenereerde, aannemelijke data.</p>
		<p class="text-warning"><strong>Let op:</strong> Gebruik deze functie alleen wanneer u van plan bent de database enkel ter demonstratie te gebruiken.</p>

		<form action="{{ url('tools/faker') }}" method="POST">
			{{ method_field('POST') }}
			{{ csrf_field() }}

			<strong>Aantal rijen</strong><br/>
			<input type="text" name="aantal" class="form-control input-block-level">

			<br/>
			<input type="submit" value="Vul database" class="btn btn-primary">
		</form>

	{!! Layout::panel_end() !!}
{!! Layout::row_end() !!}
@endsection