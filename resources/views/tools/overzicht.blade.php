@extends('layouts.app')

@section('content')
{!! Layout::row_start() !!}
	{!! Layout::panel_start('Account', 3) !!}
		
		<a href="{{ url('tools/credentials') }}"><strong>&raquo; Verander inloggegevens</strong></a><br/>Verander hier uw gebruikersnaam en/of wachtwoord<br/><br/>

		<a href="{{ url('tools/import') }}"><strong>&raquo; Oude account data importeren</strong></a><br/>Importeer operaties vanuit de In Summa database<br/><br/>

	{!! Layout::panel_end() !!}
	{!! Layout::panel_start('Operaties', 3) !!}
		
		<a href="{{ url('tools/visitationreport') }}"><strong>&raquo; Visitatierapport</strong></a><br/>Genereer een visitatierapport voor een specifieke periode
		<br/><br/>
		<a href="{{ url('tools/followupletter') }}"><strong>&raquo; Follow up brief</strong></a><br/>Genereer follow up verzoek brieven voor specifieke pati&euml;nten
		<br/><br/>
		<a href="{{ url('tools/export') }}"><strong>&raquo; Exporteer</strong></a><br/>Exporteer operaties naar verschillende bestandsformaten

	{!! Layout::panel_end() !!}
	{!! Layout::panel_start('Overig', 3) !!}
		
		<a href="{{ url('tools/faker') }}"><strong>&raquo; Demonstratie data</strong></a><br/>Vul de lokale database met data ter demonstratie<br/><br/>

		<a href="{{ url('tools/currentjob') }}"><strong>&raquo; Wachtrij</strong></a><br/>Bekijk acties die worden uitgevoerd<br/><br/>

	{!! Layout::panel_end() !!}
{!! Layout::row_end() !!}
@endsection