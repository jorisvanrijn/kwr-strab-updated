@extends('layouts.app')

@section('content')
{!! Layout::row_start() !!}
	{!! Layout::panel_start('<a href="javascript:history.go(-1);"><i class="icon icon-arrow-left"></i></a> Importeer InSumma data', 3) !!}
		
		<p>Voer hier uw oude gebruikersnaam van InSumma in en de authenticatiecode die u later heeft opgegeven.</p>

		<form action="{{ url('tools/import') }}" method="POST">
			{{ method_field('POST') }}
			{{ csrf_field() }}

			<strong>Gebruikersnaam</strong><br/>
			<input type="text" name="username" class="form-control input-block-level" value="{{ $username }}">

			<strong>Oud wachtwoord</strong><br/>
			<input type="password" name="authcode" class="form-control input-block-level">

			<br/>
			<input type="submit" value="Haal data op" class="btn btn-primary">
		</form>

	{!! Layout::panel_end() !!}
{!! Layout::row_end() !!}
@endsection