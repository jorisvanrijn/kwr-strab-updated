@extends('layouts.app')

@section('content')
{!! Layout::row_start() !!}
	{!! Layout::panel_start('<a href="javascript:history.go(-1);"><i class="icon icon-arrow-left"></i></a> Importeer InSumma data', 3) !!}

		<form action="{{ url('tools/process_import') }}" method="POST">
			{{ method_field('POST') }}
			{{ csrf_field() }}

			<input type="hidden" name="account" value="{{ $account }}" />

			<p>Voor het opgegeven account ({{ $account }}) zijn er {{ $found_operations }} beschikbare operaties in de InSumma database gevonden. Wilt u deze importeren?</p>

			<p>Let op: Deze actie kan niet ongedaan gemaakt worden.</p>

			<br/>
			<input type="submit" value="Importeer {{ $found_operations }} operaties" class="btn btn-primary">
		</form>

	{!! Layout::panel_end() !!}
{!! Layout::row_end() !!}
@endsection