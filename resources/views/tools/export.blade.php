@extends('layouts.app')

@section('content')
{!! Layout::row_start() !!}
	{!! Layout::panel_start('<a href="javascript:history.go(-1);"><i class="icon icon-arrow-left"></i></a> Exporteer operaties', 6) !!}
		
		@if($filtered)
			<em><strong>{{ $operations }}</strong> gevonden operaties</em><br/><br/>
			<p class="noloading"><a href="{{ $xls_url }}" class="btn btn-primary">Download .XLS</a></p>
			<p class="noloading"><a href="{{ $xlsx_url }}" class="btn btn-primary">Download .XLSX</a></p>
			<p class="noloading"><a href="{{ $csv_url }}" class="btn btn-primary">Download .CSV</a></p>
		@else
			Maak een selectie van de operaties die u wilt exporteren. U kunt <a href="{{ url('operations/filters') }}">hier</a> nieuwe filters aanmaken.<br/>
			{!! FilterHelper::filter_interface($fields, $request, 'tools/export?filter=', -10) !!}
		@endif

	{!! Layout::panel_end() !!}
{!! Layout::row_end() !!}
@endsection

@section('footer')
	{!! FilterHelper::filter_logic($fields); !!}
@endsection