@extends('layouts.app')

@section('content')
{!! Layout::row_start() !!}
	{!! Layout::panel_start('<a href="javascript:history.go(-1);"><i class="icon icon-arrow-left"></i></a> Genereer visitatierapport', 3) !!}
		
		<p>Alle operaties met een OK-datum tussen de opgegeven periode zullen meegenomen worden in het rapport.</p>

		<form action="{{ url('tools/visitationreport') }}" method="POST">
			{{ method_field('POST') }}
			{{ csrf_field() }}

			<strong>Periode</strong><br/>
			<input type="text" name="datum_start" id="datum_start" class="datepicker input-block-level" value="{{ date('Y-m-d') }}"> tot <input type="text" name="datum_eind" id="datum_eind" class="datepicker input-block-level" value="{{ date('Y-m-d') }}">

			<br/>
			<input type="submit" value="Genereer visitatierapport" class="btn btn-primary">
		</form>

	{!! Layout::panel_end() !!}
{!! Layout::row_end() !!}
@endsection