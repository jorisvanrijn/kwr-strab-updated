@extends('layouts.app')

@section('content')
{!! Layout::row_start() !!}
	{!! Layout::panel_start('Visitatierapport ('.$datum_start.' tot '.$datum_eind.')', 5) !!}
	
<a href="#" class="btn btn-primary print-button"><i class="icon icon-white icon-print"></i> Print</a> <a href="javascript:history.go(-1);" class="btn btn-default">Terug</a>
<hr/>
<pre class="print-this">
<h4>Algemeen</h4><table class="table table-bordered">
<tr><td>Populatie: &nbsp;&nbsp;</td><td>{{ $operations_count }}</td></tr>
<tr><td>Periode: &nbsp;&nbsp;</td><td>{{ $datum_start }} tot {{ $datum_eind }}</td></tr>
<tr><td>Kenmerk: &nbsp;&nbsp;</td><td>{{ $user_id }}-{{ date('dmY') }}</td></tr>
<tr><td>Generatiedatum: &nbsp;&nbsp;</td><td>{{ date('d-m-Y') }}</td></tr>
</table><h4>Operatie type</h4><table class="table table-bordered">
@for ($i = 0; $i < count($operation_type); $i++)
<tr><td>{{ $operation_type[$i]['operation_type'] }}: &nbsp;&nbsp;</td><td>{{ $operation_type[$i]['total'] }}</td></tr>
@endfor
</table><h4>Type eso</h4><table class="table table-bordered">
@for ($i = 0; $i < count($type_esodeviaties); $i++)
@if($type_esodeviaties[$i]['type_esodeviaties'] != 'nvt')
<tr><td>{{ $type_esodeviaties[$i]['type_esodeviaties'] }}: &nbsp;&nbsp;</td><td>{{ $type_esodeviaties[$i]['total'] }}</td></tr>
@endif
@endfor
</table><h4>Type exo</h4><table class="table table-bordered">
@for ($i = 0; $i < count($type_exodeviaties); $i++)
@if($type_exodeviaties[$i]['type_exodeviaties'] != 'nvt')
<tr><td>{{ $type_exodeviaties[$i]['type_exodeviaties'] }}: &nbsp;&nbsp;</td><td>{{ $type_exodeviaties[$i]['total'] }}</td></tr>
@endif
@endfor
</table><h4>Type overig</h4><table class="table table-bordered">
@for ($i = 0; $i < count($type_overige_deviaties); $i++)
@if($type_overige_deviaties[$i]['type_overige_deviaties'] != 'nvt')
<tr><td>{{ $type_overige_deviaties[$i]['type_overige_deviaties'] }}: &nbsp;&nbsp;</td><td>{{ $type_overige_deviaties[$i]['total'] }}</td></tr>
@endif
@endfor
</table>
</pre>

	{!! Layout::panel_end() !!}
{!! Layout::row_end() !!}
@endsection