@extends('layouts.app')

@section('content')
{!! Layout::row_start() !!}
	{!! Layout::panel_start('<a href="javascript:history.go(-1);"><i class="icon icon-arrow-left"></i></a> Inloggegevens', 3) !!}
		
		<p>Hier kunt u uw gebruikersnaam en wachtwoord veranderen.</p>

		<form action="{{ url('tools/credentials') }}" method="POST">
			{{ method_field('POST') }}
			{{ csrf_field() }}

			<strong>Gebruikersnaam</strong><br/>
			<input type="text" name="username" class="form-control input-block-level" value="{{ $username }}">

			<strong>Oud wachtwoord</strong><br/>
			<input type="password" name="old_pass" class="form-control input-block-level">

			<strong>Nieuw wachtwoord</strong><br/>
			<input type="password" name="new_pass" class="form-control input-block-level">

			<strong>Nieuw wachtwoord heralen</strong><br/>
			<input type="password" name="new_pass_2" class="form-control input-block-level">

			<br/>
			<input type="submit" value="Opslaan" class="btn btn-primary">
		</form>

	{!! Layout::panel_end() !!}
{!! Layout::row_end() !!}
@endsection