@extends('layouts.app')

@section('content')
{!! Layout::row_start() !!}
	{!! Layout::panel_start('Statistieken: Lokale data', 3) !!}
		
		<h1>{{ ($pool_operations_count) }} <small>pool {{ ((($pool_operations_count) == 1) ? 'operatie' : 'operaties') }} +</small></h1>
		<h1>{{ ($eso_operations_count) }} <small>eso {{ ((($eso_operations_count) == 1) ? 'operatie' : 'operaties') }} +</small></h1>
		<h1>{{ ($exo_operations_count) }} <small>exo {{ ((($exo_operations_count) == 1) ? 'operatie' : 'operaties') }} +</small></h1>
		<hr/>
		<h3><small>=</small> {{ $eigen_operaties }} <small> lokale {{ (($eigen_operaties == 1) ? 'operatie' : 'operaties') }}</small></h3>

		@if($demodb == 'ja')
		<div class="alert">
		  Omdat u een test- of demodatabase gebruikt raden wij u aan uw database met <a href="{{ url('tools/faker') }}">hier</a> fake data te vullen.
		</div>
		@endif

	{!! Layout::panel_end() !!}

	{!! Layout::panel_start('Statistieken: Landelijke data', 3) !!}
		
		<!-- <h1>0 <small>{{ ((0 == 1) ? 'set' : 'sets') }}</small></h1> -->
		
		<h1>{{ $online_eigen_operaties }} <small> ge&uuml;ploade {{ (($eigen_operaties == 1) ? 'operatie' : 'operaties') }} vanuit eigen database in </small> {{ $eigen_sets }} <small>ge&uuml;ploade {{ (($eigen_sets == 1) ? 'set' : 'sets') }}</small></h1>
		<hr/>
		<h3>{{ $landelijke_operaties }} <small>{{ (($landelijke_operaties == 1) ? 'operatie' : 'operaties') }} in totaal door</small> {{ DB::table('users')->count() }} <small>{{ (((DB::table('users')->count()-1) == 1) ? 'arts' : 'artsen') }}</small></h3>

	{!! Layout::panel_end() !!}

	{!! Layout::panel_start('Beschikbaar voor uploaden', 3) !!}
		<h1>
			@if($pool_operations_count_upl > 0) <span style="color: #468847;">&#x2713;</span> @else <span style="color: #b94a48;">&#x2717;</span> @endif 
			{{ ($pool_operations_count_upl) }} <small>pool {{ ((($pool_operations_count_upl) == 1) ? 'operatie' : 'operaties') }}</small> 
		</h1>

		<h1>
			@if($eso_operations_count_upl >= $set_size) <span style="color: #468847;">&#x2713;</span> @else <span style="color: #b94a48;">&#x2717;</span> @endif 
			{{ ($eso_operations_count_upl) }} <small>eso {{ ((($eso_operations_count_upl) == 1) ? 'operatie' : 'operaties') }}</small>
		</h1>

		<h1>
			@if($exo_operations_count_upl >= $set_size) <span style="color: #468847;">&#x2713;</span> @else <span style="color: #b94a48;">&#x2717;</span> @endif 
			{{ ($exo_operations_count_upl) }} <small>exo {{ ((($exo_operations_count_upl) == 1) ? 'operatie' : 'operaties') }}</small>
		</h1>
		<hr/>
		@if($demodb == 'ja')
		<div class="alert">
		  U gebruikt een test- of demoversie van de database. U kunt de landelijke database niet bewerken.
		</div>
		@else
		<p><a href="{{ url('operations/upload') }}" class="btn btn-primary">Operaties uploaden</a></p>
		@endif
	{!! Layout::panel_end(); !!}

	{!! Layout::panel_start('<i class="icon icon-info-sign"></i> Over het Kwaliteitsregister', 3) !!}
		<p>Het Kwaliteitsregister is uw persoonlijke database waarin u operaties bij kunt houden. Door operaties te uploaden kunt u uwzelf met de landelijke statistieken vergelijken. Daarnaast is deze persoonlijke database uw naslag werk.</p>
		<p>Kwaliteitsregister 3.0 is ontwikkeld door <br/><a href="http://www.dutchcodingcompany.com">The Dutch Coding Company</a>.</p>
		<hr/>
		<center><img src="{{ asset('assets/img/text.png') }}" style="width: 70%;"/><br/>
		(<small>versie {{ $versie }}</small>) 
		<small>&copy; {{ date('Y') }} - Dutch Coding Company</small></center>
	{!! Layout::panel_end() !!}		
{!! Layout::row_end() !!}
@endsection