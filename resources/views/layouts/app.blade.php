<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    @if(isset($title))
        <title>Kwaliteitsregister | {{ $title }}</title>
    @else
        <title>Kwaliteitsregister</title>
    @endif

    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/img/icon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets/img/icon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/img/icon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/icon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/img/icon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/img/icon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/img/icon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/img/icon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/img/icon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('assets/img/icon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/img/icon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('assets/img/icon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/img/icon/favicon-16x16.png') }}">
    <link rel="manifest" href="img/icon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('assets/img/icon/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">

    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="_token" content="{!! csrf_token() !!}"/>

    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" media="screen">
    <link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet" media="screen">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
    <!-- <link href="{{ asset('assets/css/bootstrap-responsive.css') }}" rel="stylesheet"> -->
    <link href="{{ asset('assets/css/custom.kwr.css') }}" rel="stylesheet" media="screen">
    <link href="{{ asset('assets/css/pnotify.custom.min.css') }}" rel="stylesheet" media="screen">
    <link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet" media="screen">
    <link href="{{ asset('assets/css/query-builder.default.min.css') }}" rel="stylesheet" media="screen">
    <link href="{{ asset('assets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" media="screen">

    <script src="https://code.jquery.com/jquery.js"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
  </head>
  <body class="{{ $controller }}">
    @yield('bodyheader')
    @if(Auth::check() AND Auth::user()->userid == 'admin')
    <div class="adminindicatorbar">administration area</div>
    @endif

    @if (!Auth::guest())
        @include('partials.menu.navbar')
    @endif

    
        <div class="container-fluid button-bar" @if (!View::hasSection('buttons')) style="height: 10px; padding: 0;" @endif>
        
                    @yield('buttons')
        
        </div>
    
    
    @if(isset($title) AND false)
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6">
                    <h2 class="maintitle">{{ $title }}</h2>
                </div>
            </div>
        </div>
    @endif

    @if(isset($queueready))
        <div style="margin-top: 20px;">
        {{ $queueready }}
        </div>
    @endif

    <div id="content_area" style="margin-top: 20px;">
    @yield('content')
    </div>

    @yield('bodyfooter')

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script src="{{ asset('assets/js/masonry.pkgd.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-scrolltofixed-min.js') }}"></script>
    <script src="{{ asset('assets/js/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('assets/js/pnotify.custom.min.js') }}"></script>
    <script src="{{ asset('assets/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-sortable-min.js') }}"></script>
    <script src="{{ asset('assets/js/jQuery.extendext.min.js') }}"></script>
    <script src="{{ asset('assets/js/doT.min.js') }}"></script>
    <script src="{{ asset('assets/js/moment.js') }}"></script>
    <script src="{{ asset('assets/js/query-builder.min.js') }}"></script>
    <script src="{{ asset('assets/js/query-builder.nl.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/js/sql-parser.js') }}"></script>
    <script src="{{ asset('assets/js/jQuery.print.js') }}"></script>
    <script src="https://js.pusher.com/3.2/pusher.min.js"></script>

    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}

    <script type="text/javascript">
        var stack_bottomleft = {"dir1": "up", "dir2": "right", "firstpos1": 25, "firstpos2": 25};
        
        $(document).ready(function(){

            $('.print-button').click(function(){
                $('.print-this').print();
            });

            $('.datepicker').datepicker({
                format: 'yyyy-mm-dd'
            });

            @if(isset($sorttype))
                $('.sortabletable').rowSorter({handler: '.sorter', onDrop: function(tbody, row, new_index, old_index){
                    var orderedids = [];

                    $(tbody).find('tr').each(function(e){
                        orderedids.push($(this).data('id'));
                    });

                    // console.log(orderedids);

                    $.ajax( {type:"POST", url: "{{ url('ajax/setorder') }}", headers: { 'X-CSRF-Token' : "{!! csrf_token() !!}" }, data: {'sorttype': '{{ $sorttype }}', 'orderedids': orderedids, '_token' : "{!! csrf_token() !!}" }, success: function( data ) {
                      console.log( data );
                    }} );
                }});
            @endif 

            $("select").select2();

            // enter as tab
             $('input').keydown( function(e) {
                var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
                if(key == 13) {
                    e.preventDefault();
                    var inputs = $(this).closest('form').find(':input:visible');
                    inputs.eq( inputs.index(this)+ 1 ).focus();
                }
            });

            @if (isset($job_data) AND $action != 'currentjob')
                new PNotify({
                    text: 'Het systeem is op de achtergrond bezig.<br/><br/><a href="{{ url('tools/currentjob') }}" class="btn btn-warningn btn-small">Bekijk voortgang</a>',
                    type: 'info',
                    addclass: "stack-bottomleft",
                    stack: stack_bottomleft,
                    hide: false
                });
            @else
                <!-- {!! Layout::spacing() !!} -->
            @endif
            
            @if (session('status'))
                new PNotify({
                    text: '{!! session('status') !!}',
                    @if(session('type'))
                        type: '{{ session('type' )}}',
                    @else
                        type: null,
                    @endif
                    addclass: "stack-bottomleft",
                    stack: stack_bottomleft
                });
            @else
                <!-- {!! Layout::spacing() !!} -->
            @endif

            $('[data-toggle=tooltip]').tooltip();

            $('.closinglink').click(function(e){
                e.preventDefault();
                
                if($(this).html() == '[-]'){
                    $(this).html('[+]');
                }else if($(this).html() == '[+]'){
                    $(this).html('[-]');
                }

                $(this).parent().parent().parent().find('.panel-body').toggle();
                return false;
            });

            $(".delete-link").on("click", null, function(){
                return confirm("Weet u zeker dat u " + $(this).data('deletemsg') + " wilt verwijderen? Dit kan niet ongedaan worden.");
            });

            $('.grid').masonry({
              itemSelector: '.grid-item'
            });

            $('.button-bar').scrollToFixed();

            // $('.filter-box').scrollToFixed({ marginTop: 70 });

            $.LoadingOverlaySetup({
                image           : "{{ asset('assets/img/loading.gif') }}"
            });

            $('a').not('.noloading a, .delete-link, .noloadinglink').click(function(){ 
                $('#content_area').LoadingOverlay("show"); 
            });
            $('form').submit(function(){ $('#content_area').LoadingOverlay("show"); });

            $('.save-button').click(function(){
                $(this).html('Een ogenblik geduld a.u.b.');
            });

            $('.deg-to-dpt').keyup(function(){
                // console.log($(this).data('pairedwith'));
                var deg = $(this).val();
                if(deg == '') return;
                var dpt = round((getTanDeg(deg)*100), 4);
                console.log(dpt);
                $('.dpt_' + $(this).data('pairedwith')).val(dpt);
            });

            $('.dpt-to-deg').keyup(function(){
                // console.log($(this).data('pairedwith'));
                var dpt = $(this).val();
                if(dpt == '') return;
                var deg = round((Math.atan(dpt/100)) * (180/Math.PI), 4);
                console.log(deg);
                $('.deg_' + $(this).data('pairedwith')).val(deg);
            });

            function getTanDeg(deg) {
              var rad = deg * Math.PI/180;
              return Math.tan(rad);
            }

            function getaTanDeg(deg) {
              var rad = deg * Math.PI/180;
              return Math.atan(deg);
            }

            function round(value, decimals) {
                return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
            }

        });
    </script>

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-86861359-1', 'auto');
      ga('send', 'pageview');

    </script>

    @yield('footer')
  </body>
</html>