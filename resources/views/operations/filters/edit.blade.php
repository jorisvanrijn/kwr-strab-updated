@extends('layouts.app')

@section('buttons')
	<!-- <input type="submit" value="Filter opslaan" class="btn btn-primary"> -->
	<span class="noloading"><a href="#" class="btn btn-default" id="usefilter">Verfijn selectie</a></span>
	<span class="noloading"><a href="#" class="btn btn-primary" id="savefilter">Filter opslaan</a></span>
@endsection

@section('content')

	{!! Layout::row_start() !!}

		{!! Layout::panel_start('Nieuwe filter', 10) !!}

			<label>Filter naam:</label>
			{!! Layout::error_and_success_msg($errors, 'key') !!}
			<input type="text" name="key" class="input-block-level" placeholder="Naam" value="{{ $filterkey }}" />

			<div id="builder"></div>

			<input type="hidden" name="value" value="" id="sqldata"/>
			<input type="hidden" name="actionbutton" value="" id="actionbutton"/>
			<input type="hidden" name="prevurl" value="{{ $prevurl }}"/>

		{!! Layout::panel_end() !!}

		{!! csrf_field() !!}
		{!! method_field('POST') !!}
		
	{!! Layout::row_end() !!}

@endsection

@section('bodyheader')
	<form method="POST" action="{{ url('operations/edit_filter/'.$filterid	) }}" role="form" id="theform">
@endsection

@section('bodyfooter')
	</form>
@endsection

@section('footer')
<script text="text/javascript">
$(document).ready(function(){
	$('#savefilter').on('click', function() {
	  var result = $('#builder').queryBuilder('getSQL');

	  if (result.sql.length) {
	    console.log(result.sql);
	    $('#sqldata').val(result.sql);
	    $('#actionbutton').val('save');
	    $('#theform').submit();
	  }
 	});

 	$('#usefilter').on('click', function() {
	  var result = $('#builder').queryBuilder('getSQL');

	  if (result.sql.length) {
	    console.log(result.sql);
	    $('#sqldata').val(result.sql);
	    $('#actionbutton').val('use');
	    $('#theform').submit();
	  }
 	});

	// var rules_basic = {
	//   condition: 'AND',
	//   rules: [{
	//     id: 'price',
	//     operator: 'less',
	//     value: 10.25
	//   }, {
	//     condition: 'OR',
	//     rules: [{
	//       id: 'category',
	//       operator: 'equal',
	//       value: 2
	//     }, {
	//       id: 'category',
	//       operator: 'equal',
	//       value: 1
	//     }]
	//   }]
	// };
	
	$('#builder').queryBuilder({
	  plugins: ['bt-tooltip-errors'],
	  
	  <?php
	  	$filtersettings = [];
	  	foreach($fields as $k => $v){
			if( (isset($v['filterable']) AND $v['filterable'] != false) OR !isset($v['filterable']) ){
				$_filtersettings = [];
				$_filtersettings['id'] = $v['key'];
				$_filtersettings['label'] = $v['trivial'];
				$_filtersettings['type'] = 'string';

				// jquery query builder types: string, integer, double, string
				// jquery query builder input: select (assoc values array), radio (assoc values array)
				// bij select: operators: ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null']
				// bij radio: operators: ['equal']
				// bij string: operators: ['equal', 'not_equal'],
				// $v['laravel_db_function'];
				
				switch ($v['laravel_db_function']) {
					case 'enum':
						$_filtersettings['type'] = 'string';
						$_filtersettings['input'] = 'select';
						$_filtersettings['values'] = $v['choices'];
						break;
					case 'integer':
						$_filtersettings['type'] = 'integer';
						break;
					case 'boolean':
						$_filtersettings['type'] = 'boolean';
						break;
					case 'double':
						$_filtersettings['type'] = 'double';
						break;
					case 'degdpt':
						$_filtersettings['type'] = 'double';
						break;
					case 'date':
						$_filtersettings['type'] = 'date';
						$_filtersettings['plugin'] = 'datepicker';
						$_filtersettings['validation']['format'] = 'yyyy-mm-dd';
						$_filtersettings['plugin_config'] = [
							'format' => 'yyyy-mm-dd',
							'todayBtn' => 'linked',
							'todayHighlight' => true,
							'autoclose' => true
						];
						break;
					default:
						$_filtersettings['type'] = 'string';
						break;
				}
				
				$filtersettings[] = $_filtersettings;
			}
		}
		echo 'filters: '.json_encode($filtersettings).',';
	  ?>

	  // rules: {{ json_encode($filtervalue) }}
	});

	$('#builder').queryBuilder('setRulesFromSQL', '{!! $filtervalue !!}');

});
</script>
@endsection