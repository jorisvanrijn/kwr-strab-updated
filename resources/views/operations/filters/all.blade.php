@extends('layouts.app')

@section('buttons')
	<a href="{{ url('operations/new_filter') }}" class="btn btn-primary">Filter toevoegen</a>
@endsection

@section('content')

{!! Layout::row_start() !!}

	{!! Layout::panel_start('Filters', 3) !!}
		Filters bepalen welke operaties getoond moeten worden of meegenomen moeten worden in een analyse of export. U kunt hier filters gebruiker of verwijderen.
	{!! Layout::panel_end() !!}
	
	{!! Layout::panel_start('Filters: '.$filters->total() . ' '. (($filters->total() == 1) ? 'resultaat' : 'resultaten') . ' op '.$filters->lastPage() . ' '. (($filters->lastPage() == 1) ? 'pagina' : 'pagina\'s') . Layout::export_button($request), 5, true) !!}

		@if($filters->total() > 0)
			{!!
				$filters->columns(array(
					'created_at' => 'Filter',
			        'edit' => ''
			    ))

			    ->modify('created_at', function($row) {
			    	$t = DateTime::createFromFormat('Y-m-d H:i:s', $row->created_at);
			    	if (!$t) throw new \UnexpectedValueException("Could not parse the date: $row->created_at");
			        return '<strong><i class="icon icon-resize-vertical sorter"></i> '.$row->key.'</strong><br/> '.$t->format('d-m-Y H:i:s');
			    })

			    ->modify('edit', function($row) {

			    	$x = 'operations';
			    	if(isset($_GET['redirect'])) $x = $_GET['redirect'];

			    	if($row['value2'] != 'predefined'){ return '

			    	<!-- <a href="'.url($x.'/?'.$row->value).'" class="btn btn-mini btn-default" data-toggle="tooltip" title="Gebruiker filter"><i class="icon icon-arrow-right"></i></a> -->

			    	<a href="'.url('operations/edit_filter/'.$row->id).'" class="btn btn-mini btn-default" data-toggle="tooltip" title="Bewerk filter"><i class="icon icon-pencil"></i></a>

			    	<a href="'.url('operations/del_filter/'.$row->id).'" data-deletemsg="deze filter" class="delete-link btn btn-mini btn-default" data-toggle="tooltip" title="Verwijder"><i class="icon icon-trash"></i></a>

			    	<!--<a href="'.url('operations/del_filter/'.$row->id).'"><i class="icon icon-trash"></i></a>-->';
			    	}else{

			    		return '<em><small class="muted">Niet aanpasbaar</small></em>';
			    		return '<!-- <a href="'.url($x.'/?'.$row->value).'" class="btn btn-mini btn-default" data-toggle="tooltip" title="Gebruiker filter"><i class="icon icon-arrow-right"></i></a> -->';

			    	}
			    })

			    ->modifyRow('mod1', function($row) {
			        return array('data-id' => $row->id);
			    })

			    ->attributes(array(
			        'id' => 'filters',
			        'class' => 'table sortabletable',
			        'style' => 'margin-bottom: 0px;',
			    ))

				->sortable(array('datum_operatie', 'geslacht'))
	    		->showPages()
			    ->render()
			!!}
		@else
			<div class="alert alert-info" style="margin:20px;">Geen rijen gevonden</div>
		@endif

		<div class="text-left pagination" style="margin:0;margin-left:15px;">{!! $filters->links() !!}</div>

	{!! Layout::panel_end() !!}

{!! Layout::row_end() !!}

@endsection