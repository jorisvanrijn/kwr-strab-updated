@extends('layouts.app')

@section('buttons')
	<input type="submit" value="Set opslaan" class="btn btn-primary">
	<div class="editted">Toegevoegd op: <strong>{{ $set->created_at }}</strong> | Laatst bewerkt op: <strong>{{ $set->updated_at }}</strong></div>
@endsection

@section('content')

	{!! Layout::row_start() !!}

		{!! Layout::panel_start('Set: '.$set->key, 4) !!}

			<label>Notitie voor set:</label>
			{!! Layout::error_and_success_msg($errors, 'value') !!}
			<textarea name="value" placeholder="Notitie" class="input-block-level">{{ $set->value }}</textarea>

		{!! Layout::panel_end() !!}

		{!! csrf_field() !!}
		{!! method_field('POST') !!}
		
	{!! Layout::row_end() !!}

@endsection

@section('bodyheader')
	<form method="POST" action="{{ url('operations/edit_set/'.$id) }}" role="form">
@endsection

@section('bodyfooter');
	</form>
@endsection