@extends('layouts.app')

@section('content')

{!! Layout::row_start() !!}

	{!! Layout::panel_start('Beschikbare operaties', 3) !!}
		<h1>
			@if($pool_operations_count > 0) <span style="color: #468847;">&#x2713;</span> @else <span style="color: #b94a48;">&#x2717;</span> @endif 
			{{ ($pool_operations_count) }} <small>pool {{ ((($pool_operations_count) == 1) ? 'operatie' : 'operaties') }}</small> 
		</h1>

		<h1>
			@if($eso_operations_count >= $set_size) <span style="color: #468847;">&#x2713;</span> @else <span style="color: #b94a48;">&#x2717;</span> @endif 
			{{ ($eso_operations_count) }} <small>eso {{ ((($eso_operations_count) == 1) ? 'operatie' : 'operaties') }}</small>
		</h1>

		<h1>
			@if($exo_operations_count >= $set_size) <span style="color: #468847;">&#x2713;</span> @else <span style="color: #b94a48;">&#x2717;</span> @endif 
			{{ ($exo_operations_count) }} <small>exo {{ ((($exo_operations_count) == 1) ? 'operatie' : 'operaties') }}</small>
		</h1>
	{!! Layout::panel_end(); !!}	

	{!! Layout::panel_start('Uploaden', 4) !!}

		<p>Wanneer er genoeg operaties worden gevonden in de lokale database worden de operaties in de landelijke database geplaatst. Operaties met het type 'eso' of 'exo' worden in sets van {{ $set_size }} ge&uuml;pload. Overige operaties worden los in de landelijke database gezet. De operaties worden (oplopend) gesorteerd op datum van toevoegen.</p>
		<p>Nadat operaties geupload zijn kunnen de meeste variabelen niet meer aangepast worden. Enkel waarden die uw eigen administratie betreffen kunnen hierna aangepast worden.</p>
		<!-- <p>Er kunnen maximaal 2 sets en 100 pool operaties tegelijk geupload worden.</p> -->

		@if($demodb == 'ja')
		<div class="alert">
		  U gebruikt een test- of demoversie van de database. U kunt de landelijke database niet bewerken.
		</div>
		@else
		<p>{!! $btn !!}</p>
		@endif

	{!! Layout::panel_end(); !!}

{!! Layout::row_end(); !!}

@endsection