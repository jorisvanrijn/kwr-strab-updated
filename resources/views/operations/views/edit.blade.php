@extends('layouts.app')

@section('buttons')
	<input type="submit" value="View opslaan" class="btn btn-primary">
	<div class="editted">Toegevoegd op: <strong>{{ $view->created_at }}</strong> | Laatst bewerkt op: <strong>{{ $view->updated_at }}</strong></div>
@endsection

@section('content')

	{!! Layout::row_start() !!}

		{!! Layout::panel_start('View informatie', 4) !!}

			<label>View naam:</label>
			{!! Layout::error_and_success_msg($errors, 'key') !!}
			<input type="text" name="key" class="input-block-level" placeholder="Naam" value="{{ $view->key }}"/>

			<hr/>

			{!! Layout::sup_manual() !!}

		{!! Layout::panel_end() !!}

		{!! Layout::panel_start('View velden', 5) !!}

			{!! Layout::error_and_success_msg($errors, 'value') !!}

			@foreach($categories as $c => $d)

				<h4>{{ $d['trivial'] }}</h4>

				@foreach($fields as $k => $v)

					@if( (isset($v['hidden']) AND !$v['hidden']) OR !isset($v['hidden']) AND $v['is_actual_field'] AND $v['category'] == $c)
						<input type="checkbox" name="tostoreinview[]" value="{{ $k }}" id="{{ $k }}" @if(in_array($k, $inview))
						 {{ ' checked="checked"' }} @endif> <label for="{{ $k }}" style="display: inline;"> {{ $v['trivial'] }}

						@if(isset($v['required_for']) AND in_array('save', $v['required_for']))
							<sup>1</sup>
						@endif
						@if(isset($v['required_for']) AND in_array('upload', $v['required_for']))
							<sup>2</sup>
						@endif
						@if(isset($v['upload']) AND $v['upload'])
							<sup>3</sup>
						@endif

						<br/>
					@endif

				@endforeach

				<hr/>
			@endforeach

		{!! Layout::panel_end() !!}

		{!! csrf_field() !!}
		{!! method_field('POST') !!}
		
	{!! Layout::row_end() !!}

@endsection

@section('bodyheader')
	<form method="POST" action="{{ url('operations/edit_view/'.$id) }}" role="form">
@endsection

@section('bodyfooter')
	</form>
@endsection