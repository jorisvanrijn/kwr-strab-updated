@extends('layouts.app')

@section('buttons')
	<a href="{{ url('operations/new_view') }}" class="btn btn-primary">View toevoegen</a>
@endsection

@section('content')

{!! Layout::row_start() !!}

	{!! Layout::panel_start('Views', 3) !!}
		Views bepalen welke velden er per operatie getoond worden. U kunt hier uw views aanpassen.
	{!! Layout::panel_end() !!}
	
	{!! Layout::panel_start('Views: '.$views->total() . ' '. (($views->total() == 1) ? 'resultaat' : 'resultaten') . ' op '.$views->lastPage() . ' '. (($views->lastPage() == 1) ? 'pagina' : 'pagina\'s') . Layout::export_button($request), 5, true) !!}

		@if($views->total() > 0)
			{!!
				$views->columns(array(
					'created_at' => 'View',
			        'edit' => ''
			    ))

			    ->modify('created_at', function($row) {
			    	$t = DateTime::createFromFormat('Y-m-d H:i:s', $row->created_at);
			    	if (!$t) throw new \UnexpectedValueException("Could not parse the date: $row->created_at");
			        return '<strong><i class="icon icon-resize-vertical sorter"></i> '.$row->key.'</strong><br/> '.$t->format('d-m-Y H:i:s');
			    })

			    ->modify('edit', function($row) {

			    	$x = 'operations';
			    	if(isset($_GET['redirect'])) $x = $_GET['redirect'];

			    	if($row['value2'] != 'predefined'){ return '

			    	<!-- <a href="'.url($x.'/?'.$row->value).'" class="btn btn-mini btn-default" data-toggle="tooltip" title="Gebruiker view"><i class="icon icon-arrow-right"></i></a> -->

			    	<a href="'.url('operations/edit_view/'.$row->id).'" class="btn btn-mini btn-default" data-toggle="tooltip" title="Bewerk view"><i class="icon icon-pencil"></i></a>

			    	<a href="'.url('operations/del_view/'.$row->id).'" data-deletemsg="deze view" class="delete-link btn btn-mini btn-default" data-toggle="tooltip" title="Verwijder"><i  class="icon icon-trash"></i></a>

			    	<!--<a href="'.url('operations/del_view/'.$row->id).'"><i class="icon icon-trash"></i></a>-->';
			    	}else{
			    	return '<em><small class="muted">Niet aanpasbaar</small></em>';
			    	}
			    })

			    ->modifyRow('mod1', function($row) {
			        return array('data-id' => $row->id);
			    })

			    ->attributes(array(
			        'id' => 'views',
			        'class' => 'table sortabletable',
			        'style' => 'margin-bottom: 0px;',
			    ))

				->sortable(array('datum_operatie', 'geslacht'))
	    		->showPages()
			    ->render()
			!!}
		@else
			<div class="alert alert-info" style="margin:20px;">Geen rijen gevonden</div>
		@endif

		<div class="text-left pagination" style="margin:0;margin-left:15px;">{!! $views->links() !!}</div>

	{!! Layout::panel_end() !!}

{!! Layout::row_end() !!}

@endsection