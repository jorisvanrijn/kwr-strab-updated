@extends('layouts.app')

@section('buttons')
	<a href="{{ url('operations/new') }}" class="btn btn-primary">Operatie toevoegen</a>
@endsection

@section('content')

{!! Layout::row_start() !!}

	{!! Layout::panel_start('Filter operaties', 3, false, true) !!}
		{!! FilterHelper::filter_interface($fields, $request, '', $current_filter); !!}
	{!! Layout::panel_end() !!}


	{!! Layout::panel_start('Operaties: '.$operations->total() . ' '. (($operations->total() == 1) ? 'resultaat' : 'resultaten') . ' op '.$operations->lastPage() . ' '. (($operations->lastPage() == 1) ? 'pagina' : 'pagina\'s') . Layout::export_button($request), 9, true) !!}

		<div class="text-left pagination" style="margin:0;margin-left:15px;">{!! $operations->appends($request->all())->links() !!}</div>

		@if($operations->total() > 0)
			{!!
				$operations->columns(array(
			        'patientnummer' => 'Patientnummer',
			        'patientnaam' => 'Patient naam',
			        'operatiedatum' =>'Datum OK',
			        'uploaded_at' => 'Ge&uuml;pload',
			        'completed_at' => 'Compleet',
			        'operation_type' => 'Type',
			        'edit' => '',
			    ))
			    
			    ->modify('operatiedatum', function($row) {
			    	$t = DateTime::createFromFormat('Y-m-d', $row->operatiedatum);
			    	if (!$t) return ('[invalid date]');
			        return $t->format('d-m-Y');
			    })

			    ->modify('edit', function($row) {
			    	return '
			    	<span class="noloading"><a href="'.url('operations/view/'.$row->id).'" class="btn btn-mini btn-default" data-toggle="tooltip" title="Bekijk"><i class="icon icon-eye-open"></i></a> 
			    	<span class="noloading"><a href="'.url('tools/followupletter/'.$row->id).'" class="btn btn-mini btn-default" data-toggle="tooltip" title="Verzoek follow up"><i class="icon icon-envelope"></i></a> 
			    	<a href="'.url('operations/edit/'.$row->id).'" class="btn btn-mini btn-default" data-toggle="tooltip" title="Bewerk"><i class="icon icon-pencil"></i></a> 
			    	<a href="'.url('operations/del/'.$row->id).'" data-deletemsg="patient '.$row->patientnummer.'" class="delete-link btn btn-mini btn-default" data-toggle="tooltip" title="Verwijder"><i class="icon icon-trash"></i></a></span>
			    	';
			    })

			    ->modify('patientnummer', function($row) {
			    	return '<span class="noloading"><a href="'.url('operations/view/'.$row->id).'">'.$row->patientnummer.'</a></span>';
			    })

			    ->modify('uploaded_at', function($row) {
			    	return (is_null($row->uploaded_at) ? '<span class="label label-important">Nee</span>' : '<span class="label label-success">Ja</span>');
			    })

			    ->modify('completed_at', function($row) {
			    	return (is_null($row->completed_at) ? '<span class="label label-important">Nee</span>' : '<span class="label label-success">Ja</span>');
			    })

			    ->attributes(array(
			        'id' => 'operations',
			        'class' => 'table table-striped table-hover',
			        'style' => 'margin-bottom: 0px;',
			    ))

				// ->sortable(array('operatiedatum', 'geslacht'))
				
	    		->showPages()
			    ->render()
			!!}
		@else
			<div class="alert alert-info" style="margin:20px;">Geen rijen gevonden</div>
		@endif

		<div class="text-left pagination" style="margin:0;margin-left:15px;">{!! $operations->appends($request->all())->links() !!}</div>

	{!! Layout::panel_end() !!}

{!! Layout::row_end() !!}

@endsection

@section('footer')
	
@endsection