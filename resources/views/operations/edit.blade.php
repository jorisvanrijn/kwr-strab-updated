@extends('layouts.app')

@section('buttons')
	<input type="submit" value="Operatie opslaan" class="btn btn-primary">
	<div class="editted">Toegevoegd op: <strong>{{ $operation->created_at }}</strong> | Laatst bewerkt op: <strong>{{ $operation->updated_at }}</strong></div>
@endsection

@section('content')

	<div class="container-fluid"><div class="row-fluid">

		<div class="span3">
			{!! Layout::panel_start('Operatie toevoegen', 3, false, false) !!}
				{!! Layout::view_helper($request) !!}
				<hr/>
				{!! Layout::sup_manual() !!}

				@if(!is_null($operation->uploaded_at))
					<p class="text-info">Omdat deze operatie ge&uuml;pload is kunt u niet alle data meer aanpassen.</p>
				@endif
			{!! Layout::panel_end() !!}

			{!! Layout::operation_column('col1', $fields, $categories, $fields, $operation, $errors, $request) !!}
		</div>

		<div class="span3">
			{!! Layout::operation_column('col2', $fields, $categories, $fields, $operation, $errors, $request) !!}
		</div>

		<div class="span3">
			{!! Layout::operation_column('col3', $fields, $categories, $fields, $operation, $errors, $request) !!}
		</div>

		<div class="span3">
			{!! Layout::operation_column('col4', $fields, $categories, $fields, $operation, $errors, $request) !!}
		</div>

		{!! csrf_field() !!}
		{!! method_field('POST') !!}
		
	{!! Layout::row_end() !!}

@endsection

@section('bodyheader')
	<form method="POST" action="{{ url('operations/edit/'.$id) }}" role="form">
@endsection

@section('bodyfooter')
	</form>
@endsection

@section('footer')
	{!! Layout::view_logic($request) !!}
	<script type="text/javascript">
		$(document).ready(function(){
			$('#complicaties_no').change(function(){
				if($('#complicaties_no').is(':checked')){
					$('#perop_bloeding_no').prop('checked', true);
					$('#perop_perforatie_no').prop('checked', true);
					$('#perop_slipped_muscle_no').prop('checked', true);
					$('#perop_cornea_erosie_no').prop('checked', true);
					$('#perop_andere_oogheelkundig_no').prop('checked', true);
					$('#perop_andere_systemisch_no').prop('checked', true);
				}
			});
			$('#postop_complicaties_no').change(function(){
				if($('#postop_complicaties_no').is(':checked')){
					$('#postop_diplopie_no').prop('checked', true);
					$('#postop_extraoculare_infectie_no').prop('checked', true);
					$('#postop_intraocularie_infectie_no').prop('checked', true);
					$('#postop_allergie_no').prop('checked', true);
					$('#postop_conjunctivaproblemen_no').prop('checked', true);
					$('#postop_corneaproblemen_no').prop('checked', true);
					$('#postop_andere_oogheelkundig_no').prop('checked', true);
					$('#postop_andere_systemisch_no').prop('checked', true);
				}
			});
		});
	</script>
@endsection