@extends('layouts.app')

@section('buttons')
	<a href="{{ url('operations/edit/'.$id) }}" class="btn btn-primary">Operatie bewerken</a>
@endsection

@section('content')	

	<div class="container-fluid"><div class="row-fluid">

		<div class="span3">
			{!! Layout::panel_start('Operatie toevoegen', 3, false, false) !!}
				{!! Layout::view_helper($request) !!}
				<hr/>
				{!! Layout::sup_manual() !!}
			{!! Layout::panel_end() !!}

			{!! Layout::operation_column('col1', $fields, $categories, $fields, $operation, $errors, $request, false) !!}
		</div>

		<div class="span3">
			{!! Layout::operation_column('col2', $fields, $categories, $fields, $operation, $errors, $request, false) !!}
		</div>

		<div class="span3">
			{!! Layout::operation_column('col3', $fields, $categories, $fields, $operation, $errors, $request, false) !!}
		</div>

		<div class="span3">
			{!! Layout::operation_column('col4', $fields, $categories, $fields, $operation, $errors, $request, false) !!}
		</div>
		
	{!! Layout::row_end() !!}

@endsection

@section('footer')
	{!! Layout::view_logic($request) !!}
@endsection