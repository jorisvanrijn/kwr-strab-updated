@extends('layouts.app')

@section('content')
{!! Layout::row_start() !!}
	{!! Layout::panel_start('Statistieken', 3) !!}
			<h4>Statistieken</h4>
			<h1>{{ $landelijke_operaties }} <small>operaties</small></h1>
			<h1>{{ DB::table('users')->count() }} <small>accounts</small></h1>	
	{!! Layout::panel_end() !!}
	{!! Layout::panel_start('<i class="icon icon-info-sign"></i> Over het Kwaliteitsregister', 3) !!}
		<p>Het Kwaliteitsregister is uw persoonlijke database waarin u operaties bij kunt houden. Door operaties te uploaden kunt u uwzelf met de landelijke statistieken vergelijken. Daarnaast is deze persoonlijke database uw naslag werk.</p>
		<p>Kwaliteitsregister 3.0 is ontwikkeld door <br/><a href="http://www.dutchcodingcompany.com">The Dutch Coding Company</a>.</p>
		<hr/>
		<center><img src="{{ asset('assets/img/text.png') }}" style="width: 70%;"/><br/>
		(<small>versie {{ $versie }}</small>) 
		<small>&copy; {{ date('Y') }} - Dutch Coding Company</small></center>
	{!! Layout::panel_end() !!}	
	{!! Layout::panel_start('Support', 3) !!}
			<p>Voor vragen kunt u bellen (+31 (0) 85 88 88 511) of mailen naar:</p>
			<p><a href="mailto:info@dutchcodingcompany.com">info@dutchcodingcompany.com</a></p>
			<p><small class="muted text-muted">(Schakel naar <a href="?advanced">geavanceerde administratie</a>)</small></p>
	{!! Layout::panel_end() !!}
{!! Layout::row_end() !!}
@endsection