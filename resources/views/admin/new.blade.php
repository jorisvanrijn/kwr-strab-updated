@extends('layouts.app')

@section('content')

{!! Layout::row_start() !!}

	{!! Layout::panel_start('Nieuw account toevoegen', 4) !!}

		<p>Voer hier het emailadres van de persoon in. Een nieuwe database wordt automatisch (en anoniem) verstuurd naar de persoon.
		<form action="" method="POST">
			{!! csrf_field() !!}
			{!! method_field('POST') !!}
			<strong>Gebruikersnaam</strong><br/>
			<input type="text" name="username" class="form-control input-block-level" placeholder="Gebruikersnaam"><br/>
			<strong>Email</strong><br/>
			<input type="text" name="email" class="form-control input-block-level" placeholder="Email"><br/>
			<label class="checkbox">
		      <input type="checkbox" name="demodb"> Demo database <br/><small>(vink dit aan wanneer de database een test- of demodatabse is)</small>
		    </label>
			<input type="submit" value="Verstuur database" class="form-control btn btn-primary">
		</form>
		@if(isset($msg))
			<hr/>
			{!! $msg !!}
		@endif
		@if (count($errors) > 0)
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
		</div>

	{!! Layout::panel_end(); !!}

{!! Layout::row_end(); !!}

@endsection