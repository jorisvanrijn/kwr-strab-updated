@extends('layouts.app')

@section('content')

	{!! Layout::row_masonry_start() !!}

		{!! Layout::panel_start('Systeemgebruik: Statistieken', 4, false, true) !!}
			
			<p>Klik op de onderstaande link om doorgeschakeld te worden naar de uitgebreide gebruikersstatistieken</p>
			<p><a href="https://analytics.google.com/analytics/web/#report/visitors-overview/a86861359w129214851p132966084/" target="_blank" class="btn btn-primary">Google Analytics dashboard</a></p>
			<p><small>U dient eerst toegang te krijgen tot de gebruikersstatistieken. Neem hiervoor contact op met <a href="mailto:info@dutchcodingcompany.com">info@dutchcodingcompany.com</a></small></p>

		{!! Layout::panel_end() !!}
		
	{!! Layout::row_masonry_end() !!}

@endsection

@section('footer')

@endsection