@extends('layouts.app')

@section('buttons')
	<a href="{{ url('admin/new') }}" class="btn btn-primary">Account toevoegen</a>
@endsection

@section('content')

{!! Layout::row_start() !!}

	{!! Layout::panel_start('Accounts', 6) !!}

		@if($users->total() > 0)
			{!!
				$users->columns(array(
					'userid' => 'Gebruikersnaam',
			        'name' => 'Email',
			        'edit' => ''
			    ))

			    ->modify('name', function($row){

			    	return '<a href="mailto:'.$row->name.'">'.$row->name.'</a>';

			    })

			    ->modify('edit', function($row) {

			    	if($row->userid == 'admin') return '';

			    	return '

			    	<a href="'.url('admin/resetpassword/'.$row->id).'" class="btn btn-mini btn-default" data-toggle="tooltip" title="Reset password"><i class="icon icon-lock"></i></a>

			    	';
			    })

			    ->modifyRow('mod1', function($row) {
			        return array('data-id' => $row->id);
			    })

			    ->attributes(array(
			        'id' => 'users',
			        'class' => 'table sortabletable',
			        'style' => 'margin-bottom: 0px;',
			    ))

				->sortable(array('datum_operatie', 'geslacht'))
	    		->showPages()
			    ->render()
			!!}
		@else
			<div class="alert alert-info" style="margin:20px;">Geen rijen gevonden</div>
		@endif

		<div class="text-left pagination" style="margin:0;margin-left:15px;">{!! $users->links() !!}</div>

	{!! Layout::panel_end(); !!}

{!! Layout::row_end(); !!}

@endsection