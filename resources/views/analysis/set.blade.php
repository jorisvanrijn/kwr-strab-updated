@extends('layouts.app')

@section('buttons')
	Aantal operaties in analyse: 
		{{ $aantal_sets }} sets (maximaal {{ $set_size }} operaties per set) - {{ $used_operations }} operaties uit eigen set ({{ $set }})
@endsection


@section('content')

	{!! Layout::row_masonry_start() !!}

		{!! Layout::panel_start('Set analyse: '.$set.' (voor Horizontaal manifest op afstand hoek)', 7, false, true) !!}
			
			<div id="chart_div"><img src="{{ $graph_url }}" /></div>
			<!-- <div id="chart_div" style="width: 100%; height: 500px;"></div> -->

		{!! Layout::panel_end() !!}

		{!! Layout::panel_start('Set analyse: '.$set.' (voor Horizontaal manifest op afstand hoek)', 7, true, true) !!}
			{!! $tabel !!}
		{!! Layout::panel_end(); !!}
		
	{!! Layout::row_masonry_end() !!}

@endsection

@section('footer')
	<!-- <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
		  function drawChart() {
		    var data = google.visualization.arrayToDataTable({!! json_encode($data_for_graph) !!}, true);

		    var options = {
		      legend:'none',
		      title:'Set analyse: {{ $set }} (voor Horizontaal manifest op afstand hoek)',
		      colors: ['#3ea95f'],
		      hAxis: {title: "Horizontaal manifest op afstand hoek"},
		      vAxis: {title: "% <= hoek"}
		    };

		    var chart = new google.visualization.CandlestickChart(document.getElementById('chart_div'));

		    chart.draw(data, options);
		  }
    </script> -->
@endsection