@extends('layouts.app')

@section('buttons')
	Aantal operaties in analyse: 
		{{ isset($last_analysis_count_lokaal) ? $last_analysis_count_lokaal .' (lokaal)' : '' }} 
		{{ isset($last_analysis_count_landelijk) ? $last_analysis_count_landelijk .' (landelijk)' : '' }}
@endsection

@section('content')

	<div class="overlay" style="display: none;"></div>
	<div class="overlay_graph" style="display: none;">
		<a href="#" class="noloadinglink btn btn-small btn-default close-button"><i class="icon icon-remove-sign"></i> Sluit grafiek</a>
		<div class="graph_container_overlay"></div>
	</div>

	{!! Layout::row_masonry_start() !!}

		{!! Layout::panel_start('Views', 3, false, true) !!}
			{!! Layout::view_helper($request) !!}
			<hr/>
			{!! Layout::sup_manual() !!}
		{!! Layout::panel_end() !!}

		@foreach($categories as $c => $cat)

			{!! Layout::panel_start($cat['trivial'], 3, false, true, true) !!}

				{!! Layout::htmlAnalyseFields($fields, $c, $analyse, $request, $last_analysis_type) !!}

			{!! Layout::panel_end() !!}

		@endforeach
		
	{!! Layout::row_masonry_end() !!}

@endsection

@section('footer')
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script type="text/javascript">
	google.charts.load('current', {'packages':['corechart']});
	</script>

	{!! Layout::graph_logic_list($fields, $analyse, $request, $last_analysis_type) !!}
	{!! Layout::view_logic($request) !!}

	<script type="text/javascript">
		$(document).ready(function(){
			$('.enlarge-graph').click(function(e){

				// $('#' + $(this).data('graph') + '_graph').remove();

				$('.graph_container_overlay').addClass($(this).data('graph') + '_graph');
				$('.overlay_graph').css('display', 'block');
				$('.overlay').css('display', 'block');

				eval('draw_' + $(this).data('graph') + '()');

				e.preventDefault();
    			return false;
			});

			$('.close-button').click(function(e){
				$('.graph_container_overlay').removeClass($(this).data('graph') + '_graph');
				$('.overlay_graph').css('display', 'none');
				$('.overlay').css('display', 'none');

				e.preventDefault();
    			return false;
			});
		});
	</script>
@endsection