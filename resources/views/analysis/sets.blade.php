@extends('layouts.app')

@section('content')
	
{!! Layout::row_start() !!}
	
	{!! Layout::panel_start('Sets', 3) !!}
		Selecteer een set om te vergelijken met de landelijke data.
	{!! Layout::panel_end() !!}

	{!! Layout::panel_start('Sets: '.$sets->total() . ' '. (($sets->total() == 1) ? 'resultaat' : 'resultaten') . ' op '.$sets->lastPage() . ' '. (($sets->lastPage() == 1) ? 'pagina' : 'pagina\'s'), 5, true) !!}

		@if($sets->total() > 0)
			{!!
				$sets->columns(array(
					'created_at' => 'Set',
					'value2' => 'Type',
			        'value' => 'Notitie',
			        'edit' => ''
			    ))

			    ->modify('created_at', function($row) {
			    	$t = DateTime::createFromFormat('Y-m-d H:i:s', $row->created_at);
			    	if (!$t) throw new \UnexpectedValueException("Could not parse the date: $row->created_at");
			        return '<strong>'.$row->key.'</strong><br/> '.$t->format('d-m-Y H:i:s');
			    })

			    ->modify('edit', function($row) {
			    	return '
			    	
			    	<a href="'.url('analysis/do/set/'.$row->id).'" class="btn btn-small btn-default" data-toggle="tooltip" title="Analyseer deze set"><i class="icon icon-eye-open"></i></a>
			    	<!-- <a href="'.url('operations?filter%5B%5D=set_id&comparator%5B%5D=%3D&values%5B%5D='.$row->key).'" class="btn btn-small btn-default" data-toggle="tooltip" title="Operaties in deze set"><i class="icon icon-filter"></i></a> -->
			    	<!-- <a href="'.url('operations/edit_set/'.$row->id).'" class="btn btn-small btn-default" data-toggle="tooltip" title="Analyseer deze set"><i class="icon icon-eye-open"></i></a> -->
			    	<!-- <a href="'.url('operations/edit_set/'.$row->id).'" class="btn btn-small btn-default" data-toggle="tooltip" title="Bewerk"><i class="icon icon-pencil"></i></a> -->

			    	<!--<a href="'.url('operations/del_set/'.$row->id).'"><i class="icon icon-trash"></i></a>-->';
			    })

			    ->attributes(array(
			        'id' => 'sets',
			        'class' => 'table table-striped table-hover',
			        'style' => 'margin-bottom: 0px;',
			    ))

				->sortable(array('datum_operatie', 'geslacht'))
	    		->showPages()
			    ->render()
			!!}
		@else
			<div class="alert alert-info" style="margin:20px;">Geen rijen gevonden</div>
		@endif

		<div class="text-left pagination" style="margin:0;margin-left:15px;">{!! $sets->links() !!}</div>

	{!! Layout::panel_end() !!}

{!! Layout::row_end() !!}

@endsection