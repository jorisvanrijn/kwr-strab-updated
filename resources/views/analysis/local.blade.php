@extends('layouts.app')

@section('content')

	{!! Layout::row_masonry_start() !!}

		{!! Layout::panel_start('Views', 3, false, true) !!}
			{!! Layout::view_helper($request) !!}
			<hr/>
			{!! Layout::sup_manual() !!}
		{!! Layout::panel_end() !!}

		@foreach($categories as $c => $cat)

			{!! Layout::panel_start($cat['trivial'], 3, false, true, true) !!}

				{!! Layout::htmlAnalyseFields($fields, $c, $analysis, $request) !!}

			{!! Layout::panel_end() !!}

		@endforeach
		
	{!! Layout::row_masonry_end() !!}

@endsection

@section('footer')
	{!! Layout::view_logic($request) !!}
@endsection