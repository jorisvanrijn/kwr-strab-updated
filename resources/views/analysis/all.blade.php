@extends('layouts.app')

@section('content')

{!! Layout::row_start() !!}

	{!! Layout::panel_start('Set Analyse', 2) !!}
		<p>Analyseer uw eigen operatie kwaliteit ten opzichte van de landelijke data</p>
		<p><a href="{{ url('analysis/set') }}" class="btn btn-primary">Doe analyse</a></p>
	{!! Layout::panel_end() !!}

	{!! Layout::panel_start('Lokaal vs. landelijk', 2) !!}
		<p>Analyseer uw eigen parameters ten opzichte van de landelijke parameters</p>
		<p><a href="{{ url('analysis/localvsnational') }}" class="btn btn-primary">Doe analyse</a></p>
	{!! Layout::panel_end() !!}

	{!! Layout::panel_start('Lokale analyse', 2) !!}
		<p>Analyseer enkel uw eigen data. Bekijk het in tabel en grafiek vorm.</p>
		<p><a href="{{ url('analysis/local') }}" class="btn btn-primary">Doe analyse</a></p>
	{!! Layout::panel_end() !!}

	{!! Layout::panel_start('Landelijke analyse', 2) !!}
		<p>Analyseer enkel de landelijke data. Bekijk het in tabel en grafiek vorm.</p>
		<p><a href="{{ url('analysis/national') }}" class="btn btn-primary">Doe analyse</a></p>
	{!! Layout::panel_end() !!}

{!! Layout::row_end() !!}

@endsection