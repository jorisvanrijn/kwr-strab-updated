@extends('layouts.app')

@section('content')

{!! Layout::row_start() !!}

	{!! Layout::panel_start('Analyseer operaties', 6, false, true) !!}
		Maak een selectie van de operaties die u wilt analyseren. U kunt <a href="{{ url('operations/filters') }}">hier</a> nieuwe filters aanmaken.<br/>
		{!! FilterHelper::filter_interface($fields, $request, $url, -10); !!}
	{!! Layout::panel_end() !!}

{!! Layout::row_end() !!}

@endsection

@section('footer')
	
@endsection