Beste gebruiker,<br/><br/>
U wachtwoord is aangepast voor de Kwaliteitsregistratie {{ $dist }}.<br/>
U kunt inloggen met de onderstaande gegevens.<br/><br/>
Naam: {{ $userid }}<br/>
Wachtwoord: {{ $password }}
<br/><br/>
<img src="{{ asset('assets/img/text.png') }}" style="width: 300px;" />
<br/><br/>
<small>Deze mail is automatisch gegenereerd. Antwoorden worden niet gelezen.</small><br/>
<small>Kwaliteitsregistratie {{ $dist }}</small>