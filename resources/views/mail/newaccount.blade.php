Beste gebruiker,<br/><br/>
Er is een account voor u klaar gezet op de Kwaliteitsregistratie {{ $dist }}.<br/>
U kunt inloggen met de onderstaande informatie en de bijgevoegde database.<br/><br/>
Naam: {{ $userid }}<br/>
Wachtwoord: {{ $password }}<br/>
<br/>
U kunt inlogen op <a href="{{ url('login') }}">{{ url('login') }}</a>. Een handleiding is te vinden op <a href="{{ url('downloads/GettingStarted.pdf') }}">{{ url('downloads/GettingStarted.pdf') }}</a>.
@if($demodb)
<br/><br/>
Let op: Deze opgestuurde versie betreft een test- of demodatabase. Vraag bij de administrator een andere database aan wanneer u deze live wil gebruiken.
@endif
<br/><br/>
<img src="{{ asset('assets/img/text.png') }}" style="width: 300px;" />
<br/><br/>
<small>Deze mail is automatisch gegenereerd. Antwoorden worden niet gelezen.</small><br/>
<small>Kwaliteitsregistratie {{ $dist }}</small>