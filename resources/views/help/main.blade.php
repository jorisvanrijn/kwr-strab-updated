@extends('layouts.app')

@section('content')
{!! Layout::row_start() !!}
	{!! Layout::panel_start('Session stats', 3) !!}
		<p><strong>User ID:</strong> <pre>{!! $user_id !!}</pre></p>
		<p><strong>Session key:</strong> <pre>{!! $session_key !!}</pre></p>
		<p><strong>DB Version:</strong> <pre>{!! $db_version !!}</pre></p>
		@if($demodb == 'ja')
		<p><strong>Test- of demodatabase:</strong> <pre>Ja</pre></p>
		@endif
	{!! Layout::panel_end() !!}
	{!! Layout::panel_start('Handleiding', 3) !!}
		<p>Een handleiding voor het systeem is hier te downloaden:</p>
		<p><a href="{{ url('downloads/GettingStarted.pdf') }}" target="_blank">Download handleiding (PDF)</a></p>
	{!! Layout::panel_end() !!}	
	{!! Layout::panel_start('<i class="icon icon-info-sign"></i> Over het Kwaliteitsregister', 3) !!}
		<p>Het Kwaliteitsregister is uw persoonlijke database waarin u operaties bij kunt houden. Door operaties te uploaden kunt u uwzelf met de landelijke statistieken vergelijken. Daarnaast is deze persoonlijke database uw naslag werk.</p>
		<p>Kwaliteitsregister 3.0 is ontwikkeld door <br/><a href="http://www.dutchcodingcompany.com">The Dutch Coding Company</a>.</p>
		<hr/>
		<center><img src="{{ asset('assets/img/text.png') }}" style="width: 70%;"/><br/>
		<small>&copy; {{ date('Y') }} - Dutch Coding Company</small></center>
	{!! Layout::panel_end() !!}		
{!! Layout::row_end() !!}
@endsection