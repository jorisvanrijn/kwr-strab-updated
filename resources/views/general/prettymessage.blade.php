@extends('layouts.app')

@section('content')
<div class="container text-center"> 
    <div class="row">
        <div class="span4 offset4 text-center">
            <!-- <a href="{{ url('/') }}"><img src="{{ asset('assets/img/text.png') }}" class="logo"/></a> -->
        </div>
    </div>
    <div class="row">
        <div class="span4 offset4 text-center box-it">
            <div class="inner">
               <p>{!! $msg !!}</p>
            </div>
        </div>
    </div>
</div>
@endsection