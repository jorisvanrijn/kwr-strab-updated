@extends('layouts.app')

@section('content')


{!! Layout::row_start() !!}

	{!! Layout::panel_start('', 4) !!}

		<p>{!! $msg !!}</p>

	{!! Layout::panel_end(); !!}

{!! Layout::row_end(); !!}

@endsection