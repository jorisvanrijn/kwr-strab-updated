@extends('layouts.app')

@section('content')


{!! Layout::row_start() !!}

	{!! Layout::panel_start($title, 4) !!}

		<p style="margin-bottom: 0;" class="text-error">{!! $msg !!}</p>
		<br/>
		<p><a href="{{ url()->previous() }}" class="btn btn-default">Terug</a></p>

	{!! Layout::panel_end(); !!}

{!! Layout::row_end(); !!}

@endsection