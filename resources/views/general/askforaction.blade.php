@extends('layouts.app')

@section('content')

{!! Layout::row_start() !!}

	{!! Layout::panel_start('Bevestig actie', 4) !!}

		<p>{!! $msg !!}</p>
		<div style="margin-top: 20px; display: block;">{!! $btn !!}</div>

	{!! Layout::panel_end(); !!}

{!! Layout::row_end(); !!}

@endsection