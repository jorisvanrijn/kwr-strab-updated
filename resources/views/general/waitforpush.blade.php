@extends('layouts.app')

@section('content')
<div class="container text-center"> 
    <div class="row">
        <div class="span4 offset4 text-center">
            <!-- <a href="{{ url('/') }}"><img src="{{ asset('assets/img/text.png') }}" class="logo"/></a> -->
        </div>
    </div>
    <div class="row">
        <div class="span4 offset4 text-center box-it">
            <div class="inner">
               <p><img src="{{ asset('assets/img/loading.gif') }}" style=" width: 60px; margin-bottom: 15px; margin-top: 10px;"/></p>
               <p class="topmsg">{{ $status }} (<span class="current">0</span>/{{ $total }})</p>
               <div class="progress active">
                  <div id="prbar" class="bar" style="width: 3%;"></div>
               </div>
               <p style="margin-bottom: 0;" class="waitmsg"><em>Een ogenblik geduld alstublieft. <span class="perc">0%</span></em><!-- <br/><br/> -->
                  <!-- <small>Tip: Ondertussen kunt u naar andere pagina's navigeren</small> -->
               </p>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
  
  <script type="text/javascript">
    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('9b4b5f978bb061f5c42e', {
      cluster: 'eu',
      encrypted: true
    });

    var channel = pusher.subscribe('{{ $serpentes }}');
    channel.bind('App\\Events\\JobFinished', function(data) {
        // new PNotify({
        //     text: data.msg,
        //     type: data.type_alert,
        //     addclass: "stack-bottomleft",
        //     stack: stack_bottomleft,
        //     hide: false
        // });
        
        if(data.last){
          $('#prbar').css('width', '100%');
          $('.topmsg').html('<strong>' + data.msg + '</strong>');
          // $('.topmsg').addClass('text-success');
          $('.waitmsg').html('<a href="' + data.link + '" class="btn btn-primary btn-block">{{ $button }}</a>');
          
          // $('.progress').addClass('progress-success');
          // $('.progress').removeClass('progress-striped');

          window.setTimeout(gotolink(data.link), 3000);
          
        }else{
          $('#prbar').css('width', ( Math.round(data.iteration/data.max_iterations*100) ) + '%');
          $('.perc').html( Math.round(data.iteration/data.max_iterations*100)  + '%');
          $('.current').html( Math.round(data.iteration * data.per_keer) );
        }
    });

    function gotolink(link){
      window.location.href = link;
    }
  </script>

@endsection