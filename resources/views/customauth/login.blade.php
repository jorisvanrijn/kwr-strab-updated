@extends('layouts.app')

@section('content')
<div class="container text-center"> 
    <div class="row">
        <div class="span4 offset4 text-center">
            <a href="{{ url('/login?force') }}"><img src="{{ asset('assets/img/text.png') }}" class="logo"/></a>
        </div>
    </div>
    <div class="row">
        <div class="span4 offset4 text-center box-it">
            <div class="inner">
               <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    @if ($errors->has('userid'))
                        <br/><span class="alert alert-error text-left">
                            <strong>{{ $errors->first('userid') }}</strong>
                        </span><br/><br/>
                    @endif

                    @if ($errors->has('database'))
                        <br/><span class="alert alert-error text-left">
                            <strong>{{ $errors->first('database') }}</strong>
                        </span><br/><br/>
                    @endif

                    @if($maintenancemessage AND !isset($_GET['force']))
                        <br/><div class="alert text-left">
                            <strong>Korte update</strong><br/>
                            Op het moment voeren wij een korte update aan het systeem door. Het systeem zal snel weer beschikbaar zijn.
                        </div>
                    @else

                        <br/>

                        <div class="form-group{{ $errors->has('userid') ? ' has-error' : '' }}">
                            <label for="userid" class="col-md-4 text-left"><strong>User-ID</strong></label>

                            <div>
                                <input id="userid" type="text" class="form-control input-block-level" name="userid" value="">
                                <!-- voorheen: {{ old('userid') }}-->
                            </div>
                        </div>
                        <br/>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 text-left"><strong>Wachtwoord</strong> <small>(<a href="mailto:support@kwaliteitsregister.net?Subject=Aanvraag%20nieuw%20wachtwoord%20kwaliteitsregister&Body=Vraag%20een%20nieuw%20wachtwoord%20aan%20door%20uw%20gebruikersnaam%20en%20emailadress%20te%20mailen%20naar%20support@kwaliteitsregister.net" class="noloadinglink">vergeten?</a>)</small></label>

                            <div>
                                <input id="password" type="password" class="form-control input-block-level" name="password" value="">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <br/>
                        <div class="form-group{{ $errors->has('database') ? ' has-error' : '' }}">
                            <label for="database" class="col-md-4 text-left"><strong>Lokale database</strong></label>

                            <div class="col-md-6" style="clear: both; padding-bottom: 10px;">
                                <input type="file" name="file" id="file" class="inputfile" />
                                <label for="file" id="filelabel" class="btn btn-default btn-block">Selecteer bestand</label>
                            </div>
                        </div>

                        <div class="form-group" style="clear: both; ">
                            <div class="col-md-6 col-md-offset-4"><br/>
                                <button type="submit" class="btn btn-primary btn-block" style="margin-bottom: 0;">
                                    <i class="fa fa-btn fa-sign-in"></i> Login
                                </button>

                                <!-- <a class="btn btn-link" href="{{ url('downloads/2016-09-18 Getting Started.pdf') }}" target="_blank">Problemen met inloggen?</a> -->
                            </div>
                        </div>

                    @endif
                </form>
            </div>
        </div>
    </div>
    <div style="margin-top: 10px;">
        <center>
            <small>(versie {{ $versie }})</small> &copy; {{ date('Y') }} - Dutch Coding Company</small>
        </center>
    </div>
</div>
@endsection

@section('footer')
<script type="text/javascript">

    $(function(){

        $('#login').hide();

        $('#file').change(function(){
            var filename = $('#file').val().split('\\').pop();
            $('#filelabel').text(filename);
            $('#login').slideDown();
        });

    });

</script>
@endsection