@extends('layouts.app')

@section('content')
<div class="container text-center"> 
    <div class="row">
        <div class="span4 offset4 text-center">
            <!-- <a href="{{ url('/') }}"><img src="{{ asset('assets/img/text.png') }}" class="logo"/></a> -->
        </div>
    </div>
    <div class="row">
        <div class="span4 offset4 text-center box-it">
            <div class="inner">
               <p>Vergeet uw database niet op te slaan voordat u het systeem af sluit.</p>
               <p class="noloading"><a href="{{ url('dashboard/download') }}" class="btn btn-primary btn-block btn-large save-button">Opslaan</a></p>
               <p style="margin-bottom: 0;"><a href="{{ url('logout?continue') }}" class="btn btn-danger">Uitloggen</a></p>
            </div>
        </div>
    </div>
</div>
@endsection