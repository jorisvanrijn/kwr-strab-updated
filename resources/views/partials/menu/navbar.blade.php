@if((Menu::get('firstBar')))
    <div class="firsttitle">
@else
    <div class="firsttitle" style="padding-bottom: 15px;">
@endif
<a href="{{ url('/') }}"><img src="{{ asset('assets/img/text-white.png') }}" class="small-logo"/></a></div>

@if((Menu::get('firstBar')))
<div class="navbar" style="margin-bottom: 0;">
    <div class="navbar-inner">

        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <!-- <a class="brand" href="{{ url('/') }}"><img src="{{ asset('assets/img/text-white.png') }}" class="small-logo"/></a> -->
            <div class="nav-collapse">
                <ul class="nav">
                    
                        @include('partials.menu.items', ['items'=> Menu::get('firstBar')->roots()])
                                        
                </ul>
            </div>
            <div class="nav-collapse pull-right noloading">
                <ul class="nav">
                    @include('partials.menu.items', ['items'=> Menu::get('userBar')->roots()])
                </ul>
            </div>
        </div>
    </div>
</div>
@endif

@if((Menu::get('secondBar')))
<div class="navbar navbar-second">
    <div class="navbar-inner">
        <div class="container">       
            <a class="btn btn-navbar" data-toggle="collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <!-- <a class="brand">Dashboard &raquo;</a>      -->
            <div class="nav-collapse">
                <ul class="nav">
                    @include('partials.menu.items', ['items'=> Menu::get('secondBar')->roots()])
                </ul>
            </div>
        </div>
    </div>
</div>
@endif